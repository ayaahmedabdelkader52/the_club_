<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following languages lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these languages lines according to your application's requirements.
    |
    */

    'failed' => 'بيانات الاعتماد هذه غير متطابقة مع البيانات المسجلة لدينا.',
    'password' => 'كلمة المرور المستخدمة غير صحيحة.',
    'throttle' => 'محاولات تسجيل دخول كثيرة جدًا. يرجى المحاولة مرة أخرى خلال: ثواني.',
    'invalid_code' => 'الكود الذي ادخلته غير صحيح',
    'code_sent' => 'تم إرسال الكود',
    'your_code' => 'كود التفعيل الخاص بك من ',
];
