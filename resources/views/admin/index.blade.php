@extends('admin.master')
@section('title')
    الصفحه الرئيسيه
@stop

@section('before-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css-rtl/pages/dashboard-analytics.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/vendors-rtl.min.css')}}">

@stop

@section('content')

    <!-- BEGIN: Content-->
    <div class="content-header row">
    </div>
    <div class="content-body">
        <!-- Dashboard Analytics Start -->
        <section id="dashboard-analytics">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card bg-analytics text-white">
                        <div class="card-content">
                            <div class="card-body text-center">
                                <img src="{{asset('Admin/app-assets/images/elements/decore-left.png')}}" class="img-left" alt="card-img-left">
                                <img src="{{asset('Admin/app-assets/images/elements/decore-right.png')}}" class="img-right" alt="card-img-right">
                                <div class="avatar avatar-xl bg-primary shadow mt-0">
                                    <div class="avatar-content">
                                        <i class="feather icon-award white font-large-1"></i>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <h1 class="mb-2 text-white">{{ __('dashboard.welcome_name', ['name' => auth()->user()->name ]) }}</h1>
                                    <h1 class="mb-2 text-white"></h1>

                                    <p class="m-auto w-75">الساعة الآن <h3 id="MyClockDisplay" class="dashboard-clock-now"></h3>  </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-12">
                    <div class="card">
                        <div class="card-header d-flex flex-column align-items-center pb-0">
                            <div class="avatar bg-rgba-primary p-50 m-0">
                                <div class="avatar-content">
                                    <i class="feather icon-users text-primary font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700 mt-1 mb-25">{{$numdoctors}}</h2>
                            <p>Doctors</p>
                        </div>
{{--                        <div class="card-content">--}}
{{--                            <div id="subscribe-gain-chart"></div>--}}
{{--                        </div>--}}
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="card">
                        <div class="card-header d-flex flex-column align-items-center pb-0">
                            <div class="avatar bg-rgba-primary p-50 m-0">
                                <div class="avatar-content">
                                    <i class="feather icon-users text-primary font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700 mt-1 mb-25">{{$numpatients}}</h2>
                            <p class="mb-15">Patients</p>
                        </div>
                    </div>
                </div>
                        <div class="col-lg-4 col-md-6 col-12">
                    <div class="card">
                        <div class="card-header d-flex flex-column align-items-center pb-0">
                            <div class="avatar bg-rgba-primary p-50 m-0">
                                <div class="avatar-content">
                                    <i class="feather icon-users text-primary font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700 mt-1 mb-25">{{$allusers}}</h2>
                            <p class="mb-15">All users</p>
                        </div>
{{--                        <div class="card-content">--}}
{{--                            <div id="orders-received-chart"></div>--}}
{{--                        </div>--}}
                    </div>
                </div>
                    </div>

                    <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="chart-info d-flex justify-content-between mb-1">
                                    <div class="series-info d-flex align-items-center">
                                        <i class="fa fa-circle-o text-bold-700 text-primary"></i>
                                        <span class="text-bold-600 ml-50">Finished Order</span>
                                    </div>
                                    <div class="product-result">
                                        <span>{{$finished_order}}</span>
                                    </div>
                                </div>
                                <div class="chart-info d-flex justify-content-between mb-1">
                                    <div class="series-info d-flex align-items-center">
                                        <i class="fa fa-circle-o text-bold-700 text-warning"></i>
                                        <span class="text-bold-600 ml-50">Pending Order</span>
                                    </div>
                                    <div class="product-result">
                                        <span>{{$new_order}}</span>
                                    </div>
                                </div>
                                <div class="chart-info d-flex justify-content-between mb-75">
                                    <div class="series-info d-flex align-items-center">
                                        <i class="fa fa-circle-o text-bold-700 text-danger"></i>
                                        <span class="text-bold-600 ml-50">Rejected Order</span>
                                    </div>
                                    <div class="product-result">
                                        <span>{{$rejected_order}}</span>
                                    </div>
                                </div>

                                <div class="chart-info d-flex justify-content-between mb-75">
                                    <div class="series-info d-flex align-items-center">
                                        <i class="fa fa-circle-o text-bold-700 text-danger"></i>
                                        <span class="text-bold-600 ml-50">Active Order</span>
                                    </div>
                                    <div class="product-result">
                                        <span>{{$active_order}}</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="chart-info d-flex justify-content-between mb-1">
                                    <div class="series-info d-flex align-items-center">
                                        <i class="fa fa-circle-o text-bold-700 text-primary"></i>
                                        <span class="text-bold-600 ml-50">Blocked users</span>
                                    </div>
                                    <div class="product-result">
                                        <span>{{$blocked_users}}</span>
                                    </div>
                                </div>
                                <div class="chart-info d-flex justify-content-between mb-1">
                                    <div class="series-info d-flex align-items-center">
                                        <i class="fa fa-circle-o text-bold-700 text-warning"></i>
                                        <span class="text-bold-600 ml-50">Pending users</span>
                                    </div>
                                    <div class="product-result">
                                        <span>{{$pending_users}}</span>
                                    </div>
                                </div>
{{--                                <div class="chart-info d-flex justify-content-between mb-75">--}}
{{--                                    <div class="series-info d-flex align-items-center">--}}
{{--                                        <i class="fa fa-circle-o text-bold-700 text-danger"></i>--}}
{{--                                        <span class="text-bold-600 ml-50">Rejected Order</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="product-result">--}}
{{--                                        <span>{{$rejected_order}}</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

                                <div class="chart-info d-flex justify-content-between mb-75">
                                    <div class="series-info d-flex align-items-center">
                                        <i class="fa fa-circle-o text-bold-700 text-danger"></i>
                                        <span class="text-bold-600 ml-50">Active users</span>
                                    </div>
                                    <div class="product-result">
                                        <span>{{$active_users}}</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    <!-- END: Content-->
@stop
@section('scripts')
    <script src="{{asset('Admin/app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('Admin/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/extensions/tether.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/extensions/shepherd.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('Admin/app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{asset('Admin/app-assets/js/core/app.js')}}"></script>
    <script src="{{asset('Admin/app-assets/js/scripts/components.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('Admin/app-assets/js/scripts/pages/dashboard-analytics.js')}}"></script>
    <!-- END: Page JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('Admin/app-assets/vendors/js/charts/chart.min.js')}}"></script>

    <script src="{{asset('Admin/app-assets/js/scripts/charts/chart-chartjs.js')}}"></script>
    <!-- END: Page JS-->

    <script>
        function showTime(){
            var date = new Date();
            var h = date.getHours(); // 0 - 23
            var m = date.getMinutes(); // 0 - 59
            var s = date.getSeconds(); // 0 - 59
            var session = "AM";

            if(h == 0){
                h = 12;
            }

            if(h > 12){
                h = h - 12;
                session = "PM";
            }

            h = (h < 10) ? "0" + h : h;
            m = (m < 10) ? "0" + m : m;
            s = (s < 10) ? "0" + s : s;

            var time = h + ":" + m + ":" + s + " " + session;
            document.getElementById("MyClockDisplay").innerText = time;
            document.getElementById("MyClockDisplay").textContent = time;

            setTimeout(showTime, 1000);

        }

        showTime();
    </script>

@stop
