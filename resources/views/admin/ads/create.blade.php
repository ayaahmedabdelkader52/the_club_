@extends('admin.master')
@section('title')
    اضافه اعلان
    @stop
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css/plugins/forms/validation/form-validation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/pickers/pickadate/pickadate.css')}}">
    <style>

    </style>
@stop

@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الاعلانات</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.ads.index') }}">جميع الاعلانات</a></li>

                            <li class="breadcrumb-item active">اضافه</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <div class="content-body">
        <!-- // Basic multiple Column Form section start -->
        <section id="multiple-column-form">
            <div class="row match-height">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">اضافه اعلان</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form" action="{{ route('admin.ads.store') }}" method="POST" enctype="multipart/form-data" >
                                @csrf

                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <input autofocus type="text" id="first-name-column" class="form-control" name="title" value="{{ old('title') }}"
                                                           placeholder="العنوان">
                                                    <label for="first-name-column">العنوان</label>
                                                </div>
                                                @error('title')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <input type="text" id="last-name-column" class="form-control" name="content" value="{{ old('content') }}"
                                                           placeholder="محتوي الاعلان" >
                                                    <label for="last-name-column">محتوي الاعلان</label>
                                                </div>
                                                @error('content')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>


                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <input type='text' id="expiry_date" class="form-control format-picker"
                                                           name="expiry_date" placeholder="تاريخ الانتهاء"
                                                           value="{{ old('expiry_date') }}"/>

                                                    <label for="expiry_date">تاريخ الانتهاء</label>
                                                </div>
                                                @error('expiry_date')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>

                                            <div class="col-md-6 col-12 " style="margin-top: 10px">
                                                <div class="form-label-group form-group">
                                                    <input type="file" id="image" class="form-control" name="image" placeholder="صوره الاعلان">
                                                    <label for="email-id-column">صوره الاعلان</label>

                                                    <div class="form-label-group">
                                                        <div class="multi-img-result">
                                                            <div class="img-uploaded">
                                                                <img src="{{ asset('assets/uploads/users/default.png') }}" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                @error('image')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>


                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">اضافه</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // Basic Floating Label Form section end -->
    </div>

@stop
@section('scripts')
    <script src="{{ asset('includes/image-preview-2.js') }}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/pickers/pickadate/picker.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/pickers/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/pickers/pickadate/picker.time.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/pickers/pickadate/legacy.js')}}"></script>
    <script>
          $('.format-picker').pickadate({
        format: 'yyyy-mm-dd'
    });

    </script>

@stop
