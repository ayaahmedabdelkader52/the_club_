@extends('admin.master')
@section('title')
    الاعلانات
@stop
@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الاعلانات</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.ads.index') }}">الاعلانات</a></li>
                            <li class="breadcrumb-item active">جميع الاعلانات</li>
                        </ol>
                    </div>
                </div>
        </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <!-- Data list view starts -->
    <section id="data-list-view" class="data-list-view-header">
        <div class="action-btns d-none">
            <div class="btn-dropdown mr-1 mb-1">
                <div class="btn-group dropdown actions-dropodown">
                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('dashboard.main.Actions') }}
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{route('admin.ads.create')}}"><i
                                class="feather icon-archive"></i>اضف اعلان </a>
                        <a class="dropdown-item delete-all" href="#"><i class="feather icon-trash"></i>حذف
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Data list view Ends -->

        <!-- DataTable starts -->
        <div class="table-responsive">
            <table class="table data-list-view">
                <thead>
                <tr>
                    <th></th>
                    <th>صوره الاعلان</th>
                    <th>عنوان الاعلان</th>
                    <th>محتوي الاعلان</th>
                    <th>تاريخ الانتهاء</th>
                    <th>الاجراءت</th>
                </tr>
                </thead>
                <tbody>
                @foreach($ads as $ad)
                    <tr id="ad-{{$ad->id}}">
                        <td data-ad-id="{{$ad->id}}"></td>
                        <td><img src="{{ $ad->imagePath }}" style="width: 100px;" height="100px;"></td>
                        <td>{{$ad->title}}</td>
                        <td>{{$ad->content}}</td>
                        <td>{{ \Carbon\Carbon::parse($ad->expiry_date)->isoFormat('Y-m-d') }}</td>
                        <td>
                            <a href="{{route('admin.ads.edit',$ad)}}">
                                <i class="feather icon-edit"></i>
                            </a>

                            <a href="#">
                                 <span class="action-delete" data-ad-id="{{ $ad->id }}">
                                    <i class="feather icon-trash"></i>
                                 </span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- DataTable ends -->
    </section>
    <!-- Data list view end -->
@endsection
@section('scripts')

<script>

    var selectedAds = [];
    $('.action-delete').on('click', function () {
    var adId = $(this).data('ad-id'),
    url = '{{ route("admin.ads.destroy", ":id") }}',
    newUrl = url.replace(':id', adId);

    console.log( adId);
    console.log( newUrl);

    Swal.fire({
    title: 'هل تريد حذف الاعلان ?',
    // text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'نعم احذفه ',
    confirmButtonClass: 'btn btn-primary',
    cancelButtonClass: 'btn btn-danger ml-1',
    cancelButtonText: 'الغاء',
    buttonsStyling: false,
    }).then(function (result) {
    if (result.value) {
    $.ajax({
    url: newUrl,
    method: 'DELETE',
    // _token: "{{ csrf_token() }}",
    success: function(response) {
    fireSuccess('{{ __('dashboard.alerts.deleted')  }}')
    Swal.fire({
    position: 'top-start',
    type: 'success',
    title: '{{ __('dashboard.alerts.deleted')  }}',
    showConfirmButton: false,
    timer: 1500,
    confirmButtonClass: 'btn btn-primary',
    buttonsStyling: false,
    })
    console.log(response)
    console.log(response.data.adId)
    // Remove the raw
    $('#ad-' + adId).remove();
    }
    });
    }
    })
    });
    </script>

    <script>
          $('.delete-all').click(function () {
            var selectedAds = [];
            $('.dt-checkboxes:checked').each(function() {
                selectedAds.push( $(this).parent().data('ad-id') );
            });
            // console.log(selectedAds.length);
            if(selectedAds.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: 'هل تريد حذف الاعلانات المحدده',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم احذف المحدد',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: 'الغاء',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.ads.destroy_selected') }}",
                            method: 'POST',
                            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            // _token: "{{ csrf_token() }}",
                            data: {
                                ads: selectedAds
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: 'تم حذف المحدد بنجاح ',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                selectedAds.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#ad-' + element).remove();
                                } );

                                // console.log(selectedAds)
                                // console.log(response.data.adId)
                            }
                        });
                    }
                })

            }

        });
    </script>


@stop
