@extends('admin.master')
@section('title')
    اضافه تخصص
@stop

@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">التخصصات</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.subcategories.index') }}"> التخصصات</a></li>
                            <li class="breadcrumb-item active">اضافه</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <div class="content-body">
        <!-- // Basic multiple Column Form section start -->
        <section id="multiple-column-form">
            <div class="row match-height">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">اضافه تخصص</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form" action="{{ route('admin.subcategories.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <input type="text" class="form-control" name="title_ar"
                                                           placeholder="{{ __('dashboard.main.name_in_ar') }}" autofocus
                                                           value="{{old('title_ar')}}">
                                                    <label for="ar-name">{{ __('dashboard.main.name_in_ar') }}</label>
                                                    @include('admin.includes.alerts.input-errors', ['input' => "title_ar"])
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <input type="text" class="form-control" name="title_en"
                                                           placeholder="{{ __('dashboard.main.name_in_en') }}" autofocus
                                                           value="{{ old('title_en')}}">
                                                    <label for="en-name">{{ __('dashboard.main.name_in_en') }}</label>
                                                    @include('admin.includes.alerts.input-errors', ['input' => "title_en"])
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <input type="file" class="form-control" name="logo"
                                                           placeholder="{{ __('dashboard.main.image') }}" autofocus >
{{--                                                    <img src="">--}}
                                                    <label for="en-name">{{ __('dashboard.main.image') }}</label>
                                                    @include('admin.includes.alerts.input-errors', ['input' => "logo"])
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label for="projectinput2"> أختر القسم </label>
                                                    <select name="category_id" class="select2 form-control">
                                                        <optgroup label=" أختر القسم ">
                                                            @if($categories && $categories -> count() > 0)
                                                                @foreach($categories as $category)
                                                                    <option name="category_id" value="{{$category -> id }}">{{$category -> title_ar}}</option>
                                                                @endforeach
                                                            @endif
                                                        </optgroup>
                                                    </select>
                                                    @include('admin.includes.alerts.input-errors', ['input' => "category_id"])
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">اضافه</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // Basic Floating Label Form section end -->
    </div>

@stop
@section('scripts')
    <script src="{{asset('Admin/app-assets/vendors/js/pickers/pickadate/picker.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/pickers/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/pickers/pickadate/picker.time.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/pickers/pickadate/legacy.js')}}"></script>

    <script src="{{asset('Admin/app-assets/vendors/js/editors/quill/katex.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/editors/quill/highlight.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/editors/quill/quill.min.js')}}"></script>
    <script src="{{ asset('includes/ckeditor/ckeditor.js') }}"></script>

    <script>
          $('.format-picker').pickadate({
        format: 'yyyy-mm-dd'
    });

    </script>
    <script>
        CKEDITOR.replace('answer[ar]',{
            contentsLangDirection:'rtl',
            language: '{{ app()->getLocale() }}',
            removePlugins:'image,flash,iframe,smiley,about',

        });
        CKEDITOR.replace('answer[en]',{
            contentsLangDirection:'ltr',
            language: '{{ app()->getLocale() }}',
            removePlugins:'image,flash,iframe,smiley,about',

        });

    </script>
{{--    <script>--}}

{{--    	var quill = new Quill('.quill_editor', {--}}
{{--    	modules: {--}}
{{--      'formula': true,--}}
{{--      'syntax': true,--}}
{{--      'toolbar': [--}}
{{--        [{--}}
{{--          'font': []--}}
{{--        }, {--}}
{{--          'size': []--}}
{{--        }],--}}
{{--        ['bold', 'italic', 'underline', 'strike'],--}}
{{--        [{--}}
{{--          'color': []--}}
{{--        }, {--}}
{{--          'background': []--}}
{{--        }],--}}
{{--        [{--}}
{{--          'script': 'super'--}}
{{--        }, {--}}
{{--          'script': 'sub'--}}
{{--        }],--}}
{{--        [{--}}
{{--          'header': '1'--}}
{{--        }, {--}}
{{--          'header': '2'--}}
{{--        }, 'blockquote', 'code-block'],--}}
{{--        [{--}}
{{--          'list': 'ordered'--}}
{{--        }, {--}}
{{--          'list': 'bullet'--}}
{{--        }, {--}}
{{--          'indent': '-1'--}}
{{--        }, {--}}
{{--          'indent': '+1'--}}
{{--        }],--}}
{{--        ['direction', {--}}
{{--          'align': []--}}
{{--        }],--}}
{{--        ['link', 'image', 'video', 'formula'],--}}
{{--        ['clean']--}}
{{--      ],--}}
{{--    },--}}
{{--        		theme: 'snow'--}}
{{--    	});--}}
{{--    	var form = document.getElementById("terms-form");--}}
{{--    form.onsubmit = function() {--}}
{{--        document.getElementById("quill_html").value = quill.root.innerHTML;--}}
{{--    }--}}
{{--</script>--}}

@stop
