@extends('admin.master')

@section('title', 'تعديل دولة')

@section('content')

    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الدول</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.countries.index') }}">الدول</a></li>
                            <li class="breadcrumb-item active">{{ __('dashboard.action.edit') }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        {{--        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">--}}
        {{--            <div class="form-group breadcrum-right">--}}
        {{--                <div class="dropdown">--}}
        {{--                    <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>--}}
        {{--                    <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
    <!--end of content wrapper -->
    <div class="content-body">
        <!-- // Basic multiple Column Form section start -->
        <section id="multiple-column-form">
            <div class="row match-height">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{ __('dashboard.action.edit') }}</h4>
                        </div>

                        <!-- You can all alert messages by removing the comment -->
                                                @include('admin.includes.alerts.all-errors')
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form" action="{{ route('admin.countries.update', $country) }}" method="POST" enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-body">
                                        <div class="row">
                                            @foreach(config('app.languages') as $key => $language)
                                                <div class="col-md-6 col-12">
                                                    <div class="form-label-group form-group">
                                                        <input type="text" id="{{$key}}-name" class="form-control" name="name[{{$key}}]"
                                                               placeholder="{{ __('dashboard.main.name_in_' . $key) }}" autofocus
                                                               value="{{ $country->getTranslation("name", $key) }}">

                                                        <label for="{{$key}}-name">{{ __('dashboard.main.name_in_' . $key) }}</label>

                                                        @include('admin.includes.alerts.input-errors', ['input' => "name.$key"])
                                                    </div>
                                                </div>
                                            @endforeach


                                            @foreach(config('app.languages') as $key => $language)
                                                <div class="col-md-6 col-12">
                                                    <div class="form-label-group form-group">
                                                        <input type="text" id="{{$key}}-currency" class="form-control"
                                                               name="currency[{{$key}}]" placeholder="{{ __('dashboard.main.currency_in_' . $key) }}"
                                                               value="{{ $country->getTranslation("currency", $key) }}">

                                                        <label for="{{$key}}-currency">{{ __('dashboard.main.currency_in_' . $key) }}</label>

                                                        @include('admin.includes.alerts.input-errors', ['input' => "currency.$key"])
                                                    </div>
                                                </div>
                                            @endforeach

                                            @foreach(config('app.languages') as $key => $language)
                                                <div class="col-md-6 col-12">
                                                    <div class="form-label-group form-group">
                                                        <input type="text" id="{{$key}}-currency_code" class="form-control"
                                                               name="currency_code[{{$key}}]" placeholder="{{ __('dashboard.main.currency_code_in_' . $key) }}"
                                                               value="{{ $country->getTranslation("currency_code", $key) }}">

                                                        <label for="{{$key}}-currency_code">{{ __('dashboard.main.currency_code_in_' . $key) }}</label>

                                                        @include('admin.includes.alerts.input-errors', ['input' => "currency_code.$key"])
                                                    </div>
                                                </div>
                                            @endforeach

                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <input type="text" id="phone-code" class="form-control" name="calling_code"
                                                           placeholder="كود الموبايل" value="{{ $country->calling_code }}" >

                                                    <label for="phone-code">كود الموبايل</label>

                                                    @include('admin.includes.alerts.input-errors', ['input' => 'calling_code'])
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <input type="text" id="iso3" class="form-control" name="iso3"
                                                           placeholder="ISO3" value="{{ $country->iso3 }}">

                                                    <label for="iso2">ISO3</label>

                                                    @include('admin.includes.alerts.input-errors', ['input' => 'iso3'])
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <input type="text" id="iso2" class="form-control" name="iso2"
                                                           placeholder="ISO2" value="{{ $country->iso2 }}">

                                                    <label for="iso2">ISO2</label>

                                                    @include('admin.includes.alerts.input-errors', ['input' => 'iso2'])
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <div class="custom-control custom-switch switch-lg">
                                                        <input type="checkbox" name="active" @if($country->active) checked @endif class="custom-control-input" id="customSwitch1">
                                                        <label class="custom-control-label" for="customSwitch1">
                                                            <span class="switch-text-left ">مفعل</span>
                                                            <span class="switch-text-right">غير مفعل</span>
                                                        </label>
                                                    </div>
                                                    <label for="email-id-column">الحالة</label>

                                                    @include('admin.includes.alerts.input-errors', ['input' => 'active'])
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12 ">
                                                <div class="form-label-group form-group">
                                                    <input type="file" id="image" class="form-control" name="flag" placeholder="العلم">
                                                    <label for="email-id-column">العلم</label>

                                                    <div class="form-label-group">
                                                        <div class="multi-img-result">
                                                            <div class="img-uploaded">
                                                                <img src="{{ $country->flagPath }}" alt="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @include('admin.includes.alerts.input-errors', ['input' => 'flag'])
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.edit') }}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // Basic Floating Label Form section end -->
    </div>

@endsection
@section('scripts')
    <script src="{{ asset('includes/image-preview-2.js') }}"></script>
@endsection

