<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="rtl">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="@if(isset($setting['site_description'])){{ $setting['site_description'] }} @endif">
    <meta name="keywords" content="@if(isset($setting['site_tagged'])){{ $setting['site_tagged'] }} @endif">
    <meta name="author" content="@if(isset($setting['site_name'])){{ $setting['site_name'] }} @endif">
    <title>@yield('title')</title>

    @yield('before-styles')

{{--    <link rel="shortcut icon" type="image/x-icon" href="@if(isset($setting['favicon'])){{asset('assets/uploads/settings/' . $setting['favicon'] )}} @else {{asset('Admin/app-assets/images/ico/default.png')}} @endif">--}}
    <link rel="shortcut icon" href="{{asset('assets/uploads/settings/' . $setting['logo'])}}" type="image/x-icon" />

    <link href="{{ asset('Admin/app-assets/fonts/googleapi.css') }}" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/vendors'.$dir.'.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/charts/apexcharts.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/forms/select/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/pickers/pickadate/pickadate.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/editors/quill/katex.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/editors/quill/monokai-sublime.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/editors/quill/quill.snow.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/charts/apexcharts.css')}}">
    <link rel="stylesheet" type="text/css" href=" {{asset('Admin/app-assets/vendors/css/extensions/tether-theme-arrows.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/extensions/tether.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/extensions/shepherd-theme-default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/file-uploaders/dropzone.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/pages/app-chat.css')}}">

    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href=" {{asset('Admin/app-assets/css'.$dir.'/colors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/plugins/forms/validation/form-validation.css')}}">

    <link rel="stylesheet" type="text/css" href=" {{asset('Admin/app-assets/css'.$dir.'/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/themes/dark-layout.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/themes/semi-dark-layout.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/core/menu/menu-types/horizontal-menu.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/plugins/file-uploaders/dropzone.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/pages/data-list-view.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/pages/dashboard-analytics.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/pages/card-analytics.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/plugins/tour/tour.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/pages/app-user.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/pages/app-email.css')}}">
    <!-- END: Page CSS-->


    <!-- BEGIN: csrf-token-->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- END: csrf-token-->


    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css'.$dir.'/custom'.$dir.'.css')}}">
    <link rel="stylesheet" type="text/css" href=" {{asset('Admin/assets/css/style.css')}}">
    <!-- END: Custom CSS-->
    @yield('styles')
</head>

<body class="vertical-layout vertical-menu-modern {{ session()->get('them') }}-layout 2-columns  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="dark-layout">


<!-- BEGIN: Header-->
@include('admin.includes.header')
<!-- END: Header-->


<!-- BEGIN: Side bar-->
@include('admin.includes.sidebar')
<!-- END: Side bar-->

<!-- Main Content -->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        @yield('content')
    </div>
</div>

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Vendor JS-->
<script src="{{asset('Admin/app-assets/vendors/js/vendors.min.js')}}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{asset('Admin/app-assets/vendors/js/tables/ag-grid/ag-grid-community.min.noStyle.js')}}"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{asset('Admin/app-assets/js/core/app-menu.js')}}"></script>
<script src="{{asset('Admin/app-assets/js/core/app.js')}}"></script>
<script src="{{asset('Admin/app-assets/js/scripts/components.js')}}"></script>
<!-- END: Theme JS-->

<!-- SweetAlert js -->
<script src="{{asset('Admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}" type="text/javascript"></script>
<script src="{{asset('Admin/app-assets/js/scripts/extensions/sweet-alerts.js')}}" type="text/javascript"></script>

<!-- toastr js -->
<script src="{{ asset('includes/toastr.min.js') }}"></script>
<script src="{{ asset('Admin/assets/js/scripts.js') }}"></script>
<!-- End of toastr js -->

<script src="{{ asset('Admin/app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('Admin/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
<script src="{{ asset('Admin/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('Admin/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
<script src="{{ asset('Admin/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
<script src="{{ asset('Admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('Admin/app-assets/js/scripts/ui/data-list-view.js') }}"></script>

<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    @if(session()->has('success'))
    fireSuccess("{{ session()->get('success') }}")

    @elseif(session()->has('error'))
    fireError("{{ session()->get('error') }}")
    @endif
{{--    @if($errors->any())--}}
{{--    fireError("{{ $errors->first() }}")--}}
{{--    @endif--}}
</script>
<!-- Request Errors -->


@yield('scripts')

<!-- BEGIN: Page JS-->
<script src="{{asset('Admin/app-assets/js/scripts/pages/app-user.js')}}"></script>
<!-- END: Page JS-->
</body>
</html>

