@extends('admin.master')

@section('title', 'المدن')

@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الدول</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.countries.index') }}">الدول</a></li>
                            <li class="breadcrumb-item active">
                                <a href="{{ route('admin.countries.show', $region->country) }}">{{ $region->country?->name }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.countries.regions', $region->country) }}">المناطق</a>
                            </li>
                            <li class="breadcrumb-item active">
                                <a href="{{ route('admin.regions.show', $region) }}">{{ $region->name }}</a>
                            </li>
                            <li class="breadcrumb-item active">المدن</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <!-- Data list view starts -->
    <section id="data-list-view" class="data-list-view-header">
        <div class="action-btns d-none">
            <div class="btn-dropdown mr-1 mb-1">
                <div class="btn-group dropdown actions-dropodown">
                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('dashboard.main.Actions') }}
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('admin.regions.cities_create', $region) }}"><i class="feather icon-plus"></i>{{ __('dashboard.action.add') }}</a>
                        <a class="dropdown-item delete-all" href="#"><i class="feather icon-trash"></i>{{ __('dashboard.action.delete') }}</a>
                        <a class="dropdown-item action-download" href="{{ route('admin.cities.downloadExcel') }}" ><i class="feather icon-file"></i>{{ __('dashboard.action.download_excel') }}</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Data list view Ends -->

        <!-- DataTable starts -->
        <div class="table-responsive">
            <table class="table data-list-view">
                <thead>
                <tr>
                    <th></th>
                    @foreach(config('app.languages') as $key => $lang)
                        <th>{{ __('dashboard.main.name_in_' . $key) }}</th>
                    @endforeach
                    <th>{{ __('dashboard.main.Actions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cities as $city)
                    <tr id="city-{{$city->id}}">
                        <td data-id="{{ $city->id }}"></td>

                        @foreach(config('app.languages') as $key => $lang)
                            <td>{{ $city->getTranslation('name', $key) }}</td>
                        @endforeach

                        <td class="product-action">
                            <a href="{{ route('admin.cities.edit', $city) }}">
                                    <span data-id="{{ $city->id }}" >
                                        <i class="feather icon-edit"></i>
                                    </span>
                            </a>
                            <span class="action-delete" data-id="{{ $city->id }}" title="{{ __('dashboard.action.delete') }}">
                                    <i class="feather icon-trash"></i>
                                </span>

                            <a href="{{ route('admin.cities.show', $city) }}">
                                <span title="{{ __('dashboard.action.show') }}">
                                    <i class="feather icon-eye"></i>
                                </span>
                            </a>

                            <a href="{{ route('admin.cities.districts', $city) }}">
                                <span title="الاحياء">
                                    <i class="fa fa-map"></i>
                                </span>
                            </a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- DataTable ends -->

    </section>
    <!-- Data list view end -->

@endsection

@section('scripts')
    <script>

        var cities = [];

        // On Delete
        // confirm options
        $('.action-delete').on('click', function () {
            var city = $(this).data('id'),
                url = '{{ route("admin.cities.destroy", ":id") }}',
                newUrl = url.replace(':id', city);

            console.log( city);
            console.log( newUrl);

            Swal.fire({
                title: '{{ __('dashboard.alerts.do_you_want_to_delete_this_row') }}',
                // text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('dashboard.action.yes_delete') }}',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'DELETE',
                        // _token: "{{ csrf_token() }}",
                        success: function(response) {
                            fireSuccess('{{ __('dashboard.alerts.deleted')  }}')
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })
                            // Remove the raw
                            $('#city-' + city).remove();
                        }
                    });
                }
            })
        });

        $('.delete-all').click(function () {
            var cities = [];
            $('.dt-checkboxes:checked').each(function() {
                cities.push( $(this).parent().data('id') );
                // console.log($(this).parent().data('city-id') )
            });
            console.log(cities);
            // console.log(cities.length);
            if(cities.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: '{{ __('dashboard.alerts.do_you_want_to_delete_selected_data') }}',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{ __('dashboard.action.yes_delete_selected') }}',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.cities.destroy_selected') }}",
                            method: 'POST',
                            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            // _token: "{{ csrf_token() }}",
                            data: {
                                cities
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: '{{ __('dashboard.alerts.deleted')  }}',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                cities.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#city-' + element).remove();
                                } );

                                // console.log(cities)
                            }
                        });
                    }
                })

            }

        });

    </script>
@endsection
