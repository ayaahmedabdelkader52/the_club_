@extends('admin.master')

@section('title', 'عرض مدينة')

@section('content')

    <!-- BEGIN: Content-->

    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الدول</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="{{ route('admin.countries.index') }}">الدول</a></li>
                            <li class="breadcrumb-item active">
                                <a href="{{ route('admin.countries.show', $city->region->country) }}">{{ $city->region->country->name }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.countries.regions', $city->region->country) }}">المناطق</a>
                            </li>
                            <li class="breadcrumb-item active">
                                <a href="{{ route('admin.regions.show', $city->region) }}">{{ $city->region->name }}</a>
                            </li>
                            <li class="breadcrumb-item active">
                                <a href="{{ route('admin.regions.cities', $city->region) }}">المدن</a>
                            </li>
                            <li class="breadcrumb-item active">عرض مدينة {{ $city->region->country->name }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        {{--        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">--}}
        {{--            <div class="form-group breadcrum-right">--}}
        {{--                <div class="dropdown">--}}
        {{--                    <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>--}}
        {{--                    <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">عرض تفاصيل المدينة</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                                        <table>
                                            <tr>
                                                <td class="font-weight-bold">{{ __('dashboard.main.name_in_ar') }}</td>
                                                <td>{{ $city->getTranslation('name', 'ar') }}</td>
                                            </tr>

                                        </table>
                                    </div>
                                    <div class="col-12 col-md-12 col-lg-5">
                                        <table class="ml-0 ml-sm-0 ml-lg-0">
                                            <tr>
                                                <td class="font-weight-bold">{{ __('dashboard.main.name_in_en') }}</td>
                                                <td>{{ $city->getTranslation('name', 'en') }}</td>
                                            </tr>

                                        </table>
                                    </div>
                                    <div class="col-12">
                                        <a href="{{ route('admin.cities.edit', $city) }}" class="btn btn-primary mr-1">
                                            <i class="feather icon-edit-1"></i> تعديل
                                        </a>
                                        <a class="btn btn-outline-danger action-delete"><i class="feather icon-trash-2"></i> حذف</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- account end -->
            </div>
        </section>
        <!-- page users view end -->

    </div>
    <!-- END: Content-->
@endsection

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // On Delete
        // confirm options
        $('.action-delete').on('click', function () {

            Swal.fire({
                title: '{{ __('dashboard.alerts.do_you_want_to_delete_this_row') }}',
                // text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('dashboard.action.yes_delete') }}',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: "{{ route('admin.cities.destroy', $city) }}",
                        method: 'DELETE',
                        // _token: "{{ csrf_token() }}",
                        success: function (response) {
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })
                            setTimeout(function () {
                                window.location.href = '{{ route('admin.regions.cities', $city->region) }}';
                            }, 1500)

                        }
                    });
                }
            })
        });
    </script>

@stop
