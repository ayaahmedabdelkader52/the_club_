@extends('admin.master')
@section('title')
    تفاصيل الطلب
@stop
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الطلبات</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.orders.index') }}">الطلبات</a></li>
                            <li class="breadcrumb-item active">تفاصيل الطلب</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->
    <section class="page-users-view">
        <div class="row">
            <!-- account start -->
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">عرض الطلب</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">

                            <div class="row">

                                <div  class="col-md-6 col-12 card" >
                                        <img src="{{$order->doctor->avatarPath}}" style="width: 200px" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h5 class="card-title" style="align-content: center;" >اسم الطبيب:{{$order->doctor->name}}</h5>
{{--                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--}}
                                            <a href="{{ route('admin.users.show', $user =$order->doctor) }}" class="btn btn-primary">View user</a>
                                        </div>
                                    </div>

                                <div class="col-md-6 col-12 card" >
                                        <img src="{{$order->user->avatarPath}}" style="width: 200px" class="card-img-top" >
                                        <div class="card-body">
                                            <h5 class="card-title" style="align-content: center;" >اسم المريض:{{$order->user->name}}</h5>
{{--                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--}}
                                            <a href="{{ route('admin.users.show', $user =$order->user) }}" class="btn btn-primary">View user</a>
                                        </div>
                                    </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-label-group form-group">
                                        <input type="text" id="total" class="form-control" value="{{$order->hours_per_day}}"
                                               disabled autofocus>
                                        <label for="total">السعر في الساعه الواحده</label>
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-label-group form-group">
                                        <input type="text" id="total" class="form-control" value="@if($order->status = 'new') جديد@elseif($order->status = 'active')مفعل
                                        @elseif($order->status = 'have_finished')منتهي من احد الاطراف
                                        @else منتهي @endif" disabled autofocus>
                                        <label for="total">الحالة</label>
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-label-group form-group">
                                        <input type="text" id="total" class="form-control" value="{{$order->start}}"
                                               disabled autofocus>
                                        <label for="total">البدايه</label>
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-label-group form-group">
                                        <input type="text" id="total" class="form-control" value="{{$order->end}}"
                                               disabled autofocus>
                                        <label for="total">النهاية</label>
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-label-group form-group">
                                        <input type="text" id="total" class="form-control" value="{{$order->type}}"
                                               disabled autofocus>
                                        <label for="total">النوع</label>
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-label-group form-group">
                                        <textarea type="date" id="total" class="form-control"
                                                  disabled autofocus>
                                            @foreach($days as $day)
                                                {{$day->name_ar}}
                                            @endforeach
{{--                                            {{$order->days->name_ar}}--}}
                                        </textarea>
                                        <label for="total">الايام</label>
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-label-group form-group">
                                        <input type="text" id="total" class="form-control" value="{{$order->total_price}}"
                                               disabled autofocus>
                                        <label for="total">الاجمالي</label>
                                    </div>
                                </div>

                            </div>

{{--                                <table class="table data-list-view">--}}
{{--                                        <thead>--}}
{{--                                        <tr>--}}
{{--                                            <th></th>--}}
{{--                                            <th>اسم الطبيب</th>--}}
{{--                                            <th>اسم المريض</th>--}}
{{--                                            <th>السعر في الساعه </th>--}}
{{--                                            <th>الاجمالي </th>--}}
{{--                                            <th>الحالة </th>--}}
{{--                                            <th>البداية </th>--}}
{{--                                            <th>النهاية </th>--}}
{{--                                            <th>الايام </th>--}}
{{--                                            <th>عدد الايام </th>--}}
{{--                                            <th>عدد الساعات في اليوم </th>--}}
{{--                                            <th>الاجراءت</th>--}}
{{--                                        </tr>--}}
{{--                                        </thead>--}}
{{--                                        <tbody>--}}
{{--                                            <tr id="order-{{$order->id}}">--}}
{{--                                                <td data-order-id="{{$order->id}}"></td>--}}
{{--                                                <td>{{$order->doctors->name}}</td>--}}
{{--                                                <td>{{$order->users->name}}</td>--}}
{{--                                                <td>{{$order->totalprice}}</td>--}}
{{--                                                <td>{{$order->totalprice / ($order->hours_per_day * $order->total_days)}}</td>--}}
{{--                                                <td>{{$order->status}}</td>--}}
{{--                                                <td>{{$order->total_days}}</td>--}}
{{--                                                <td>{{$order->hours_per_day}}</td>--}}
{{--                                                <td>--}}

                                                    {{--                            <a href="{{route('admin.orders.edit',$category)}}">--}}
                                                    {{--                                <i class="feather icon-edit"></i>--}}
                                                    {{--                            </a>--}}




                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
