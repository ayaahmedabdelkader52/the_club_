@extends('admin.master')
@section('title')
    الطلبات
@stop
@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الطلبات المعلقه</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.orders.index') }}">الطلبات</a></li>
                            <li class="breadcrumb-item active">الطلبات المعلقه</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <!-- Data list view starts -->
    <section id="data-list-view" class="data-list-view-header">
        <div class="action-btns d-none">
            <div class="btn-dropdown mr-1 mb-1">
                <div class="btn-group dropdown actions-dropodown">
                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('dashboard.main.Actions') }}
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item delete-all" href="#"><i class="feather icon-trash"></i>حذف
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Data list view Ends -->

        <!-- DataTable starts -->
        <div class="table-responsive">
            <table class="table data-list-view">
                <thead>
                <tr>
                    <th></th>
                    <th>اسم الطبيب</th>
                    <th>اسم المريض</th>
{{--                    <th>السعر في الساعه </th>--}}
                    <th>الاجمالي </th>
                    <th>الحالة </th>
{{--                    <th>البداية </th>--}}
{{--                    <th>النهاية </th>--}}
{{--                    <th>الايام </th>--}}
                    <th>عدد الايام </th>
                    <th>عدد الساعات في اليوم </th>
                    <th>الاجراءت</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr id="order-{{$order->id}}">
                        <td data-order-id="{{$order->id}}"></td>
                        <td>{{$order->doctor->name}}</td>
                        <td>{{$order->user->name}}</td>
                        <td>{{$order->total_price}}</td>
                        <td>@if($order->status = 'new') جديد@elseif($order->status = 'active')مفعل
                            @elseif($order->status = 'have_finished')منتهي من احد الاطراف
                            @else منتهي @endif</td>
                        <td>{{$order->total_days}}</td>
                        <td>{{$order->hours_per_day}}</td>
                        <td>

{{--                            <a href="{{route('admin.orders.edit',$category)}}">--}}
{{--                                <i class="feather icon-edit"></i>--}}
{{--                            </a>--}}

                            <a href="#">
                                 <span class="action-delete" data-category-id="{{ $order->id }}">
                                    <i class="feather icon-trash"></i>
                                 </span>
                            </a>
                            <a href="{{route('admin.orders.show' , $order)}}">
                                 <span>
                                    <i class="feather icon-eye"></i>
                                 </span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- DataTable ends -->
    </section>
    <!-- Data list view end -->
@endsection
@section('scripts')
    <script>
        var selectedAds = [];

    $('.action-delete').on('click', function () {
    var orderId = $(this).data('order-id'),
    url = '{{ route("admin.orders.destroy", ":id") }}',
    newUrl = url.replace(':id', orderId);

    // console.log(categoryId);
    // console.log( newUrl);

    Swal.fire({
    title: 'هل تريد حذف السؤال ؟',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'نعم احذفه ',
    confirmButtonClass: 'btn btn-primary',
    cancelButtonClass: 'btn btn-danger ml-1',
    cancelButtonText: 'الغاء',
    buttonsStyling: false,
    }).then(function (result) {
    if (result.value) {
    $.ajax({
    url: newUrl,
    method: 'GET',
    // _token: "{{ csrf_token() }}",
    success: function(response) {
    fireSuccess('{{ __('dashboard.alerts.deleted')  }}')
    Swal.fire({
    position: 'top-start',
    type: 'success',
    title: '{{ __('dashboard.alerts.deleted')  }}',
    showConfirmButton: false,
    timer: 1500,
    confirmButtonClass: 'btn btn-primary',
    buttonsStyling: false,
    })
    // Remove the raw
    $('#order-' + orderId).remove();
    }
    });
    }
    })
    });
    </script>

    <script>
          $('.delete-all').click(function () {
            var selectedorders = [];
            $('.dt-checkboxes:checked').each(function() {
                selectedorders.push( $(this).parent().data('order-id') );
            });
            console.log(selectedorders.length);
            if(selectedorders.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: 'هل تريد حذف الاقسام المحدده',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم احذف المحدد',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: 'الغاء',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.orders.destroy_selected') }}",
                            method: 'POST',
                             headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                             _token: "{{ csrf_token() }}",
                            data: {
                                ids: selectedorders
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: 'تم حذف المحدد بنجاح ',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                selectedorders.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#order-' + element).remove();
                                } );
                            }
                        });
                    }
                })

            }

        });
    </script>


@stop
