@extends('admin.master')
@section('title')
    قائمه الاعضاء
@stop
@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{ __('dashboard.user.users') }}</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">{{ __('dashboard.user.users') }}</a></li>
                            <li class="breadcrumb-item active">{{ __('dashboard.user.all_users') }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        {{--        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">--}}
        {{--            <div class="form-group breadcrum-right">--}}
        {{--                <div class="dropdown">--}}
        {{--                    <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>--}}
        {{--                    <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
    <!--end of content wrapper -->

    <!-- Data list view starts -->
    <section id="data-list-view" class="data-list-view-header">
        <div class="action-btns d-none">
            <div class="btn-dropdown mr-1 mb-1">
                <div class="btn-group dropdown actions-dropodown">
                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('dashboard.main.Actions') }}
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('admin.users.create') }}"><i class="feather icon-archive"></i>{{ __('dashboard.action.add') }}</a>
                        <a class="dropdown-item" data-toggle="modal" data-target="#message-modal-to-all"><i class="feather icon-send"></i>{{ __('dashboard.action.send_message_to_all') }}</a>
                        <a class="dropdown-item s" href="#"><i class="feather icon-trash"></i>{{ __('dashboard.action.delete') }}</a>
                        <a class="dropdown-item action-download" href="{{ route('admin.users.downloadUsers') }}" ><i class="feather icon-file"></i>{{ __('dashboard.action.download_excel') }}</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Data list view Ends -->

        <!-- DataTable starts -->
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th></th>
                    <th>{{ __('dashboard.user.avatar') }}</th>
                    <th>{{ __('dashboard.user.name') }}</th>
                    <th>{{ __('dashboard.user.phone') }}</th>
                    <th>{{ __('dashboard.user.email') }}</th>
                    <th>{{ __('dashboard.user.role') }}</th>
                    <th>{{ __('dashboard.main.Actions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr id="user-{{$user->id}}">
                        <td data-user-id="{{ $user->id }}"></td>
                        <td class="td-img">
                            <img src="{{ $user->avatarPath }}" alt="">
                        </td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->fullPhone }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <div class="chip @if($user->role_id == 1) chip-primary @else chip-success @endif">
                                <div class="chip-body">
                                    <div class="chip-text">{{ ($user->role) ? $user->role->name : '' }}</div>
                                </div>
                            </div>
                        </td>
                        <td class="product-action">
                            {{--                        <a href="#" class="action-edit">--}}
                            <a href="{{ route('admin.users.edit', $user) }}">
                            <span data-user-id="{{ $user->id }}" data-name="{{ $user->name }}"
                                  data-email="{{ $user->email }}" data-phone="{{ $user->fullPhone }}" data-role-id="{{ $user->role_id }}"
                                  data-address="{{ $user->address }}" data-status="{{ $user->status }}"
                                  data-avatar="{{ $user->avatarPath }}" title="{{ __('dashboard.action.edit') }}" >
                                <i class="feather icon-edit"></i>
                            </span>
                            </a>
                            <span class="action-delete" data-user-id="{{ $user->id }}" title="{{ __('dashboard.action.delete') }}">
                                <i class="feather icon-trash"></i>
                            </span>

                            <a href="{{ route('admin.users.show', $user) }}">
                            <span title="{{ __('dashboard.action.show') }}">
                                <i class="feather icon-eye"></i>
                            </span>
                            </a>

                            <span data-toggle="modal" data-target="#messageModal" class="send-message" data-user-id="{{ $user->id }}"
                                  title="{{ __('dashboard.action.send_message') }}">
                                <i class="feather icon-send"></i>
                            </span>

                            @if($user->status == 'block')
                                <span class="action-un-block" data-user-id="{{ $user->id }}" title="{{ __('dashboard.action.un_block') }}">
                                    <i class="feather icon-unlock"></i>
                                </span>
                            @else
                                <span class="action-block" data-user-id="{{ $user->id }}" title="{{ __('dashboard.action.block') }}">
                                    <i class="feather icon-lock"></i>
                                </span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <!-- DataTable ends -->


        <!-- Message starts -->
        @include('admin.users.includes.message-to-single-user')
        <!-- Message ends -->

        <!-- Message to all starts -->
        @include('admin.users.includes.message-to-all')
        <!-- Message to all ends -->

    </section>
    <!-- Data list view end -->

@endsection

@section('scripts')
    <script>
        var selectedUsers = [];

        // On Edit
        $('.action-edit').on("click",function(e){
            e.stopPropagation();
            var name = $(this).data('name'),
                avatar = $(this).data('avatar'),
                roleId = $(this).data('role-id'),
                userId = $(this).attr('data-user-id'),
                email = $(this).attr('data-email'),
                address = $(this).attr('data-address'),
                status = $(this).attr('data-status'),
                phone = $(this).attr('data-phone'),
                url = '{{ route("admin.users.update", ":id") }}',
                newUrl = url.replace(':id', userId);

            console.log(name);
            console.log( avatar );
            console.log( roleId );
            console.log( userId );
            console.log( email );
            console.log( address );
            console.log( status );
            console.log( phone );
            console.log( url );
            console.log( newUrl);

            $('#edit-user-form').attr('action', newUrl);

            $('#data-name').val(name);
            // $('#data-price').val(price);
            $(".add-new-data").addClass("show");
            $(".overlay-bg").addClass("show");
        });

        // On Delete
        // confirm options
        $('.action-delete').on('click', function () {
            var userId = $(this).data('user-id'),
                url = '{{ route("admin.users.destroy", ":id") }}',
                newUrl = url.replace(':id', userId);

            console.log( userId);
            console.log( newUrl);

            Swal.fire({
                title: '{{ __('dashboard.user.do_you_want_to_delete_this_user') }}',
                // text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('dashboard.action.yes_delete') }}',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'DELETE',
                        // _token: "{{ csrf_token() }}",
                        success: function(response) {
                            fireSuccess('{{ __('dashboard.alerts.deleted')  }}')
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })
                            console.log(response)
                            console.log(response.data.userId)
                            // Remove the raw
                            $('#user-' + userId).remove();
                        }
                    });
                }
            })
        });

        $('.delete-all').click(function () {
            selectedUsers = [];
            $('.dt-checkboxes:checked').each(function() {
                selectedUsers.push( $(this).parent().data('user-id') );
                // console.log($(this).parent().data('user-id') )
            });
            console.log(selectedUsers);
            // console.log(selectedUsers.length);
            if(selectedUsers.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: '{{ __('dashboard.user.do_you_want_to_delete_selected_users') }}',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{ __('dashboard.action.yes_delete_selected') }}',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.users.destroy_selected') }}",
                            method: 'POST',
                            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            // _token: "{{ csrf_token() }}",
                            data: {
                                users: selectedUsers
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: '{{ __('dashboard.alerts.deleted')  }}',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                selectedUsers.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#user-' + element).remove();
                                } );

                                // console.log(selectedUsers)
                                // console.log(response.data.userId)
                            }
                        });
                    }
                })

            }

        });

        $('.send-message').on('click', function() {
            var userId = $(this).data('user-id');
            // console.log('hello');
            // console.log(userId);

            $('.user-id').val(userId);
        });

        // On Block User
        $(document).on('click', '.action-block', function () {
            var userId = $(this).data('user-id'),
                url = '{{ route("admin.users.block_user", ":id") }}',
                thisElement = $(this),
                newUrl = url.replace(':id', userId);

            console.log( userId);
            console.log( newUrl);

            Swal.fire({
                title: 'هل انت متاكد من حظر هذا المستخدم ؟',
                // text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('dashboard.action.yes') }}',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'POST',
                        // _token: "{{ csrf_token() }}",
                        success: function(response) {
                            fireSuccess(response.msg);
                            // console.log(response);

                            thisElement.html('<i class="feather icon-unlock"></i>');
                            thisElement.addClass('action-un-block');
                            thisElement.removeClass('action-block');
                            thisElement.attr('title', '{{ __('dashboard.action.un_block') }}');
                        }
                    });
                }
            })
        });

        // On Un Block User
        $(document).on('click', '.action-un-block', function () {
            var userId = $(this).data('user-id'),
                url = '{{ route("admin.users.un_block_user", ":id") }}',
                thisElement = $(this),
                newUrl = url.replace(':id', userId);

            console.log( userId);
            console.log( newUrl);

            Swal.fire({
                title: 'هل انت متاكد من الغاء حظر هذا المستخدم ؟',
                // text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('dashboard.action.yes') }}',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'POST',
                        // _token: "{{ csrf_token() }}",
                        success: function(response) {
                            fireSuccess(response.msg);
                            // console.log(response);

                            thisElement.html('<i class="feather icon-lock"></i>');
                            thisElement.addClass('action-block');
                            thisElement.removeClass('action-un-block');
                            thisElement.attr('title', '{{ __('dashboard.action.block') }}');
                        }
                    });
                }
            })
        });

    </script>
@endsection
