@extends('admin.master')
@section('title')
    عرض تفاصيل العضو
@stop
@section('content')

    <!-- BEGIN: Content-->

    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{ __('dashboard.user.users') }}</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">{{ __('dashboard.user.users') }}</a></li>
                            <li class="breadcrumb-item active">{{ __('dashboard.action.show') }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        {{--        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">--}}
        {{--            <div class="form-group breadcrum-right">--}}
        {{--                <div class="dropdown">--}}
        {{--                    <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>--}}
        {{--                    <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">الحساب</div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="users-view-image">
                                    <img src="{{ $user->avatarPath }}" class="users-avatar-shadow w-100 rounded mb-2 pr-2 ml-1" alt="avatar">
                                </div>
                                <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                                    <table>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('dashboard.user.name') }}</td>
                                            <td>{{ $user->name }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('dashboard.user.email') }}</td>
                                            <td>{{ $user->email }}</td>
                                        </tr>

                                        <tr>
                                            <td class="font-weight-bold">{{ __('dashboard.user.address') }}</td>
                                            <td>{{ $user->address }}</td>
                                        </tr>

                                    </table>
                                </div>
                                <div class="col-12 col-md-12 col-lg-5">
                                    <table class="ml-0 ml-sm-0 ml-lg-0">
                                        <tr>
                                            <td class="font-weight-bold">{{ __('dashboard.user.status') }}</td>
                                            <td>{{ __('dashboard.user.' . $user->status) }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('dashboard.user.role') }}</td>
                                            <td>{{ ($user->role) ? $user->role->name : '' }}</td>
                                        </tr>


                                    </table>
                                </div>
                                <div class="col-12">
                                    <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-primary mr-1"><i class="feather icon-edit-1"></i> Edit</a>
                                    <a class="btn btn-outline-danger action-delete"><i class="feather icon-trash-2"></i> Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- account end -->

                <!-- permissions start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-header border-bottom mx-2 px-0">
                            <h6 class="border-bottom py-1 mb-0 font-medium-2"><i class="feather icon-lock mr-50 "></i>الصلاحيات</h6>
                        </div>
                        <div class="card-body px-75">
                            <div class="row">
                                <!-- Permissions -->
                                @foreach($superPermissions as $key => $permission)
                                    <div class="col col-md-4 col-sm-6 md-3 roll-checkk">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="brands">
                                                    <a class="filter-title mb-0 select-all-permissions">{{ $permission['title'] }}</a>
                                                    <div class="brand-list" id="brands">
                                                        <ul class="list-unstyled">
                                                            @foreach($permission['childrens'] as $child)
                                                                <li class="d-flex justify-content-between align-items-center py-25">
                                        <span class="vs-checkbox-con vs-checkbox-primary">
                                            <input name="permissions[]" value="{{ $child }}" disabled class="checkbox-input"
                                                   @if(in_array($child, $roleUserPermissions)) checked @endif
                                                   type="checkbox">
                                            <span class="vs-checkbox">
                                                <span class="vs-checkbox--check">
                                                    <i class="vs-icon feather icon-check"></i>
                                                </span>
                                            </span>
                                            <span class="">{{ __('routes.' . $child ) }}</span>
                                        </span>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                            <!-- /Permissions -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- permissions end -->
            </div>
        </section>
        <!-- page users view end -->

    </div>
    <!-- END: Content-->
@endsection

@section('scripts')
    <script>
        // On Delete
        // confirm options
        $('.action-delete').on('click', function () {
            var userId = $(this).data('user-id'),
                url = '{{ route("admin.users.destroy", ":id") }}',
                newUrl = url.replace(':id', userId);

            console.log(userId);
            console.log(newUrl);

            Swal.fire({
                title: '{{ __('dashboard.user.do_you_want_to_delete_this_user') }}',
                // text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('dashboard.action.yes_delete') }}',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'DELETE',
                        // _token: "{{ csrf_token() }}",
                        success: function (response) {
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })
                            console.log(response)
                            console.log(response.data.userId)
                            // Remove the raw
                            $('#user-' + response.data.userId).remove();
                        }
                    });
                }
            })
        });
    </script>

@stop
