@extends('admin.master')

@section('title', 'قائمه  الاطباء ')

@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0"> الاطباء </h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">{{ __('dashboard.user.users') }}</a></li>
                            <li class="breadcrumb-item active">قائمه الاطباء </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <!-- Data list view starts -->
    <section id="data-list-view" class="data-list-view-header">
        <div class="action-btns d-none">
            <div class="btn-dropdown mr-1 mb-1">
                <div class="btn-group dropdown actions-dropodown">
                    <div class="dropdown-menu">
                        <a class="dropdown-item delete-all" href="#"><i class="feather icon-trash"></i>{{ __('dashboard.action.delete') }}</a>
                        <a class="dropdown-item" href="{{ route('admin.users.create') }}"><i class="feather icon-archive"></i>{{ __('dashboard.action.add') }}</a>
                        <a class="dropdown-item" href="#"><i class="feather icon-file"></i>{{ __('dashboard.action.print') }}</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Data list view Ends -->

        <!-- DataTable starts -->
        <div class="table-responsive">
            <table class="table data-list-view">
                <thead>
                <tr>
                    <th></th>
                    <th>{{ __('dashboard.user.avatar') }}</th>
                    <th>{{ __('dashboard.user.name') }}</th>
                    <th>{{ __('dashboard.user.phone') }}</th>
                    <th>{{ __('dashboard.user.status') }}</th>
                    <th>{{ __('dashboard.user.email') }}</th>
{{--                    <th>مميز</th>--}}
                    <th>{{ __('dashboard.user.role') }}</th>
                    <th>{{ __('dashboard.main.Actions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr id="user-{{$user->id}}">
                        <td data-user-id="{{ $user->id }}"></td>
                        <td class="td-img">
                            <img src="{{ $user->avatarPath }}" alt="">
                        </td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->fullPhone }}</td>
                        <td>{{ $user->status }}</td>
                        <td>{{ $user->email }}</td>
{{--                        <td>@if($user->star == 'true')--}}
{{--                                <i class="feather icon-star"></i>--}}
{{--                            @else--}}
{{--                                ليس مميز--}}
{{--                            @endif</td>--}}
{{--                        <td>--}}
                            <div class="chip @if($user->role_id == 1) chip-primary @else chip-success @endif">
                                <div class="chip-body">
                                    <div class="chip-text">{{ ($user->role) ? $user->role->name : '' }}</div>
                                </div>
                            </div>
                        </td>
                        <td class="product-action">
                            {{--                        <a href="#" class="action-edit">--}}
                            <a href="{{ route('admin.users.edit', $user) }}">
                            <span data-user-id="{{ $user->id }}" data-name="{{ $user->name }}"
                                  data-email="{{ $user->email }}" data-phone="{{ $user->fullPhone }}" data-role-id="{{ $user->role_id }}"
                                  data-address="{{ $user->address }}" data-status="{{ $user->status }}"
                                  data-avatar="{{ $user->avatarPath }}" title="{{ __('dashboard.action.edit') }}" >
                                <i class="feather icon-edit"></i>
                            </span>
                            </a>
                            <span class="action-delete" data-user-id="{{ $user->id }}" title="{{ __('dashboard.action.delete') }}">
                            <i class="feather icon-trash"></i>
                        </span>

                            <a href="{{ route('admin.users.show', $user) }}">
                            <span title="{{ __('dashboard.action.show') }}">
                            <i class="feather icon-eye"></i>
                        </span>
                            </a>

                            <span data-toggle="modal" data-target="#messageModal" title="{{ __('dashboard.action.send_message') }}">
                            <i class="feather icon-send"></i>
                        </span>
                            <span class="action-un-block" data-user-id="{{ $user->id }}" title="{{ __('dashboard.action.un_block') }}">
                                <i class="feather icon-unlock"></i>
                            </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- DataTable ends -->


        <!-- Message starts -->
        <div class="modal fade text-left" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel33">{{ __('dashboard.user.send_message') }} </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="sms-message-fill" data-toggle="tab" href="#sms-message" role="tab" aria-controls="sms-message" aria-selected="true">{{ __('dashboard.messages.sms_send') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="email-message-fill" data-toggle="tab" href="#email-message" role="tab" aria-controls="email-message" aria-selected="false">{{ __('dashboard.messages.email_send') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="notification-message-fill" data-toggle="tab" href="#notification-message" role="tab" aria-controls="notification-message" aria-selected="false">{{ __('dashboard.messages.notification_send') }}</a>
                        </li>
                    </ul>

                    <div class="tab-content pt-1">
                        <div class="tab-pane active" id="sms-message" role="tabpanel" aria-labelledby="sms-message-fill">
                            <form action="#">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <fieldset class="form-group">
                                                <textarea name="sms_message_content" class="form-control" id="basicTextarea" rows="3" placeholder="{{ __('dashboard.messages.sms_content') }}"></textarea>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">{{ __('dashboard.action.send') }}</button>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="email-message" role="tabpanel" aria-labelledby="email-message-fill">
                            <form action="#">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <fieldset class="form-group">
                                                <textarea name="email_message_content" class="form-control" id="basicTextarea" rows="3" placeholder="{{ __('dashboard.messages.email_content') }}"></textarea>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">{{ __('dashboard.action.send') }}</button>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="notification-message" role="tabpanel" aria-labelledby="notification-message-fill">
                            <form action="#">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <fieldset class="form-group">
                                                <textarea name="notification_message_content" class="form-control" id="basicTextarea" rows="3" placeholder="{{ __('dashboard.messages.notification_content') }}"></textarea>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">{{ __('dashboard.action.send') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Message ends -->


        <!-- add new sidebar starts -->
        <div class="add-new-data-sidebar">
            <div class="overlay-bg"></div>
            <div class="add-new-data">
                <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
                    <div>
                        <h4 class="text-uppercase">Edit user</h4>
                    </div>
                    <div class="hide-data-sidebar">
                        <i class="feather icon-x"></i>
                    </div>
                </div>
                <div class="data-items pb-3">
                    <div class="data-fields px-2 mt-3">
                        <form id="edit-user-form" action="" method="post">
                            @csrf
                            @method('put')
                            <div class="row">
                                <div class="col-sm-12 data-field-col">
                                    <label for="data-name">Name</label>
                                    <input type="text" class="form-control" id="data-name">
                                </div>
                                <div class="col-sm-12 data-field-col">
                                    <label for="data-category"> Category </label>
                                    <select class="form-control" id="data-category">
                                        <option>Audio</option>
                                        <option>Computers</option>
                                        <option>Fitness</option>
                                        <option>Appliance</option>
                                    </select>
                                </div>
                                <div class="col-sm-12 data-field-col">
                                    <label for="data-status">Order Status</label>
                                    <select class="form-control" id="data-status">
                                        <option>Pending</option>
                                        <option>Canceled</option>
                                        <option>Delivered</option>
                                        <option>On Hold</option>
                                    </select>
                                </div>
                                <div class="col-sm-12 data-field-col">
                                    <label for="data-price">Price</label>
                                    <input type="text" class="form-control" id="data-price">
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <div class="add-data-footer d-flex justify-content-around px-3 mt-2">
                    <div class="add-data-btn">
                        <button class="btn btn-primary">Add Data</button>
                    </div>
                    <div class="cancel-data-btn">
                        <button class="btn btn-outline-danger">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- add new sidebar ends -->
    </section>
    <!-- Data list view end -->

@endsection

@section('scripts')
    <script>
        var selectedUsers = [];

        // On Edit
        $('.action-edit').on("click",function(e){
            e.stopPropagation();
            var name = $(this).data('name'),
                avatar = $(this).data('avatar'),
                roleId = $(this).data('role-id'),
                userId = $(this).attr('data-user-id'),
                email = $(this).attr('data-email'),
                address = $(this).attr('data-address'),
                status = $(this).attr('data-status'),
                phone = $(this).attr('data-phone'),
                url = '{{ route("admin.users.update", ":id") }}',
                newUrl = url.replace(':id', userId);

            console.log(name);
            console.log( avatar );
            console.log( roleId );
            console.log( userId );
            console.log( email );
            console.log( address );
            console.log( status );
            console.log( phone );
            console.log( url );
            console.log( newUrl);

            $('#edit-user-form').attr('action', newUrl);

            $('#data-name').val(name);
            // $('#data-price').val(price);
            $(".add-new-data").addClass("show");
            $(".overlay-bg").addClass("show");
        });

        // On Delete
        // confirm options
        $('.action-delete').on('click', function () {
            var userId = $(this).data('user-id'),
                url = '{{ route("admin.users.destroy", ":id") }}',
                newUrl = url.replace(':id', userId);

            console.log( userId);
            console.log( newUrl);

            Swal.fire({
                title: '{{ __('dashboard.user.do_you_want_to_delete_this_user') }}',
                // text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('dashboard.action.yes_delete') }}',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'DELETE',
                        // _token: "{{ csrf_token() }}",
                        success: function(response) {
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })
                            console.log(response)
                            console.log(response.data.userId)
                            // Remove the raw
                            $('#user-' + response.data.userId).remove();
                        }
                    });
                }
            })
        });

        $('.delete-all').click(function () {
            selectedUsers = [];
            $('.dt-checkboxes:checked').each(function() {
                selectedUsers.push( $(this).parent().data('user-id') );
                // console.log($(this).parent().data('user-id') )
            });
            console.log(selectedUsers);
            // console.log(selectedUsers.length);
            if(selectedUsers.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: '{{ __('dashboard.user.do_you_want_to_delete_selected_users') }}',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{ __('dashboard.action.yes_delete_selected') }}',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.users.destroy_selected') }}",
                            method: 'POST',
                            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            // _token: "{{ csrf_token() }}",
                            data: {
                                users: selectedUsers
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: '{{ __('dashboard.alerts.deleted')  }}',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                console.log(response)
                                console.log(response.data.userId)
                                // Remove the raw
                                $('#user-' + response.data.userId).remove();
                            }
                        });
                    }
                })

            }

        });

        // On Un Block User
        $(document).on('click', '.action-un-block', function () {
            var userId = $(this).data('user-id'),
                url = '{{ route("admin.users.un_block_user", ":id") }}',
                thisElement = $(this),
                newUrl = url.replace(':id', userId);

            console.log( userId);
            console.log( newUrl);

            Swal.fire({
                title: 'هل انت متاكد من الغاء حظر هذا المستخدم ؟',
                // text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('dashboard.action.yes') }}',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'POST',
                        // _token: "{{ csrf_token() }}",
                        success: function(response) {
                            fireSuccess(response.msg);
                            // console.log(response);

                            // Remove the raw
                            $('#user-' + userId).remove();
                        }
                    });
                }
            })
        });

    </script>
@endsection
