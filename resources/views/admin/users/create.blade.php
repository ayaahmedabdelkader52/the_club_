@extends('admin.master')
@section('title', trans('dashboard.main.Create'))
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css/plugins/forms/validation/form-validation.css')}}">
    <style>

    </style>
@stop

@section('content')

    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{ __('dashboard.user.users') }}</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">{{ __('dashboard.user.users') }}</a></li>
                            <li class="breadcrumb-item active">{{ __('dashboard.action.create') }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        {{--        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">--}}
        {{--            <div class="form-group breadcrum-right">--}}
        {{--                <div class="dropdown">--}}
        {{--                    <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>--}}
        {{--                    <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
    <!--end of content wrapper -->
    <div class="content-body">
        <!-- // Basic multiple Column Form section start -->
        <section id="multiple-column-form">
            <div class="row match-height">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{ __('dashboard.user.edit') }}</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form" action="{{ route('admin.users.store') }}" method="POST" enctype="multipart/form-data" >
                                @csrf

                                <!-- You can all alert messages by removing the comment -->
                                    {{--                                    @include('admin.includes.alerts.all-errors')--}}

                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <input type="text" id="first-name-column" class="form-control" name="name"
                                                           placeholder="{{ __('dashboard.user.name') }}" value="{{ old('name') }}">
                                                    <label for="first-name-column">{{ __('dashboard.user.name') }}</label>

                                                    @include('admin.includes.alerts.input-errors', ['input' => 'name'])
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <input type="email" id="last-name-column" class="form-control" name="email"
                                                           placeholder="{{ __('dashboard.user.email') }}" value="{{ old('email') }}">
                                                    <label for="last-name-column">{{ __('dashboard.user.email') }}</label>
                                                    @include('admin.includes.alerts.input-errors', ['input' => 'email'])
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <input type="password" id="last-name-column" class="form-control" name="password" placeholder="{{ __('dashboard.user.password') }}">
                                                    <label for="last-name-column">{{ __('dashboard.user.password') }}</label>

                                                    @include('admin.includes.alerts.input-errors', ['input' => 'password'])
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <input type="password" id="last-name-column" class="form-control" name="password_confirmation" placeholder="{{ __('dashboard.user.confirm_password') }}">
                                                    <label for="last-name-column">{{ __('dashboard.user.confirm_password') }}</label>

                                                    @include('admin.includes.alerts.input-errors', ['input' => 'confirm_password'])
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <input type="text" id="city-column" class="form-control" name="phone"
                                                           placeholder="{{ __('dashboard.user.phone') }}" value="{{ old('phone') }}">
                                                    <label for="city-column">{{ __('dashboard.user.phone') }}</label>

                                                    @include('admin.includes.alerts.input-errors', ['input' => 'phone'])
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <select class="form-control" name="role_id">
                                                        @foreach($roles as $role)
                                                            <option value="{{ $role->id }}" @if(old('role_id') == $role->id ) selected @endif >{{ $role->name }}</option>
                                                        @endforeach
                                                    </select>

                                                    <label for="country-floating">{{ __('dashboard.user.role') }}</label>
                                                    @include('admin.includes.alerts.input-errors', ['input' => 'role_id'])
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <input type="text" id="company-column" class="form-control" name="address"
                                                           placeholder="{{ __('dashboard.user.address') }}" value="{{ old('address') }}">
                                                    <label for="company-column">{{ __('dashboard.user.address') }}</label>

                                                    @include('admin.includes.alerts.input-errors', ['input' => 'address'])
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <div class="form-label-group form-group">
                                                    <select class="form-control" name="status">
                                                        <option value="active" @if(old('status') == 'active' ) selected @endif >{{ __('dashboard.user.active') }}</option>
                                                        <option value="pending" @if(old('status') == 'pending' ) selected @endif >{{ __('dashboard.user.pending') }}</option>
                                                        <option value="block" @if(old('status') == 'block' ) selected @endif >{{ __('dashboard.user.block') }}</option>
                                                    </select>
                                                    <label for="email-id-column">{{ __('dashboard.user.status') }}</label>

                                                    @include('admin.includes.alerts.input-errors', ['input' => 'status'])
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12 ">
                                                <div class="form-label-group form-group">
                                                    <input type="file" id="image" class="form-control" name="avatar" placeholder="{{ __('dashboard.user.avatar') }}">
                                                    <label for="email-id-column">{{ __('dashboard.user.avatar') }}</label>

                                                    <div class="form-label-group">
                                                        <div class="multi-img-result">
                                                            <div class="img-uploaded">
                                                                <img src="{{ asset('assets/uploads/users/default.png') }}" alt="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @include('admin.includes.alerts.input-errors', ['input' => 'avatar'])
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{ __('dashboard.add') }}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // Basic Floating Label Form section end -->
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('includes/image-preview-2.js') }}"></script>
@stop
