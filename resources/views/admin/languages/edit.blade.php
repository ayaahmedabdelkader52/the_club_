@extends('admin.master')
@section('title')
    تعديل
@stop


@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">اللغات </h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.languages.index') }}"> اللغات </a></li>
                            <li class="breadcrumb-item active">نعديل اللغه</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <div class="content-body">
        <!-- // Basic multiple Column Form section start -->
        <section id="multiple-column-form">
            <div class="row match-height">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">تعديل اللغه</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form" action="{{ route('admin.languages.update',$language) }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-body">
                                        <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-label-group form-group">
                                                        <input type="text" class="form-control" name="title_ar"
                                                               placeholder="{{ __('dashboard.main.name_in_ar') }}" autofocus
                                                               value="{{ $language -> name_ar }}">
                                                        <label for="ar-name">{{ __('dashboard.main.name_in_ar') }}</label>
                                                        @include('admin.includes.alerts.input-errors', ['input' => "title_ar"])
                                                    </div>
                                                </div>
                                            <div class="col-md-6 col-12">
                                                    <div class="form-label-group form-group">
                                                        <input type="text" class="form-control" name="title_en"
                                                               placeholder="{{ __('dashboard.main.name_in_en') }}" autofocus
                                                               value="{{ $language -> name_en }}">
                                                        <label for="en-name">{{ __('dashboard.main.name_in_en') }}</label>
                                                        @include('admin.includes.alerts.input-errors', ['input' => "title_en"])
                                                    </div>
                                                </div>

                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">تعديل</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // Basic Floating Label Form section end -->
    </div>

@stop

