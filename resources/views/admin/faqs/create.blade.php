@extends('admin.master')
@section('title')
    اضافه سؤال
@stop
@section('before-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/css/plugins/forms/validation/form-validation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/pickers/pickadate/pickadate.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/editors/quill/katex.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/editors/quill/monokai-sublime.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/editors/quill/quill.snow.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Admin/app-assets/vendors/css/editors/quill/quill.bubble.css')}}">
@stop
@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الاسئله الشائعه</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.faqs.index') }}">الاسئله الشائعه</a></li>
                            <li class="breadcrumb-item active">اضافه</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <div class="content-body">
        <!-- // Basic multiple Column Form section start -->
        <section id="multiple-column-form">
            <div class="row match-height">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">اضافه الاسئله الشائعه</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form" action="{{ route('admin.faqs.store') }}" method="POST">
                                    @csrf

                                    <div class="form-body">
                                        <div class="row">
                                            @foreach(config('app.languages') as $key => $language)
                                                <div class="col-md-6 col-12">
                                                    <div class="form-label-group form-group">
                                                        <input type="text" class="form-control" name="question[{{$key}}]"
                                                               placeholder="{{ __('dashboard.main.question_in_' . $key) }}" autofocus
                                                               value="{{ old("question.$key") }}">
                                                        <label for="{{$key}}-name">{{ __('dashboard.main.question_in_' . $key) }}</label>
                                                        @include('admin.includes.alerts.input-errors', ['input' => "question.$key"])
                                                    </div>
                                                </div>
                                            @endforeach

                                                @foreach(config('app.languages') as $key => $language)
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-label-group form-group">
                                                            <textarea class="form-control" id="answer[{{$key}}]" name="answer[{{$key}}]">{!! old("answer.$key") !!}</textarea>
                                                            <label for="{{$key}}-answer">{{ __('dashboard.main.answer_in_' . $key) }}</label>

                                                            @include('admin.includes.alerts.input-errors', ['input' => "answer.$key"])
                                                        </div>
                                                    </div>
                                                @endforeach

                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">اضافه</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // Basic Floating Label Form section end -->
    </div>

@stop
@section('scripts')
    <script src="{{asset('Admin/app-assets/vendors/js/pickers/pickadate/picker.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/pickers/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/pickers/pickadate/picker.time.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/pickers/pickadate/legacy.js')}}"></script>

    <script src="{{asset('Admin/app-assets/vendors/js/editors/quill/katex.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/editors/quill/highlight.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/editors/quill/quill.min.js')}}"></script>
    <script src="{{ asset('includes/ckeditor/ckeditor.js') }}"></script>

    <script>
          $('.format-picker').pickadate({
        format: 'yyyy-mm-dd'
    });

    </script>
    <script>
        CKEDITOR.replace('answer[ar]',{
            contentsLangDirection:'rtl',
            language: '{{ app()->getLocale() }}',
            removePlugins:'image,flash,iframe,smiley,about',

        });
        CKEDITOR.replace('answer[en]',{
            contentsLangDirection:'ltr',
            language: '{{ app()->getLocale() }}',
            removePlugins:'image,flash,iframe,smiley,about',

        });

    </script>
{{--    <script>--}}

{{--    	var quill = new Quill('.quill_editor', {--}}
{{--    	modules: {--}}
{{--      'formula': true,--}}
{{--      'syntax': true,--}}
{{--      'toolbar': [--}}
{{--        [{--}}
{{--          'font': []--}}
{{--        }, {--}}
{{--          'size': []--}}
{{--        }],--}}
{{--        ['bold', 'italic', 'underline', 'strike'],--}}
{{--        [{--}}
{{--          'color': []--}}
{{--        }, {--}}
{{--          'background': []--}}
{{--        }],--}}
{{--        [{--}}
{{--          'script': 'super'--}}
{{--        }, {--}}
{{--          'script': 'sub'--}}
{{--        }],--}}
{{--        [{--}}
{{--          'header': '1'--}}
{{--        }, {--}}
{{--          'header': '2'--}}
{{--        }, 'blockquote', 'code-block'],--}}
{{--        [{--}}
{{--          'list': 'ordered'--}}
{{--        }, {--}}
{{--          'list': 'bullet'--}}
{{--        }, {--}}
{{--          'indent': '-1'--}}
{{--        }, {--}}
{{--          'indent': '+1'--}}
{{--        }],--}}
{{--        ['direction', {--}}
{{--          'align': []--}}
{{--        }],--}}
{{--        ['link', 'image', 'video', 'formula'],--}}
{{--        ['clean']--}}
{{--      ],--}}
{{--    },--}}
{{--        		theme: 'snow'--}}
{{--    	});--}}
{{--    	var form = document.getElementById("terms-form");--}}
{{--    form.onsubmit = function() {--}}
{{--        document.getElementById("quill_html").value = quill.root.innerHTML;--}}
{{--    }--}}
{{--</script>--}}

@stop
