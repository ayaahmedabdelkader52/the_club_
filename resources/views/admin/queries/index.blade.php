@extends('admin.master')
@section('title')
    استفسارات
@stop
@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0"> الاستفسارات </h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.queries.index') }}">الاستفسارات </a></li>
                            <li class="breadcrumb-item active">جميع الاستفسارات </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <!-- Data list view starts -->
    <section id="data-list-view" class="data-list-view-header">
        <div class="action-btns d-none">
            <div class="btn-dropdown mr-1 mb-1">
                <div class="btn-group dropdown actions-dropodown">
                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('dashboard.main.Actions') }}
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item delete-all" href="#"><i class="feather icon-trash"></i>حذف
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Data list view Ends -->

        <!-- DataTable starts -->
        <div class="table-responsive">
            <table class="table data-list-view">
                <thead>
                <tr>
                    <th></th>
                    <th>اسم الراسل </th>
                    <th>الاسم المتلقي </th>
                    <th>السؤال </th>
                    <th> الاجابه </th>
                    <th>الاجراءت</th>
                </tr>
                </thead>
                <tbody>
                @foreach($queries as $query)
                    <tr id="language-{{$query->id}}">
                        <td data-language-id="{{$query->id}}"></td>
                        <td>{{$query->user->name}}</td>
                        <td>{{$query->doctor->id}}</td>
                        <td>{{$query->question}}</td>
                        <td>{{$query->answer}}</td>
                        <td>



                            <a href="#">
                                 <span class="action-delete" data-category-id="{{ $query->id }}">
                                    <i class="feather icon-trash"></i>
                                 </span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- DataTable ends -->
    </section>
    <!-- Data list view end -->
@endsection
@section('scripts')
    <script>
        var selectedAds = [];

    $('.action-delete').on('click', function () {
    var languageId = $(this).data('language-id'),
    url = '{{ route("admin.queries.destroy", ":id") }}',
    newUrl = url.replace(':id', languageId);

    // console.log(categoryId);
    // console.log( newUrl);

    Swal.fire({
    title: 'هل تريد حذف السؤال ؟',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'نعم احذفه ',
    confirmButtonClass: 'btn btn-primary',
    cancelButtonClass: 'btn btn-danger ml-1',
    cancelButtonText: 'الغاء',
    buttonsStyling: false,
    }).then(function (result) {
    if (result.value) {
    $.ajax({
    url: newUrl,
    method: 'GET',
    // _token: "{{ csrf_token() }}",
    success: function(response) {
    fireSuccess('{{ __('dashboard.alerts.deleted')  }}')
    Swal.fire({
    position: 'top-start',
    type: 'success',
    title: '{{ __('dashboard.alerts.deleted')  }}',
    showConfirmButton: false,
    timer: 1500,
    confirmButtonClass: 'btn btn-primary',
    buttonsStyling: false,
    })
    // Remove the raw
    $('#language-' + languageId).remove();
    }
    });
    }
    })
    });
    </script>

    <script>
          $('.delete-all').click(function () {
            var selectedLanguages = [];
            $('.dt-checkboxes:checked').each(function() {
                selectedLanguages.push( $(this).parent().data('language-id') );
            });
            console.log(selectedLanguages.length);
            if(selectedLanguages.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: 'هل تريد حذف الاقسام المحدده',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم احذف المحدد',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: 'الغاء',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.languages.destroy_selected') }}",
                            method: 'POST',
                             headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                             _token: "{{ csrf_token() }}",
                            data: {
                                ids: selectedLanguages
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: 'تم حذف المحدد بنجاح ',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                selectedLanguages.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#language-' + element).remove();
                                } );
                            }
                        });
                    }
                })

            }

        });
    </script>


@stop
