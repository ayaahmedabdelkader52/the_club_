@extends('admin.master')
@section('title')
    البريد الوارد
@stop
@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">البريد الوارد</h2>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <!-- Data list view starts -->
{{--    <section>--}}
    <section id="data-list-view" class="data-list-view-header">
        <div class="action-btns d-none">
            <div class="btn-dropdown mr-1 mb-1">
                <div class="btn-group dropdown actions-dropodown">
                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('dashboard.main.Actions') }}
                    </button>

                    <div class="dropdown-menu">
                        <a class="dropdown-item action-delete-selected" href="#"><i class="feather icon-trash"></i>{{ __('dashboard.action.delete') }}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="action-filters d-none">
            <div class="dataTables_length" id="DataTables_Table_0_length">
                <label>
                    <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="custom-select custom-select-sm form-control form-control-sm">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                </label>
            </div>
            <div id="DataTables_Table_0_filter" class="dataTables_filter">
                <label>
                    <input type="search" class="form-control form-control-sm" placeholder="" aria-controls="DataTables_Table_0">
                </label>
            </div>
        </div>
        <!-- Data list view Ends -->

        <!-- DataTable starts -->
        <div class="table-responsive">
            <table class="table data-list-view">
                <thead>
                <tr>
                    <th></th>
                    <th>الاسم</th>
                    <th>الهاتف</th>
                    <th>الرساله</th>
                    <th>التاريخ</th>
                    <th>التحكم</th>
                </tr>
                </thead>
                <tbody>
                @foreach($messages as $message )
                    <tr id="message-{{$message->id}}">
                        <td data-message-id="{{ $message->id }}"></td>
                        <td>{{$message->name}}</td>
                        <td> {{$message->phone}} </td>
                        <td> {{$message->message}} </td>
                        <td> {{ $message->created_at }} </td>
                        <td>
                            <span class="action-delete" data-message-id="{{ $message->id }}" title="حذف">
                              <i class="feather icon-trash"></i>
                            </span>

                            <span data-toggle="modal" data-target="#messageModal" class="send-message" data-user-phone="{{ $message->phone }}"
                                  data-user-email="{{ $message->email }}" data-user-id="{{ $message->user_id }}"
                                  title="{{ __('dashboard.action.reply_message') }}">
                                <i class="feather icon-send"></i>
                            </span>

                            <a href="{{ route('admin.inbox.show', $message) }}">
                                <span title="عرض">
                                    <i class="feather icon-eye"></i>
                                </span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- DataTable ends -->
    </section>
    <!-- Data list view end -->

    <!-- Message starts -->
    @include('admin.contact.includes.message-to-single-user')
    <!-- Message ends -->

@stop
@section('scripts')
    <script>

        $('.action-delete').on('click', function () {
            var messageId = $(this).data('message-id'),
                url = '{{ route("admin.inbox.destroy", ":id") }}',
                newUrl = url.replace(':id', messageId);


            Swal.fire({
                title: 'هل تريد حذف الرساله',
                // text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('dashboard.action.yes_delete') }}',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger ml-1',
                cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: newUrl,
                        method: 'DELETE',
                        // _token: "{{ csrf_token() }}",
                        success: function(response) {
                            Swal.fire({
                                position: 'top-start',
                                type: 'success',
                                title: '{{ __('dashboard.alerts.deleted')  }}',
                                showConfirmButton: false,
                                timer: 1500,
                                confirmButtonClass: 'btn btn-primary',
                                buttonsStyling: false,
                            })
                            $('#message-' + messageId).remove();

                        }
                    });
                }
            })
        });

        $('.action-delete-selected').on('click', function () {
            var selectedMessages = [];
            $('.dt-checkboxes:checked').each(function() {
                selectedMessages.push( $(this).parent().data('message-id') );
                // console.log($(this).parent().data('user-id') )
            });
            console.log(selectedMessages);
            // console.log(selectedMessages.length);
            if(selectedMessages.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: '{{ __('dashboard.alerts.do_you_want_to_delete_selected_data') }}',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{ __('dashboard.action.yes_delete_selected') }}',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: '{{ __('dashboard.action.cancel') }}',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.inbox.destroy_selected') }}",
                            method: 'POST',
                            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            // _token: "{{ csrf_token() }}",
                            data: {
                                messages: selectedMessages
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: '{{ __('dashboard.alerts.deleted')  }}',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                selectedMessages.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#message-' + element).remove();
                                } );

                                // console.log(selectedMessages)
                                // console.log(response.data.userId)
                            }
                        });
                    }
                })

            }

        });

        $('.send-message').on('click', function() {
            var userEmail = $(this).data('user-email'),
                userId = $(this).data('user-id'),
                userPhone = $(this).data('user-phone');

            // console.log('hello');
            console.log(userEmail);
            console.log(userPhone);
            console.log(userId);

            $('.user-phone').val(userPhone);
            $('.user-email').val(userEmail);
            $('.user-id').val(userId);
        });
    </script>
@stop
