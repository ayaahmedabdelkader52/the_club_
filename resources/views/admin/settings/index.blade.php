@extends('admin.master')

@section('title', 'الإعدادات')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <div class="breadcrumb-wrapper col-12">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- account setting page start -->
        <section id="page-account-settings">
            <div class="row">
                <!-- left menu section -->
                <div class="col-md-3 mb-2 mb-md-0">
                    <ul class="nav nav-pills flex-column mt-md-0 mt-1">
                        <li class="nav-item">
                            <a class="nav-link d-flex py-75 active" id="account-pill-general" data-toggle="pill"
                               href="#update-profile" aria-expanded="true">
                                <i class="feather icon-globe mr-50 font-medium-3"></i>
                                تحديث الملف الشخصي
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex py-75" id="account-pill-password" data-toggle="pill"
                               href="#change-password" aria-expanded="false">
                                <i class="feather icon-lock mr-50 font-medium-3"></i>
                                تغيير الرقم السري
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex py-75" id="site-pill-settings" data-toggle="pill"
                               href="#site-settings" aria-expanded="true">
                                <i class="feather icon-globe mr-50 font-medium-3"></i>
                               اعدادات الموقع
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex py-75" id="account-pill-social" data-toggle="pill"
                               href="#social-media" aria-expanded="false">
                                <i class="feather icon-camera mr-50 font-medium-3"></i>
                               مواقع التواصل
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex py-75" id="account-pill-connections" data-toggle="pill"
                               href="#sms" aria-expanded="false">
                                <i class="ficon feather icon-message-square  mr-50 font-medium-3"></i>الرسائل
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex py-75" id="account-pill-connections" data-toggle="pill"
                               href="#smtp-package" aria-expanded="false">
                                <i class="ficon feather icon-mail  mr-50 font-medium-3"></i>الإيميل
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link d-flex py-75" id="terms-pill-notification" data-toggle="pill"
                               href="#notification" aria-expanded="false">
                                <i class="feather icon-bell mr-50 font-medium-3"></i>
                                الاشعارات
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link d-flex py-75" id="terms-pill-Api" data-toggle="pill"
                               href="#api" aria-expanded="false">
                                <i class="feather icon-feather mr-50 font-medium-3"></i>
                                API
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- right content section -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="tab-content">
                                    @include('admin.settings.includes.update-profile')
                                    @include('admin.settings.includes.change-password')
                                    @include('admin.settings.includes.site-settings')
                                    @include('admin.settings.includes.social-media')
                                    @include('admin.settings.includes.sms')
                                    @include('admin.settings.includes.email')
                                    @include('admin.settings.includes.notification')
                                    @include('admin.settings.includes.api')

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- account setting page end -->

    </div>
@stop
@section('scripts')
    <script src="{{asset('Admin/app-assets/vendors/js/editors/quill/katex.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/editors/quill/highlight.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/editors/quill/quill.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/js/scripts/last-tab.js')}}"></script>
    <script src="{{asset('includes/image-preview-multi.js')}}"></script>

    <script>

        var loadFile = function (event) {

            var outputId = event.path[0].id;
            outputId = 'output-' + outputId;

            // console.log(outputId);
            // console.log(event.path[0].id);

            var outputs = document.getElementById(outputId);
            outputs.src = URL.createObjectURL(event.target.files[0]);
            outputs.onload = function () {
                URL.revokeObjectURL(outputs.src) // free memory
            }
        };
            // On Delete
            var selectedItems = [],
            rowId;
            $('.action-delete').on('click', function (){
                 rowId = $(this).data('social-id');
                var oldUrl = '{{ route("admin.settings.delete_social_media", ":id") }}',
                    url = oldUrl.replace(':id', rowId);

            swal_confirm_delete(url, rowId);

        });

    </script>
    @include('admin.includes.confirm-deletes')
    <!-- end edit script social media -->
@stop
