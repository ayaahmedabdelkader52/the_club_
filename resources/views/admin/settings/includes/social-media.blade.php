<div class="tab-pane fade " id="social-media" role="tabpanel"
     aria-labelledby="account-pill-social" aria-expanded="false">

    <button class="btn btn-primary mr-0 mr-sm-1 mb-1 mb-sm-0" data-toggle="modal" data-target="#socialModal"
            style="margin-bottom: 20px !important;">
        <i class="feather icon-file-plus"></i> اضافه موقع
    </button>
    &nbsp;
    <div class="d-flex flex-column flex-sm-row">

        <BR>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>الوجو</th>
                    <th>الاسم</th>
                    <th>اللينك</th>
{{--                    <th>تاريخ الاضافه</th>--}}
                    <th>التحكم</th>
                </tr>
                </thead>
                <tbody>

                @foreach($socials as $social)
                    <tr id="row-{{$social->id}}">

                        <td>
                            <img width="30" src="{{ $social->imagePath }}">
                        </td>
                        <td>{{$social->name}}</td>
                        <td>{{$social->link}}</td>
{{--                        <td>{{$social->created_at->diffForHumans()}}</td>--}}
                        <td>
                            <span class="action-delete" data-social-id="{{ $social->id }}">
                                <i class="feather icon-trash"></i>
                            </span>
                            <a class="editSocial" data-toggle="modal" data-target="#sociaEditlModal"
                               data-social-id="{{$social->id}}"
                               data-social-name="{{$social->name}}" data-social-link="{{$social->link}}"
                               data-social-image="{{$social->image}}" data-update-url="{{ route('admin.settings.update_social_media', $social) }}">
                                <i class="feather icon-edit"></i>
                            </a>

                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Start Social Create  -->
<div class="modal fade text-left" id="socialModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel33">اضافه موقع تواصل جديد</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


            <div class="tab-content pt-1">
                <div class="tab-pane active" id="sms-message" role="tabpanel" aria-labelledby="sms-message-fill">
                    <form action="{{route('admin.settings.store_social_media')}}" method="post"
                          enctype="multipart/form-data">
                        @csrf

                        <div class="media">
                            <a href="javascript: void(0);">
                                <img id="output-social" src="{{ asset('assets/uploads/socials/default.jpg')}}"
                                     class="rounded mr-75" alt="profile image" height="64" width="64">
                            </a>
                            <div class="media-body mt-75">
                                <div class="col-12 px-0 d-flex flex-sm-row flex-column justify-content-start">
                                    <label class="btn btn-sm btn-primary ml-50 mb-50 mb-sm-0 cursor-pointer"
                                           for="social">اضافه لوجو الموقع</label>
                                    <input type="file" id="social" name="image" hidden accept="image/*"
                                           class="input-img" onchange="loadFile(event)">
                                    @error('image')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                            </div>
                        </div>
                        <hr>

                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="controls">
                                            <input type="text" class="form-control" name="name"
                                                   placeholder="اسم الموقع">

                                            @error('name')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror

                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="controls">
                                            <input type="text" class="form-control" name="link"
                                                   placeholder="لينك الموقع">

                                            @error('link')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">اضافه</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End Social Create  -->


<!-- Start Social Edit  -->
<div class="modal fade text-left" id="sociaEditlModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel33">تعديل الموقع</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


            <div class="tab-content pt-1">
                <div class="tab-pane active" id="sms-message" role="tabpanel" aria-labelledby="sms-message-fill">
                    <form id="editForm" enctype="multipart/form-data" method="post">
                        @method('PUT')

                        @csrf
                        <input required="" type="hidden" name="id" value="">
                        <div class="col-12">
                            <div class="form-group">
                                <div class="media">
                                    <a href="javascript: void(0);">
                                        <img id="output-social-edit"
                                             src="{{ asset('assets/uploads/socials/default.jpg')}}"
                                             class="rounded mr-75" alt="profile image" height="64" width="64">
                                    </a>
                                    <div class="media-body mt-75">
                                        <div class="col-12 px-0 d-flex flex-sm-row flex-column justify-content-start">
                                            <label class="btn btn-sm btn-primary ml-50 mb-50 mb-sm-0 cursor-pointer"
                                                   for="social">Upload new photo</label>
                                            <input type="file" id="social" name="image" hidden accept="image/*"
                                                   class="input-img" onchange="loadFile(event)">
                                            @error('image')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                    </div>
                                </div>
                                <hr>
                            </div>
                        </div>

                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="controls">
                                            <input type="text" class="form-control" name="name"
                                                   placeholder="اسم الموقع">

                                            @error('name')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror

                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="controls">
                                            <input type="text" class="form-control" name="link"
                                                   placeholder="لينك الموقع">

                                            @error('link')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary editBtn">تعديل</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End Social Edit  -->
