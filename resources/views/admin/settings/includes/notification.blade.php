<div class="tab-pane fade" id="notification" role="tabpanel"
     aria-labelledby="terms-pill-notification" aria-expanded="true">
        <div class="row">
            <div class="col-md-6">
                <form action="{{ route('admin.settings.update_fcm')}}" method="post">
                    @csrf
                    <h5 class="panel-title">FCM</h5>

                    <br>

                    <div class="row">

                        <div class="col-12">
                            <div class="form-label-group form-group">
                                <input id="server-key" type="text" class="form-control" name="server_key"  placeholder="server key"
                                       value="{{ ($fcm) ? $fcm->server_key : '' }}">
                                <label for="server-key">server key</label>

                                @include('admin.includes.alerts.input-errors', ['input' => "server_key"])
                            </div>
                        </div>


                        <div class="col-12">
                            <div class="form-label-group form-group">
                                <input id="sender-id" type="text" class="form-control" name="sender_id"  placeholder="sender id"
                                       value="{{ ($fcm) ? $fcm->sender_id : '' }}">

                                <label for="sender-id">sender id</label>

                                @include('admin.includes.alerts.input-errors', ['input' => "sender_id"])
                            </div>
                        </div>

                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">
                                حفظ التغييرات
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

</div>
