<div class="tab-pane fade " id="site-settings" role="tabpanel"
     aria-labelledby="site-pill-settings" aria-expanded="false">
    <form  method="post" action="{{ route('admin.settings.update') }}" enctype="multipart/form-data">
        @csrf
        <div class="media">
            <a href="javascript: void(0);">
                <img id="output-logo"  src="{{ isset($setting['logo']) ? asset('assets/uploads/settings/' . $setting['logo'] ) : asset('assets/uploads/settings/logo.png') }}"
                      class="rounded mr-75" alt="site logo" height="64" width="64">
            </a>
            <div class="media-body mt-75">
                <div class="col-12 px-0 d-flex flex-sm-row flex-column justify-content-start">
                    <label class="btn btn-sm btn-primary ml-50 mb-50 mb-sm-0 cursor-pointer" for="logo">تحديث لوجو الموقع</label>
                    <input type="file" id="logo" name="settings[logo]" hidden  accept="image/*" class="input-img" onchange="loadFile(event)">
                </div>
            </div>

            @include('admin.includes.alerts.input-errors', ['input' => "settings.logo"])
        </div>
        <hr>
        <div class="row">
            <div class="col-12">

                <div class="form-group">
                    <div class="controls">
                        <label for="account-e-mail">البريد الالكتروني</label>
                        <input  name="settings[site_email]" class="form-control" placeholder="البريد الالكتروني للتطبيق"
                                value="{{$setting['site_email']??''}}">
                    </div>
                    @include('admin.includes.alerts.input-errors', ['input' => "settings.site_email"])
                </div>

                <div class="form-group">
                    <div class="controls">
                        <label for="account-name">رقم الهاتف</label>
                        <input type="text"  name="settings[site_phone]" class="form-control" placeholder="رقم الهاتف للتطبيق"
                        value=" {{ $setting['site_phone'] ??  '' }}">
                    </div>
                    @include('admin.includes.alerts.input-errors', ['input' => "settings.site_email"])
                </div>

                <div class="form-group">
                    <div class="controls">
                        <label for="account-name">اسم الموقع</label>
                        <input type="text" name="settings[site_name]" class="form-control" placeholder="اسم الموقع"
                        value="{{ $setting['site_name'] ??  '' }}">
                    </div>
                    @include('admin.includes.alerts.input-errors', ['input' => "settings.site_name"])
                </div>

            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">وصف التطبيق بالعربي</label>
                    <textarea class="form-control" name="settings[description_ar]"  rows="3"
                              placeholder="وصف التطبيق بالعربي">{!! $setting['description_ar'] ?? '' !!}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.description_ar"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">وصف التطبيق بالانجليزي</label>
                    <textarea class="form-control" name="settings[description_en]"  rows="3"
                              placeholder="وصف التطبيق بالانجليزي">{!! $setting['description_en']  ??  '' !!}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.description_en"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">الكلمات الدلالية</label>
                    <textarea class="form-control" name="settings[site_tagged]" rows="3"
                              placeholder="الكلمات الدلالية">{{$setting['site_tagged'] ?? ''}} </textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.site_tagged"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">حقوق التطبيق بالعربي</label>
                    <textarea class="form-control"  name="settings[copyright_ar]"  rows="3"
                              placeholder="حقوق التطبيق بالعربي">{{$setting['copyright_ar'] ?? ''}}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.copyright_ar"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">حقوق التطبيق بالانجليزي</label>
                    <textarea class="form-control"  name="settings[copyright_en]"  rows="3"
                              placeholder="حقوق التطبيق بالانجليزي">{{$setting['copyright_en'] ?? ''}}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.copyright_en"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">سياسات الموقع بالعربي</label>
                    <textarea class="form-control"  name="settings[policies_ar]"  rows="3"
                              placeholder="سياسات الموقع بالعربي">{{$setting['policies_ar'] ?? ''}} </textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.policies_ar"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">سياسات الموقع بالانجليزي</label>
                    <textarea class="form-control"  name="settings[policies_en]"  rows="3"
                              placeholder="سياسات الموقع بالانجليزي">{{ $setting['policies_en'] ?? ''}}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.policies_en"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">عن الموقع بالعربي</label>
                    <textarea class="form-control"  name="settings[about_us_ar]"  rows="3"
                              placeholder="عن الموقع بالانجليزي">{{ $setting['about_us_ar'] ?? ''}}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.about_us_ar"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">عن الموقع بالانجليزي</label>
                    <textarea class="form-control"  name="settings[about_us_en]"  rows="3"
                              placeholder="عن الموقع بالانجليزي">{{ $setting['about_us_en'] ?? ''}}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.about_us_en"])
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="accountTextarea">الاعضاء المميزين</label>
                    <textarea class="form-control"  name="settings[star]"  rows="3"
                              placeholder="عن الموقع بالانجليزي">{{ $setting['star'] ?? ''}}</textarea>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "settings.star"])
            </div>

            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">
                    حفظ التغييرات
                </button>
            </div>
        </div>
    </form>
</div>
