<div class="tab-pane fade" id="terms-and-Conditions" role="tabpanel"
     aria-labelledby="terms-pill-connections" aria-expanded="false">
    <div class="row">
        <div class="col-md-12">

            <!-- full Editor start -->
            <form id="terms-form" action="{{route('admin.settings.update_terms_and_conditions')}}" method="post">
                @csrf
                <section class="full-editor">

                    <div class="row">

                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">الشروط والاحكام</h4>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">

                                        SC
                                        <input type="hidden" id="quill_html" name="text">
                                        <div class="quill_editor"
                                             id="full-container">{!! $condition?$condition->text:'' !!}</div>

                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">

                                        </div>

                                    </div>
                                </div>
                                <div class="col-12">
                                    <button class="btn btn-primary mr-sm-1 mb-1 mb-sm-0 updateProfile"> حفظ
                                        التغييرات
                                    </button>
                                </div>
                            </div>

                        </div>

                    </div>

                </section>
            </form>
            <!-- full Editor end -->

        </div>
    </div>
</div>
