<div role="tabpanel" class="tab-pane active" id="update-profile" aria-labelledby="account-pill-general" aria-expanded="true">


    <form action="{{route('admin.settings.update_profile', auth()->user() )}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="media">
            <a href="javascript: void(0);">
                <img id="output-account-upload"
                     src="{{ auth()->user()->avatar?asset('assets/uploads/users/' . auth()->user()->avatar) : asset('assets/uploads/users/default.png')}}"
                     class="rounded mr-75" alt="profile image" height="64" width="64">
            </a>
            <div class="media-body mt-75">
                <div class="col-12 px-0 d-flex flex-sm-row flex-column justify-content-start imgs-block">
                    <label class="btn btn-sm btn-primary ml-50 mb-50 mb-sm-0 cursor-pointer" for="account-upload">تحديث صوره الملف الشخصي</label>
                    <input type="file" id="account-upload" name="avatar" hidden accept="image/*" onchange="loadFile(event)">

                    @include('admin.includes.alerts.input-errors', ['input' => "avatar"])
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-12">
                <div class="form-label-group form-group">
                        <input id="name" type="text" class="form-control" name="name" placeholder="{{ __('dashboard.user.name') }}" value="{{ auth()->user()->name }}">
                    <label for="name">{{ __('dashboard.user.name') }}</label>
                </div>
                @include('admin.includes.alerts.input-errors', ['input' => "name"])
            </div>

            <div class="col-12">
                <div class="form-label-group form-group">
                    <input id="email" type="text" class="form-control" name="phone" placeholder="{{ __('dashboard.user.email') }}"
                           value="{{auth()->user()->email}}">

                    <label for="email">{{ __('dashboard.user.email') }}</label>
                </div>

                @include('admin.includes.alerts.input-errors', ['input' => "email"])
            </div>

            <div class="col-12">
                <div class="form-label-group form-group">
                    <input id="phone" type="text" class="form-control" name="phone" placeholder="{{ __('dashboard.user.phone') }}" value="{{auth()->user()->fullPhone}}">

                    <label for="phone">{{ __('dashboard.user.phone') }}</label>
                </div>

                @include('admin.includes.alerts.input-errors', ['input' => "phone"])
            </div>

            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                <button class="btn btn-primary mr-sm-1 mb-1 mb-sm-0 updateProfile"> حفظ التغييرات</button>
            </div>
        </div>
    </form>
</div>
