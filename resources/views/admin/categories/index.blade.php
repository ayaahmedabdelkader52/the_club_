@extends('admin.master')
@section('title')
    الاقسام الرئيسيه
@stop
@section('content')
    <!--content wrapper -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">الاقسام الرئيسيه</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.categories.index') }}">الاقسام الرئيسيه</a></li>
                            <li class="breadcrumb-item active">جميع الاقسام الرئيسيه</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of content wrapper -->

    <!-- Data list view starts -->
    <section id="data-list-view" class="data-list-view-header">
        <div class="action-btns d-none">
            <div class="btn-dropdown mr-1 mb-1">
                <div class="btn-group dropdown actions-dropodown">
                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('dashboard.main.Actions') }}
                    </button>

                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{route('admin.categories.create')}}"><i
                                class="feather icon-archive"></i> اضافه </a>
                        <a class="dropdown-item delete-all" href="#"><i class="feather icon-trash"></i>حذف
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Data list view Ends -->

        <!-- DataTable starts -->
        <div class="table-responsive">
            <table class="table data-list-view">
                <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم بالعربيه</th>
                    <th>الاسم بالانجليزيه</th>
                    <th>الصوره </th>
                    <th>الاجراءت</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr id="category-{{$category->id}}">
                        <td data-category-id="{{$category->id}}"></td>
                        <td>{{$category->title_ar}}</td>
                        <td>{{$category->title_en}}</td>
                        <td><img style="width:100px" src="{{$category->logoPath}}"></td>
                        <td>

                            <a href="{{route('admin.categories.edit',$category)}}">
                                <i class="feather icon-edit"></i>
                            </a>

                            <a href="#">
                                 <span class="action-delete" data-category-id="{{ $category->id }}">
                                    <i class="feather icon-trash"></i>
                                 </span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <!-- DataTable ends -->
    </section>
    <!-- Data list view end -->
@endsection
@section('scripts')
    <script>
        var selectedAds = [];

    $('.action-delete').on('click', function () {
    var categoryId = $(this).data('category-id'),
    url = '{{ route("admin.categories.destroy", ":id") }}',
    newUrl = url.replace(':id', categoryId);

    // console.log(categoryId);
    // console.log( newUrl);

    Swal.fire({
    title: 'هل تريد حذف السؤال ؟',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'نعم احذفه ',
    confirmButtonClass: 'btn btn-primary',
    cancelButtonClass: 'btn btn-danger ml-1',
    cancelButtonText: 'الغاء',
    buttonsStyling: false,
    }).then(function (result) {
    if (result.value) {
    $.ajax({
    url: newUrl,
    method: 'GET',
    // _token: "{{ csrf_token() }}",
    success: function(response) {
    fireSuccess('{{ __('dashboard.alerts.deleted')  }}')
    Swal.fire({
    position: 'top-start',
    type: 'success',
    title: '{{ __('dashboard.alerts.deleted')  }}',
    showConfirmButton: false,
    timer: 1500,
    confirmButtonClass: 'btn btn-primary',
    buttonsStyling: false,
    })
    // Remove the raw
    $('#category-' + categoryId).remove();
    }
    });
    }
    })
    });
    </script>

    <script>
          $('.delete-all').click(function () {
            var selectedCategories = [];
            $('.dt-checkboxes:checked').each(function() {
                selectedCategories.push( $(this).parent().data('category-id') );
            });
            console.log(selectedCategories.length);
            if(selectedCategories.length < 1)
                Swal.fire({
                    type: 'error',
                    title: '{{ __('dashboard.alerts.no_data_selected') }}',
                    text: '{{ __('dashboard.alerts.select_data') }}',
                    confirmButtonClass: 'btn btn-primary',
                    buttonsStyling: false,
                })
            else {
                Swal.fire({
                    title: 'هل تريد حذف الاقسام المحدده',
                    // text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم احذف المحدد',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    cancelButtonText: 'الغاء',
                    buttonsStyling: false,
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route('admin.categories.destroy_selected') }}",
                            method: 'POST',
                             headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                             _token: "{{ csrf_token() }}",
                            data: {
                                ids: selectedCategories
                            },
                            success: function(response) {
                                Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: 'تم حذف المحدد بنجاح ',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })

                                selectedCategories.forEach((element) => {
                                    console.log('element : ' + element);
                                    $('#category-' + element).remove();
                                } );
                            }
                        });
                    }
                })

            }

        });
    </script>


@stop
