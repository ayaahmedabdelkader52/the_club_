<div>
    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2020<a class="text-bold-800 grey darken-2" href="https://1.envato.market/pixinvent_portfolio" target="_blank">Pixinvent,</a>All rights Reserved</span><span class="float-md-right d-none d-md-block">Hand-crafted & Made with<i class="feather icon-heart pink"></i></span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{asset('Admin/app-assets/vendors/js/vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('Admin/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>

    <script src="{{asset('Admin/app-assets/vendors/js/editors/quill/katex.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/editors/quill/highlight.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/editors/quill/quill.min.js')}}"></script>

    <script src="{{asset('Admin/app-assets/vendors/js/extensions/dropzone.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/tables/datatable/dataTables.select.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/js/scripts/ui/data-list-view.js')}}"></script>


    <script src="{{asset('Admin/app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/extensions/tether.min.js')}}"></script>
    <script src="{{asset('Admin/app-assets/vendors/js/extensions/shepherd.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->




    <script src="{{asset('Admin/app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{asset('Admin/app-assets/js/core/app.js')}}"></script>
    <script src="{{asset('Admin/app-assets/js/scripts/components.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->

    <script src="{{asset('Admin/app-assets/js/scripts/pages/admin-analytics.js')}}"></script>




    <!-- BEGIN: Page JS-->
    <!-- TinyMCE -->
    <script src="https://cdn.tiny.cloud/1/t88rhnf212rqjcgv6731ocwrqokontc4imi660nebjjb8tf0/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="{{asset('Admin/app-assets/js/scripts/pages/app-user.js')}}"></script>
    <script src="{{asset('Admin/app-assets/js/scripts/pages/app-email.js')}}"></script>
    <script src="{{asset('Admin/app-assets/js/scripts/forms/validation/form-validation.js')}}"></script>
    <script src="{{asset('Admin/app-assets/js/scripts/charts/chart-apex.js')}}"></script>
    <script src="{{asset('Admin/app-assets/js/scripts/pages/account-setting.js')}}"></script>
    <script src="{{asset('Admin/app-assets/js/scripts/pages/app-chat.js')}}"></script>

    @yield('scripts')
    </body>
    <!-- END: Body-->

    </html>
</div>
