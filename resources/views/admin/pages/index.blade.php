@extends('admin.master')
@section('title')
    الصفحات
@stop
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Content Grid</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#">Content</a>
                                    </li>
                                    <li class="breadcrumb-item active">Grid
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                    <div class="form-group breadcrum-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!--Grid options-->

                <!--Equal-width-->
                <!--Stacked to horizontal-->
                <section id="stacked-to-horizontal" class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">عرض الصفحات </h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="bd-example-row">
                                        <div class="bd-example">
                                            <div class="container">
                                                <div class="row">ا
                                                    <div class="col-6">من نحن </div>
                                                    <div class="col-6">.col-6</div>
                                                    <div class="col-6">.col-6</div>
                                                    <div class="col-6">.col-6</div>
                                                </div>
                                            </div>
                                        </div>

                                        <pre>
							<code class="language-html">

							</code>
						</pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!--Alignment-->



            </div>
        </div>
    </div>
    <!-- END: Content-->

@stop
