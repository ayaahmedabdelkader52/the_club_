<?php

namespace App\Services\Messaging;

use App\Contracts\MessagingService;

class YamamahService implements MessagingService
{

    public function send($phone, $msg, $data)
    {
        sleep(1);
        $url = 'api.yamamah.com/SendSMS';
        $username = $data['username'];
        $password = $data['password'];
        $sender = $data['sender'];
        $to = $phone; // Should be like 966530007039
        $text = urlencode($msg);
        $fields = array(
            "Username" => $username,
            "Password" => $password,
            "Message" => $text,
            "RecepientNumber" => $to, //'00966'.ltrim($numbers,'0'),
            "ReplacementList" => "",
            "SendDateTime" => "0",
            "EnableDR" => False,
            "Tagname" => $sender,
            "VariableList" => "0",
        );

        $fields_string = json_encode($fields);

        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
            CURLOPT_POSTFIELDS => $fields_string,
        ));
        $result = curl_exec($ch);
        curl_close($ch);
    }
}
