<?php

namespace App\Services\Messaging;

use App\Contracts\MessagingService;

class GatewayService implements MessagingService
{
    public function send($phone, $msg, $data)
    {
        $username = $data['username'];
        $password = $data['password'];
        $sender = $data['sender'];

        $contextPostValues = http_build_query(array(
            'user' => $username,
            'password' => $password,
            'msisdn' => $phone,
            'sid' => $sender,
            'msg' => $msg,
            'fl' => 0
        ));
        $contextOptions['http'] = array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => $contextPostValues,
            'max_redirects' => 0,
            'protocol_version' => 1.0,
            'timeout' => 10,
            'ignore_errors' => TRUE
        );
        $contextResouce = stream_context_create($contextOptions);
        $url = "apps.gateway.sa/vendorsms/pushsms.aspx";
        $arrayResult = file($url, FILE_IGNORE_NEW_LINES, $contextResouce);
        $result = $arrayResult[0];
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
}
