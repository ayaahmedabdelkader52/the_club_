<?php

namespace App\Services\Messaging;

use App\Contracts\MessagingService;

class ForjawallyService implements MessagingService
{
    public function send($phone, $msg, $data)
    {
        $username = $data['username'];
        $password = $data['password'];
        $sender = $data['sender'];

        $text = urlencode( $msg);
        $sender   = urlencode( $sender);
        $url  = "https://www.4jawaly.net/api/sendsms.php?username=$username&password=$password&numbers=$phone&sender=$sender&message=$text&unicode=e&Rmduplicated=1&return=string";
        $result = file_get_contents($url,true);
    }
}
