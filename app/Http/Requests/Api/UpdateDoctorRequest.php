<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDoctorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = auth('api')->user();
        return [

            'email' => ['nullable', 'email', 'unique:users,email,' . $user->id],
            'name' => 'string|max:255',
            'phone' => 'unique:users',
            'password' => 'min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6',
            'avatar' => 'image|mimes:jpg,jpeg,svg,png',
            'price_hour' => 'numeric',
            'weekly_off' => 'numeric',
            'monthly_off' => 'numeric',
            'bio' => 'string',
//                'device_id' => 'required',
//                'device_type' => 'required|in:ios,android',

        ];
    }
}
