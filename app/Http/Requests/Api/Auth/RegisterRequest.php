<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Common\RegisterRequest as MainRegisterRequest;

class RegisterRequest extends MainRegisterRequest
{
    public function rules()
    {
        return $this->getRules() + [

                'name' => 'required|string|max:255',
                'phone' => 'required|unique:users',
                'email' => 'required|unique:users',
                'password' => 'required',
                'avatar'=>'image|mimes:jpg,jpeg,svg,png',
                'device_id' => 'required',
                'lat'=>'',
                'lng'=>'',
                'address'=>'',
                'device_type' => 'required|in:ios,android',
                'type' => '',

            ];
    }

}
