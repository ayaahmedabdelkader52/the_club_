<?php

namespace App\Http\Requests\Api\Auth;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Common\RegisterRequest as MainRegisterRequest;

class PatientRegisterRequest extends MainRegisterRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  $this->getRules() + [

                'name' => 'required|string|max:255',
                'phone' => 'required|unique:users',
                'email' => 'required|unique:users',
                'password' => 'required|min6',
                'password_confirmation' => 'required_with:password|same:password|min:6',
                'nationality' => 'required',
                'avatar' => 'image|mimes:jpg,jpeg,svg,png',
                'gender' => 'required',
                'lat' => '',
                'lng' => '',
                'address' => '',
                'device_id' => 'required',
                'device_type' => 'required|in:ios,android',

            ];
    }
}
