<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Rules\CheckCodeRule;

class ActiveAccountRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'code' => ['required', new CheckCodeRule()],
            'device_id' => 'required',
            'device_type' => 'required|in:ios,android',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
