<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Models\User;
use Illuminate\Support\Facades\Request;

class LoginRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'phone' => 'required|string',
            'password' => 'required|min:8',
//            'device_id' => 'required',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'phone' => User::cleanPhone(Request::input('phone'))['phone']
        ]);
    }

    public function authorize()
    {
        return true;
    }
}
