<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Models\User;
use App\Rules\CheckPassword;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;

class UpdateProfileRequest extends ApiRequest
{
    protected function prepareForValidation()
    {
        $this->merge([
            'phone' => User::cleanPhone(Request::input('phone'))['phone'],
            'country_key' => User::cleanPhone(Request::input('phone'))['country_key'],
        ]);
    }

    public function rules()
    {
        return [
            'name' => 'nullable|string|max:191',
            'phone' => ['nullable',  Rule::unique('users')->ignore(auth('api')->user()->id, 'id')],
            'email'=> ['nullable', 'email', Rule::unique('users')->ignore(auth('api')->user()->id, 'id')],
            'avatar' => 'nullable|mimes:jpeg,png,jpg,gif,svg',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
