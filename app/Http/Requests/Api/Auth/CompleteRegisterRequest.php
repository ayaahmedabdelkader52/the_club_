<?php

namespace App\Http\Requests\Api\Auth;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Common\RegisterRequest as MainRegisterRequest;

class CompleteRegisterRequest extends MainRegisterRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [

                'nationality'=>'required',
                'price_hour'=>'numeric|required',
                'weekly_off'=>'numeric',
                'monthly_off'=>'numeric',
                'bio' => 'string',
                'gender'=>'required',
                'subcategory_id' => 'required' ,
                'languages_id' => 'required' ,
            ];
    }
}
