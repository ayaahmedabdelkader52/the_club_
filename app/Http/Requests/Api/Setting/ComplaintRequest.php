<?php

namespace App\Http\Requests\Api\Setting;

use App\Http\Requests\Api\ApiRequest;

class ComplaintRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'phone' => ['required', 'unique:users,phone', 'phone:SA,EG'],
            'subject' => 'required|string|max:255',
            'message' => 'required|string',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
