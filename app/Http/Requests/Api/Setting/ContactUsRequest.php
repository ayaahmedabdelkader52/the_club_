<?php

namespace App\Http\Requests\Api\Setting;

use App\Http\Requests\Api\ApiRequest;

class ContactUsRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'phone' => ['required', 'unique:users,phone', 'phone:SA,EG'],
            'message' => 'required|min:2|max:255',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
