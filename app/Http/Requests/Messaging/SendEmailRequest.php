<?php

namespace App\Http\Requests\Messaging;

use Illuminate\Foundation\Http\FormRequest;

class SendEmailRequest extends FormRequest
{
    public function rules()
    {
        return [
            'message' => 'required|min:1'
        ];
    }

    public function authorize()
    {
        return true;
    }
}
