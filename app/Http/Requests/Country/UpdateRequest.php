<?php

namespace App\Http\Requests\Country;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name.ar' => 'required|string',
            'name.en' => 'required|string',
            'currency.ar' => 'required|string',
            'currency.en' => 'required|string',
            'currency_code.ar' => 'required|string',
            'currency_code.en' => 'required|string',
            'calling_code' => 'required|string',
            'flag' => 'nullable|image|mimes:jpg,jpeg,svg,png',
            'active' => 'nullable',
            'iso3' => 'required',
            'iso2' => 'nullable',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
