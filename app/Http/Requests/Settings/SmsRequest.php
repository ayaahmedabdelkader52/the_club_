<?php

namespace App\Http\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;

class SmsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $type = $this->type;

        return [
            "type" => "required",
            "username.$type" => "required|string|max:255",
            "password.$type" => "required|string|max:255",
            "sender_name.$type" => "required|string|max:255",
        ];
    }

    public function messages()
    {
        return [
            'type.required' => __('dashboard.alerts.active_package')
        ];
    }
}
