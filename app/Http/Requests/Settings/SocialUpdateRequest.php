<?php

namespace App\Http\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;

class SocialUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'nullable|mimes:jpeg,png,jpg,gif,svg',
            'name' => 'required',
            'link' => 'required|url',
        ];
    }

    public function messages()
    {
        return [
            'image.required' => 'يجب اختيار لوجو للموقع',
            'image.mimes' => 'يجب اختيار لوجو للموقع بشكل صحيح',
            'name' => 'اسم الموقع مطلوب',
            'link' => 'لينك الموقع مطلوب',
        ];
    }
}
