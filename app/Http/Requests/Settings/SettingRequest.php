<?php

namespace App\Http\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{

    public function rules()
    {
        return [
            'settings.logo' => 'nullable|image|mimes:jpeg,png,jpg,svg',
            'settings.site_email' => 'nullable|email',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
