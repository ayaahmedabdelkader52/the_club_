<?php

namespace App\Http\Requests\Common;

use App\Models\User;
use App\Traits\HasResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\ValidationException;

class RegisterRequest extends FormRequest
{
    use HasResponse;

    protected array $rules = [
        'phone' => ['required', 'unique:users,phone', 'phone:SA,EG'],
        'email' => [ 'email', 'unique:users,email'],
        'password' => 'required|confirmed|min:8',
    ];

    public function getRules(): array
    {
        return $this->rules;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'phone' => User::cleanPhone(Request::input('phone'))['phone']
        ]);
    }

    protected function failedValidation(Validator $validator)
    {
        if (Request::is('api*')) {
            $this->validationResponse($validator);
        }
        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }

}
