<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ActiveUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check() && Auth::user()->status == 'pending')
            return response([
                'value' => '0' ,
                'key' => "fail",
                'status' => false,
                'msg' => 'not activated' ,
                'code' => 401 ,
            ]);
        elseif (Auth::check() && Auth::user()->status == 'active')
               return $next($request);

    }
}
