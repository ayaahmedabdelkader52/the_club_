<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $lang = app()->getLocale();
        return [
            'id'=>(int)$this->id,
            'title_'.$lang=>(string)$this['title_'.$lang],
            'logo' => (string)$this->logoPath,
            'category_name'=>$this->category['title_'.$lang],
            'user_id'=>(int)$this->user_id,
            'type'=>(string)$this->type,
        ];
    }
}
