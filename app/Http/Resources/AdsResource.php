<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Ads */
class AdsResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (int)$this->id,
            'title' => (string)$this->title,
            'body' => (string)$this->body,
            'image' => (string)$this->imagePath,
            'url' =>(string) $this->url,
            'expiry_date' => (string)$this->expiry_date,
        ];
    }
}
