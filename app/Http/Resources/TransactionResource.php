<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>(int)$this->id,
            'amount'=>(string)$this->amount,
            'user_id'=>(string)$this->user_id,
            'doctor_id'=>(string)$this->doctor_id,
            'content'=>(string)$this->content,
        ];
    }
}
