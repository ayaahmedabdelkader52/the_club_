<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class QueriesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>(int)$this->id,
            'user'=>(string)$this->user_id,
            'trainer'=>(string)$this->trainer_id,
            'question'=>(string)$this->question,
            'answer'=>(string)$this->answer,
        ];
    }
}
