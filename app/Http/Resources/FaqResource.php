<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Faq */
class FaqResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = app()->getLocale();
        return [
            'id' => (int)$this->id,
            'question' => (string)$this['question_'.$lang],
            'answer' => (string)$this['answer_'.$lang],
        ];
    }
}
