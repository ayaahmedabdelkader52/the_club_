<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>(int)$this->id,
            'user_id' =>(string)$this->user->name,
            'doctor_id'=>(string)$this->doctor->name,
            'status'=>(string)$this->status,
            'total_price'=>(string)$this->total_price,
            'price_after'=>(string)$this->price_after,
            'hours_per_day'=>(string)$this->hours_per_day,
            'total_days'=>(string)$this->total_days,
            'type'=>(string)$this->type,
            'city'=>(string)$this->city,
            'start'=>(string)$this->start,
            'end'=>(string)$this->end,
        ];
    }
}
