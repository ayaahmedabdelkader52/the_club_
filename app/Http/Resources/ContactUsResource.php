<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use function GuzzleHttp\Psr7\str;

class  ContactUsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => (int)$this->id,
            'name'=>(string)$this->name,
            'email'=>(string)$this->email,
            'phone'=>(string)$this->phone,
            'user_id'=>(string)$this->user_id,
            'message'=>(string)$this->message,
            'senderType'=>(string)$this->senderType
        ];
    }
}
