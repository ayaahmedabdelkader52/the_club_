<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Notification */
class NotificationResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = app()->getLocale();
        return [
            'id' => (integer)$this->id,
            'url' => (string)$this->url,
            'title' => (string)$this->title,
            'message_'.$lang => (string)$this['message_'.$lang],
            'type' => (string)$this->type,
            'created_at' => (string)$this->created_at,
        ];
    }
}
