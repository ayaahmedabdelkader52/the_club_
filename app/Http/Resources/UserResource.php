<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            'email' => (string)$this->email,
            'country_key' => (string)$this->country_key,
            'phone' => (string)$this->phone,
            'avatar' => (string)$this->avatarPath,
            'status' => (string)$this->status,
            'type' => (string)$this->type,
            'avg_rate' => $this->avg_rate,
            'gender' => (string)$this->gender,
            'price_hour'=>(string)$this->price_hour,
            'nationality'=>(string)$this->nationality,
            'weekly_off'=>(string)$this->weekly_off,
            'monthly_off'=>(string)$this->monthly_off,
            'star' =>(string)$this->star,
            'bio'=>(string)$this->bio,
            'lat'=>(string)$this->lat,
            'lng'=>(string)$this->lng,
        ];
    }


}
