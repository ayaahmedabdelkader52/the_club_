<?php

namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Http\Resources\QueriesResource;
use App\Http\Resources\RateResource;
use App\Http\Resources\SubCategoryResource;
use App\Http\Resources\TransactionResource;
use App\Http\Resources\UserResource;
use App\Models\Device;
use App\Models\Order;
use App\Models\Queries;
use App\Models\Rate;
use App\Models\Setting;
use App\Models\Subcategory;
use App\Models\Transactions;
use App\Models\User;
use App\Models\UserSubcategory;
use App\Traits\HasResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PatientController extends Controller
{
    use HasResponse;


    public function subcategories($id)
    {
        $subcategories = Subcategory::latest()->where('category_id' , $id)->get();
        $data['subcategories'] = SubCategoryResource::collection($subcategories);
        return $this->dataReturn($data);
    }

    public function doctors($id)
    {

        $ids = UserSubcategory::where('subcategory_id' , $id)->get();
        $user_id = [] ;
        foreach ($ids as $id)
        {
            $user_id []= $id->user_id;
        }

        $users =User::whereIn('id' , $user_id)->orderBy('created_at' , 'desc')->get();
//        $star = Setting::where('key' , 'star')->first();
//        $number = $star->value;

        $data = UserResource::collection($users);

        return $this->dataReturn($data);

    }
//        User::whereIn('id' , $user_id)->update(['star' =>'false']);
//        for ($i =0 ; $i< count($users) ; $i+=$number )
//        {
//            $s_data = [
//                'star' => 'true',
//            ];
//
//            $users[$i]->update($s_data);
//        }
    public function doctor($id)
    {
        $user = User::where('id' , $id)->first();
        $data['user'] = UserResource::collection($user);

        return $this->dataReturn($data);
    }

            //Filter

    public function filterPlace($id)
    {
        $user = auth()->user();
        $ids = UserSubcategory::where('subcategory_id' , $id)->get();

        $user_id = [] ;
        foreach ($ids as $item)
        {
            $user_id []= $item->user_id;
        }

        $doctors = User::whereIn('id' , $user_id)->where('type','doctor')->where('status','active')
         ->select(DB::raw('*, ( 6367 * acos( cos( radians('.$user->lat.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$user->lng.') ) + sin( radians('.$user->lat.') ) * sin( radians( lat ) ) ) ) AS distance'))->orderBy('distance' , 'asc')->get();
        $data['users'] = UserResource::collection($doctors);

        return $this->dataReturn($data);
    }

    public function filterCost($id)
    {
        $ids = UserSubcategory::where('subcategory_id' , $id)->get();

        $user_id = [] ;
        foreach ($ids as $item)
        {
            $user_id []= $item->user_id;
        }
        $doctors = User::where('type','doctor')->whereIn('id' , $user_id)->where('status','active')->orderBy('price_hour','asc')->get();

        $data['users'] = UserResource::collection($doctors);

        return $this->dataReturn($data);

    }

    public function filterRate($id)
    {
        $ids = UserSubcategory::where('subcategory_id' , $id)->get();

        $user_id = [] ;
        foreach ($ids as $item)
        {
            $user_id []= $item->user_id;
        }

        $doctors = User::whereIn('id' , $user_id)->where('status','active')->orderBy('avg_rate','desc')->get();


        $data['users'] = UserResource::collection($doctors);

        return $this->dataReturn($data);
    }

         //Search
    public function search(Request $request , $id)
    {
        $name= $request->name;

//        $ids = UserSubcategory::where('subcategory_id',$id);

            $results = User::query()
            ->join( 'user_subcategories' ,function ($join){
             $join->on(  'user_subcategories.user_id' , '=' , 'users.id');
            })
            ->where('user_subcategories.subcategory_id'   , $id)
            ->where('users.status' , 'active')
            ->when($name , function ($query , $name) {
                return $query->where('users.name' , 'LIKE' , "%".$name."%" );

            })->distinct()->get();

        if(!$results)
        {
            return $this->failMsg('No results');
        }
        $data['users'] = UserResource::collection($results);

        return $this->dataReturn($data);
    }

        //Rate
    public function  rate(Request  $request , $id)
    {
        $insrt = [
            'rate' => $request ->rate ,
            'comment' => $request->comment,
            'user_id'=> $id ,

        ];

        $rate=Rate::create($insrt);

       $user = User::where('id' , $id)->first();
       $rates = Rate::where('user_id' , $id)->get();
        $avg_rate = $rates->avg('rate');
        $user -> update([
            'avg_rate'=>$avg_rate
        ]);
        $data['rate'] = new RateResource($rate);
        return $this->dataReturn($data);
    }

        //get Orders
    public function newOrders()
    {
        $neworders = Order::where('status' , 'new')->where('user_id',auth()->user()->id)->get();
//        dd($neworders);
        $data = OrderResource::collection($neworders);
        return $this->dataReturn($data);
    }

    public function finishedOrders()
    {
        $finishedorders = Order::where('type' , 'finished')->where('user_id',auth()->user()->id)->get();
        $data = OrderResource::collection($finishedorders);
        $data['order'] = new OrderResource($finishedorders);
//        $notified_user['user_id'] = $finishedorders->doctor_id ;
//        $notified_user['devices'] = Device::where('user_id' , $finishedorders->doctor_id)->get() ;
//        $title = 'Finish order';
//        $message_ar =$finishedorders->user->name . ' تم انهاء الفتره من قبل ';
//        $message_en = 'Doctor has answered your query';
//        $type = 'Order';
//        $fcmdata = [];
//        $fcmdata['title'] = $title;
//        $fcmdata['user_id'] = auth()->user()->id;
//        $fcmdata['message_ar'] = $message_ar;
//        $fcmdata['message_en'] = $message_en;
//        $fcmdata['key'] = $type;
//        $fcmdata['orders_notify'] =true ;
//        $fcmdata['not_seen_notifications'] = $notified_user->notifications()->where('seen', 0)->count();

        return $this->dataReturn($data);
    }

    public function activeOrders()
    {
        $activeneworders = Order::where('type' , 'active')->where('user_id',auth()->user()->id)->get();
        $data = OrderResource::collection($activeneworders);

        return $this->dataReturn($data);
    }

    //finish order
    public function finishbutton(Request $request)
    {
        $order = Order::where('id' , $request->order_id)->first();
//    dd($order);
        if($request->is_finished == 1)
        {
           $order->update([
                'status'=>'finished',
            ]);

        }
        $data['order'] = new OrderResource($order);

        $notified_user['user_id'] = $order->doctor_id ;
        $notified_user['devices'] = Device::where('user_id' , $order->doctor_id)->get() ;
        $title = 'Finish order';
        $message_ar =$order->user->name . ' تم انهاء الفتره من قبل ';
        $message_en = 'Doctor has answered your query';
        $type = 'Order';
        $fcmdata = [];
        $fcmdata['title'] = $title;
        $fcmdata['user_id'] = auth()->user()->id;
        $fcmdata['message_ar'] = $message_ar;
        $fcmdata['message_en'] = $message_en;
        $fcmdata['key'] = $type;
        $fcmdata['orders_notify'] =true ;
//        $fcmdata['not_seen_notifications'] = $notified_user->notifications()->where('seen', 0)->count();        return $this->successReturn('' , $data);
    }

    //pay
    public function paybutton(Request $request)
    {
        $order = Order::where('id' , $request->order_id)->first();
        if($request->is_paied == 1)
        {
             $order->update([
                'is_paied' => $request->is_paied,
            ]);

            $notified_user['user_id'] = $order->doctor_id ;
            $notified_user['devices'] = Device::where('user_id' , $order->doctor_id)->get() ;
            $title = 'Finish order';
            $message_ar =$order->user->name . ' تم الدفع من قبل ';
//        $message_en = 'Doctor has answered your query';
            $type = 'Order';
            $fcmdata = [];
            $fcmdata['title'] = $title;
            $fcmdata['user_id'] = auth()->user()->id;
            $fcmdata['message'] = $message_ar;
//        $fcmdata['message_en'] = $message_en;
            $fcmdata['key'] = $type;
            $fcmdata['orders_notify'] =true ;
//        $fcmdata['not_seen_notifications'] = $notified_user->notifications()->where('seen', 0)->count();
        }
        $data['order'] = new OrderResource($order);
        return $this->successReturn('' , $data);
    }

    //balance
    public function balance()
    {
        $transactions = Transactions::where('user_id' , auth()->user()->id)->get();
       $data['transactions'] = TransactionResource::collection($transactions);
       return $this->dataReturn($data);
    }

    //Send inquiere
    public function sendInqueires(Request $request , $id)
    {
        $validator = Validator::make($request->all(),[
            'question'=>'required'
        ]);
        if ($validator->fails())
        {
            return $this->requestFailsReturn($validator);
        }
        $question =Queries::create([
            'user_id'=>auth()->user()->id,
            'doctor_id'=>$id,
            'question'=>$request->question,
            ]);
        $data['queries'] = new QueriesResource($question);

        return $this->successReturn('Question send successfully' ,$data);

    }
    // view quires
    public function viewQueries()
    {
        $queries = Queries::where('user_id' , auth()->user()->id)->get();
        $data['queries'] = QueriesResource::collection($queries);

        return $this->dataReturn($data);
    }
    // view one Query
    public function oneQuery($id)
    {
        $queries = Queries::where('id' , $id)->first();
        $data['query'] = new QueriesResource($queries);

        return $this->dataReturn($data);
    }

}
