<?php

namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\IntroResource;
use App\Http\Resources\SubCategoryResource;
use App\Http\Resources\UserResource;
use App\Models\Category;
use App\Models\Intro;
use App\Models\Subcategory;
use App\Models\User;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class HomeController extends Controller
{
    use HasResponse;
    use Uploadable;

    public function index()
    {
        $intros = Intro::all();

        $data['intros'] = IntroResource::collection($intros);

        return $this->dataReturn($data);
    }

    public function categories()
    {
        $categories = Category::all();
        $data['categories'] = CategoryResource::collection($categories);
        return $this->dataReturn($data);
    }





}
