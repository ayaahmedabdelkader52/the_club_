<?php

namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Setting\ComplaintRequest;
use App\Http\Requests\Api\Setting\ContactUsRequest;
use App\Http\Resources\CityResource;
use App\Http\Resources\FaqResource;
use App\Http\Resources\QuestionResource;
use App\Http\Resources\SocialResource;
use App\Models\Country;
use App\Models\Faq;
use App\Models\Question;
use App\Models\Setting;
use App\Models\Social;
use App\Traits\HasResponse;

class SettingController extends Controller
{
    use HasResponse;

    public function logo()
    {
        $settings = Setting::all()->pluck('value', 'key');
        $data['logo'] = $settings['logo'];

        return $this->dataReturn($data);
    }

    public function cities()
    {
        $data = [];
        // $egypt = Country::where('iso2', 'EG')->first();
        $sar = Country::where('iso2', 'SA')->first();

        $data['cities'] = CityResource::collection($sar->cities);
        return $this->dataReturn($data);
    }

    public function contactUs(ContactUsRequest $request)
    {
        auth('api')->user()->contactsUs()->create($request->validated());

        return $this->successMsg(__('site.alerts.message_sent_successfully'));
    }

    public function complaint(ComplaintRequest $request)
    {
        auth('api')->user()->complains()->create($request->validated());

        return $this->successMsg(__('site.alerts.complaint_sent_successfully'));
    }

    public function faqs()
    {
        $faqs = Faq::all();

        $data['faqs'] = FaqResource::collection($faqs);

        return $this->dataReturn($data);
    }

    public function policies()
    {
        $settings = Setting::all()->pluck('value', 'key');
        $data['policies'] = $settings['policies_' . app()->getLocale()];

        return $this->dataReturn($data);
    }

    public function aboutUs()
    {
        $settings = Setting::all()->pluck('value', 'key');
        $data['about_us'] = $settings['about_us_' . app()->getLocale()];

        return $this->dataReturn($data);
    }

    public function socials()
    {
        $socials = Social::all();
        $data['socials'] = SocialResource::collection($socials);

        return $this->dataReturn($data);
    }



}
