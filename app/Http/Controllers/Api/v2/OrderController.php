<?php

namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\OrderRequest;
use App\Http\Resources\DayResource;
use App\Http\Resources\OrderResource;
use App\Http\Resources\UserResource;
use App\Models\Day;
use App\Models\Order;
use App\Models\OrderDay;
use App\Models\User;
use App\Traits\HasResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use HasResponse;
    public function plan($id)
    {
        $doctor = User::where('id' , $id)->first();
        $data['daily_basis'] = $doctor->price_hour ;
        if($doctor->monthly_off > 0 && $doctor->weekly_off > 0)
        {
            $data['monthly_basis'] = ($doctor->monthly_off/100) * $data['daily_basis'] ;
            $data['weekly_basis'] = ($doctor->weekly_off/100) * $data['daily_basis'] ;
            $data['monthly_off'] = $doctor->monthly_off ;
            $data['weekly_off'] = $doctor->weekly_off ;
        }elseif($doctor->monthly_off > 0 )
        {
            $data['monthly_basis'] = ($doctor->monthly_off/100) * $data['daily_basis'] ;
            $data['monthly_off'] = $doctor->monthly_off ;

        }elseif($doctor->weekly_off > 0)
        {
            $data['weekly_basis'] = ($doctor->weekly_off/100) * $data['daily_basis'] ;
            $data['weekly_off'] = $doctor->weekly_off ;
        }

        return $this->dataReturn($data);
    }

    public function addOrder(OrderRequest $request , $id)
    {

        $user=User::where('id' , $id)->first();

        $orderdata =$request->except('day_id');
        $orderdata['user_id'] = auth()->user()->id ;
        $orderdata['doctor_id'] = $id ;

        if($user->monthly_off > 0 )
        {
            $monthly_basis = ($user->monthly_off/100) * $user->price_hour ;

        }elseif($user->weekly_off > 0)
        {
            $weekly_basis = ($user->weekly_off/100) * $user->price_hour ;
        }else{
            $monthly_basis = null;
        }

        if ($request->type == 'monthly')
        {
            $orderdata['total_days'] = ((count($request->day_id)) * 4) ;
            $orderdata['price_after'] = $monthly_basis ;

        }elseif ($request->type == 'weekly'){
            $orderdata->total_days = count($request->day_id);
            $orderdata['price_after'] = $weekly_basis ;
        }else{
            $orderdata['total_days'] = count($request->day_id);
            $orderdata['price_after'] = $user->price_hour ;
        }
        $orderdata['start'] = $request->start ;

           $start = new Carbon($request->start);

       $orderdata['end'] = $start->addDays($orderdata['total_days']) ;

        $orderdata['total_price'] = $orderdata['hours_per_day'] * $request->hours_per_day * $orderdata['price_after'] ;

//        return $orderdata;

        $days = Day::whereIn('id' , $request->day_id)->get();

        $order = Order::create($orderdata);
//        dd($order);

        $order->days()->attach($request->day_id);

        $data['order'] = new OrderResource($order);
        $data['days'] = DayResource::collection($days);

        return $this->dataReturn($data);

    }

}
