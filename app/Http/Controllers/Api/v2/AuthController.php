<?php

namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\ActiveAccountRequest;
use App\Http\Requests\Api\Auth\CompleteRegisterRequest;
use App\Http\Requests\Api\Auth\ForgetPasswordRequest;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\LogoutRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Http\Requests\Api\Auth\ResetPasswordRequest;
use App\Http\Resources\LanguageResource;
use App\Http\Resources\SubCategoryResource;
use App\Http\Resources\UserResource;
use App\Models\Language;
use App\Models\Subcategory;
use App\Models\User;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
    use HasResponse;
    use Uploadable;

    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

//        public function callback($provider)
//    {
//        // retrieve social user info
//        $socialUser = Socialite::driver($provider)->stateless()->user();
//
//        // check if social user provider record is stored
//        $userSocialAccount = SocialAccount::where('provider_id', $socialUser->id)->where('provider_name', $provider)->first();
//
//        if ($userSocialAccount) {
//
//            // retrieve the user from users store
//            $user = User::find($userSocialAccount->user_id);
//
//            // assign access token to user
//            $token = $user->createToken('string')->accessToken;
//
//            // return access token & user data
//            return response()->json([
//                'token' => $token,
//                'user'  => (new UserResource($user))
//            ]);
//        } else {
//
//            // store the new user record
//            $user = User::create([...]);
//
//            // store user social provider info
//            if ($user) {
//
//                SocialAccount::create([...]);
//            }
//
//            // assign passport token to user
//            $token = $user->createToken('string')->accessToken;
//            $newUser = new UserResource($user);
//            $responseMessage = 'Successfully Registered.';
//            $responseStatus = 201;
//
//            // return response
//            return response()->json([
//                'responseMessage'   => $responseMessage,
//                'responseStatus'    => $responseStatus,
//                'token' => $token,
//                'user' => $newUser
//            ]);
//        }
//
//    }

    public function Register(RegisterRequest $request)
    {

            $data =$request->except('password_confirmation' , 'device_id' , 'device_type',);
            $data['type'] = $request->type ;
            if ($request->has('avatar')) {
                $data['avatar'] = $this->uploadOne($request->avatar, 'users', true, 250, null);
            }

        $user = User::create($data);


        $request['token'] = $token = $user->createToken('Laravel Password Grant Client')->accessToken;

        $user->createOrUpdateDevice($request->toArray());

        $user->sendVerificationCode();

        return $this->dataReturn([
            'token' => $token,
            'user' => new UserResource($user),
        ]);
    }

    public function completeRegister(CompleteRegisterRequest $request)
    {

        $user=auth()->user();

        if ($user->type == 'doctor') {
            $data = $request->except('subcategory_id' , 'language_id' );
            $subcategories = Subcategory::whereIn('id', $request->subcategory_id)->get();
            $languages = Language::whereIn('id', $request->languages_id)->get();

            if ($request->has('avatar')) {
                $data['avatar'] = $this->uploadOne($request->avatar, 'users', true, 250, null);
            }
            $user->subcategories()->attach($request->subcategory_id);
            if ($languages) {
                $user->languages()->attach($request->languages_id);
                $collection = LanguageResource::collection($languages);
            } else {
                $collection = null;
            }

            return $this->dataReturn([
                'subcategories' => SubCategoryResource::collection($subcategories),
                'languages' => $collection,
            ]);
        }else{

            $data = UserResource::collection($user);

            return $this->successReturn('' , $data);
        }
    }

    public function login(LoginRequest $request)
    {

        if ($request->type == 'doctor') {
            $user = User::where('phone', $request->phone)->where('type', 'doctor')->firstOrFail();

            if (!Hash::check($request->password, $user->password)) {
                return $this->failMsg(__('auth.password'));
            }
            if (! $user->price_hour)
            {
                return response([
                 'value' => '0' ,
                'key' => "fail",
                'status' => false,
                'msg' => 'not completed' ,
                'code' => 401 ,
                    ]);
            }
        }elseif ($request->type == 'patient') {
            $user = User::where('phone', $request->phone)->where('type', 'patient')->firstOrFail();
//            dd($user);
            if (!Hash::check($request->password, $user->password)) {
                return $this->failMsg(__('auth.password'));
            }
            }else {
            $user = User::where('phone', $request->phone)->where('type', 'admin')->firstOrFail();

            if (!Hash::check($request->password, $user->password)) {
                return $this->failMsg(__('auth.password'));
            }
        }

        $data['token'] = $user->createToken('Laravel Password Grant Client')->accessToken;
        $data['user'] = new UserResource($user);
        return $this->dataReturn($data);
    }

    public function logout(LogoutRequest $request)
    {
//        dd($request->all());
        $token = $request->user()->token();
        $request->user()->deleteDevice($request->toArray());

        $token->revoke();
        return $this->successMsg('You have been successfully logged out!');
    }

    public function accountActivation(ActiveAccountRequest $request)
    {
        $user = auth('api')->user();
        $user->markAsActive();

        $user->createOrUpdateDevice($request->toArray());

        $data['user'] = new UserResource($user);
        return $this->dataReturn($data);
    }

    public function forgetPassword(ForgetPasswordRequest $request)
    {
        $user = User::where('phone', $request->phone)->firstOrFail();
        $user->sendVerificationCode();

        return $this->successMsg('تم ارسال الكود بنجاح');
    }

    public function resetPassword(Request $request)
    {
        $user = User::where('phone' ,$request->phone)->first();
//            dd($user);
        $user->resetCode($request->password);

        return $this->successMsg(__('site.alerts.activated_successfully'));
    }

//    public function resetPassword(ResetPasswordRequest $request)
//    {
//        auth('api')->user()->resetCode($request->password);
//
//        return $this->successMsg(__('site.alerts.activated_successfully'));
//    }

    public function resendCode()
    {
        auth('api')->user()->sendVerificationCode();

        return $this->successMsg(__('auth.code_sent'));

    }
}
