<?php

namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Http\Resources\QueriesResource;
use App\Http\Resources\SubCategoryResource;
use App\Models\Device;
use App\Models\Order;
use App\Models\Queries;
use App\Models\Subcategory;
use App\Models\User;
use App\Traits\HasResponse;
use App\Traits\Notifications\FcmAndDBTrait;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;

class DoctorController extends Controller
{
    use HasResponse;
    use Uploadable;
    use FcmAndDBTrait;

        //doctors subcategories

    public function getsubcategories($id)
    {
        $subcategories = Subcategory::where('category_id' , $id)->where('type','dashboard')->get();
        $userSubcategories=Subcategory::where('category_id' , $id)->where('user_id' , auth()->user()->id)->get();

        $data['subcategories'] = SubCategoryResource::collection($subcategories);
        $data['userSubcategories'] = SubCategoryResource::collection($userSubcategories);

        return $this->dataReturn($data);

    }

    public function addSubcategory(Request $request , $id)
    {
        $validator = Validator::make($request->all(), [
            'title_ar'=>'required|unique:subcategories',
            'title_en'=>'required|unique:categories',
            'logo'=>'required|image|mimes:jpg,jpeg,svg,png',
        ]);

        if ($validator->fails()) {
            return $this->requestFailsReturn($validator);
        }
        $insrt = $request->all() ;
        $insrt ['user_id'] = auth()->user()->id ;
        $insrt ['category_id'] = $id ;
        $insrt['type'] = 'doctor' ;

        if ($request->has('logo')) {
            $insrt['logo'] = $this->uploadOne($request->logo, 'subcategories', true, 250, null);
        }

        $subcategory = Subcategory::create($insrt);

        $data['subcategory'] = new SubCategoryResource($subcategory);

        return $this->dataReturn($data);
    }

    public function editSubcategory($id)
    {
        $subcategory =Subcategory::where('id' , $id)->first();


        if ( $subcategory->type == 'doctor' && $subcategory->user_id == auth()->user()->id)
        {
            $data['subcategory'] = new SubCategoryResource($subcategory);

            return $this->dataReturn($data);
        }
        return 'not allowed' ;

    }

    public function updateSubcategory(Request $request , $id)
    {
        $validator = Validator::make($request->all(), [
            'title_ar' => 'required',
            'title_en' => 'required',
            'logo' => 'mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return $this->requestFailsReturn($validator);
        }

        $subcategory = Subcategory::where('id', $id)->first();
        if ($subcategory->type == 'doctor' && $subcategory->user_id == auth()->user()->id){
        $update =$request->all();

        if ($request->has('logo')) {

            File::delete(public_path('assets/uploads/subcategories/' . $subcategory->logo));

            $update['logo'] = $this->uploadOne($request->logo, 'subcategories', true, 250, null);
        } else {
            $update['logo'] = $subcategory->logo;
        }

//        if ($subcategory->type == 'doctor' && $subcategory->user_id == auth()->user()->id){
//            $update['type'] = 'doctor';
//        $update['user_id'] = auth()->user()->id;

        $subcategory->update($update);

//        }
        $data['subcategory'] = new SubCategoryResource($subcategory);

        return $this->dataReturn($data);
        }else{
            return $this->failMsg("You don't have permission to do that");
        }
    }

    public function deleteSubcategory($id)
    {

        $subcategory =Subcategory::where('id' , $id)->first();

        if ($subcategory->type == 'doctor' && $subcategory->user_id == auth()->user()->id) {
            File::delete(public_path('assets/uploads/subcategories/' .$subcategory->photo));
            $subcategory->delete();

            return $this->successMsg(__('site.alerts.deleted_successfully'));
        }
        return 'not allowed' ;
    }

            //GET Orders

    public function newOrders()
    {
        $neworders = Order::where('type' , 'new')->where('doctor_id',auth()->user()->id)->get();
        $data = OrderResource::collection($neworders);
        return $this->dataReturn($data);
    }

    public function finishedOrders()
    {
        $finishedorders = Order::where('type' , 'finished')->where('doctor_id',auth()->user()->id)->get();
        $data = OrderResource::collection($finishedorders);
        return $this->dataReturn($data);
    }

    public function activeOrders()
    {
        $activeorders = Order::where('type' , 'active')->where('doctor_id',auth()->user()->id)->get();
        $data = OrderResource::collection($activeorders);
        return $this->dataReturn($data);
    }

        //order buttons

    public function activebutton(Request $request)
    {
        $order = Order::where('id' , $request->order_id)->first();
        if($request->is_agree == 1)
        {
           $order->update([
                'status'=>'active',
                'is_agree' => $request->is_agree,
            ]);
        }
        $data['order'] = new OrderResource($order);

        $notified_user['user_id'] = $order->user_id ;
        $notified_user['devices'] = Device::where('user_id' , $order->user_id)->get() ;
        $title = 'Finish order';
        $message_ar =  'بتنشيط طلبك'. $order->doctor->name .' لقد قام';
        $message_en = 'Doctor Accepted your order';
        $type = 'Order';
        $fcmdata = [];
        $fcmdata['title'] = $title;
        $fcmdata['user_id'] = auth()->user()->id;
        $fcmdata['message_ar'] = $message_ar;
        $fcmdata['message_en'] = $message_en;
        $fcmdata['orders_notify'] =true ;
        $fcmdata['key'] = $type;
//        $fcmdata['not_seen_notifications'] = $notified_user->notifications()->where('seen', 0)->count();

        $this->sendNotification($notified_user, $type , $fcmdata,true);

        return $this->successReturn('' , $data);
    }
   public function rejectbutton(Request $request)
    {
        $order = Order::where('id' , $request->order_id)->first();
        if($request->is_agree == 0)
        {
           $order->update([
                'status'=>'rejected',
                'is_agree' => $request->is_agree,
            ]);
        }
        $data['order'] = new OrderResource($order);

        $notified_user['user_id'] = $order->user_id ;
        $notified_user['devices'] = Device::where('user_id' , $order->user_id)->get() ;
        $title = 'Finish order';
        $message_ar =  'برفض طلبك'. $order->doctor->name .' لقد قام';
        $message_en = 'Doctor rejected your order';
        $type = 'Order';
        $fcmdata = [];
        $fcmdata['title'] = $title;
        $fcmdata['user_id'] = auth()->user()->id;
        $fcmdata['message_ar'] = $message_ar;
        $fcmdata['message_en'] = $message_en;
        $fcmdata['orders_notify'] =true ;
        $fcmdata['key'] = $type;
//        $fcmdata['not_seen_notifications'] = $notified_user->notifications()->where('seen', 0)->count();

        $this->sendNotification($notified_user, $type , $fcmdata,true);

        return $this->successReturn('' , $data);
    }

    public function finishbutton(Request $request)
    {
        $order = Order::where('id' , $request->order_id)->first();
         if($order->end > Carbon::today())
    {
        $order->update([
            'status'=>'have_finished',
        ]);
        $data['order'] = new OrderResource($order);

        $notified_user['user_id'] = $order->user_id ;
        $notified_user['devices'] = Device::where('user_id' , $order->user_id)->get() ;
        $title = 'Finish order';
        $message_ar =' تم انهاء الفتره من قبل '.$order->doctor->name.' هل تريد تاكيد الانتهاء؟';
        $message_en = 'Doctor has finished your order. Are you want to finish it?';
             $type = 'Order';
             $fcmdata = [];
             $fcmdata['title'] = $title;
             $fcmdata['user_id'] = auth()->user()->id;
             $fcmdata['message_ar'] = $message_ar;
             $fcmdata['message_en'] = $message_en;
             $fcmdata['orders_notify'] =true ;
             $fcmdata['key'] = $type;
//        $fcmdata['not_seen_notifications'] = $notified_user->notifications()->where('seen', 0)->count();

             $this->sendNotification($notified_user, $type , $fcmdata,true);

             return $this->successReturn('' , $data);
    }else {
             $order->update([
                 'status'=>'finished',
             ]);
             $data['order'] = new OrderResource($order);
             $notified_user['user_id'] = $order->user_id ;
             $notified_user['devices'] = Device::where('user_id' , $order->user_id)->get() ;
             $title = 'Finish order';
             $message_ar = $order->doctor->name.' تم انهاء الفتره من قبل';
             $message_en = 'Doctor has finished your order';
             $type = 'Order';
             $fcmdata = [];
             $fcmdata['title'] = $title;
             $fcmdata['user_id'] = auth()->user()->id;
             $fcmdata['message_ar'] = $message_ar;
             $fcmdata['message_en'] = $message_en;
             $fcmdata['key'] = $type;
             $fcmdata['orders_notify'] =true ;
//        $fcmdata['not_seen_notifications'] = $notified_user->notifications()->where('seen', 0)->count();

             $this->sendNotification($notified_user, $type , $fcmdata,true);

             return $this->successReturn('' , $data);
         }

    }

    //answer inquiere
    public function answerInqueires(Request $request , $id)
    {
        $validator = Validator::make($request->all(),[
            'answer'=>'required'
        ]);
        if ($validator->fails())
        {
            return $this->requestFailsReturn($validator);
        }
        $query = Queries::where('id' , $id)->first();
        $query->update([
            'answer' => $request->answer,
            ]);
        $data['queries'] = new QueriesResource($query);
        $notified_user['user_id'] = $query->user_id ;
        $notified_user['devices'] = Device::where('user_id' , $query->user_id)->get() ;
        $title = 'Answered';
        $message_ar ='لقد رد الطبيب على استفسارك';
        $message_en = 'Doctor has answered your query';
        $type = 'Queries';
        $fcmdata = [];
        $fcmdata['title'] = $title;
        $fcmdata['user_id'] = auth()->user()->id;
        $fcmdata['message_ar'] = $message_ar;
        $fcmdata['message_en'] = $message_en;
        $fcmdata['key'] = $type;
//        $fcmdata['not_seen_notifications'] = $notified_user->notifications()->where('seen', 0)->count();

        $this->sendNotification($notified_user, $type , $fcmdata,true);

        return $this->successReturn('Answer sent successfully' ,$data);

    }
    // view quires
    public function viewInqueries()
    {
        $queries = Queries::where('doctor_id' , auth()->user()->id)->get();

        $data['queries'] = QueriesResource::collection($queries);

        return $this->dataReturn($data);
    }
    // view one Query
    public function oneQuery($id)
    {
        $queries = Queries::where('id' , $id)->first();
        $data['query'] = new QueriesResource($queries);

        return $this->dataReturn($data);
    }


}
