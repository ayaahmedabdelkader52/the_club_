<?php

namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateDoctorRequest;
use App\Http\Resources\LanguageResource;
use App\Http\Resources\SubCategoryResource;
use App\Http\Resources\UserResource;
use App\Models\Language;
use App\Models\Subcategory;
use App\Models\User;
use App\Models\UserLanguage;
use App\Models\UserSubcategory;
use App\Traits\HasResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    use HasResponse;
    //Doctor

    public function show()
    {
        $user = auth('api')->user();
        $findsubcategories =UserSubcategory::where('user_id' , $user->id)->get();
        $subcategories_id =[] ;
        foreach($findsubcategories as $item)
        {
            $subcategories_id[] = $item->subcategory_id;
        }
        $subcategories =Subcategory::whereIn('id' , $subcategories_id )->get();
        $findlanguages = UserLanguage::where('user_id' , $user->id)->get();
        $languages_id =[] ;
        foreach($findlanguages as $subcategory)
        {
            $languages_id[] = $subcategory->language_id;
        }
        $languages = Language::whereIn('id' , $languages_id )->get();
        $data['user'] = new UserResource($user);
        $data['subcategories'] = SubCategoryResource::collection($subcategories);
        $data['languages'] = LanguageResource::collection($languages);
        return $this->dataReturn($data);
    }

    // Patient
    public function view()
    {
        $user = auth('api')->user();
        $data['user'] = new UserResource($user);
        return $this->dataReturn($data);
    }

    public function updatedoctor(UpdateDoctorRequest $request)
    {

        dd($request->validated());
        $user = auth('api')->user();

          $data = $request->except('language_id' , 'subcategory_id','password_confirmation');


        if ($request->has('subcategory_id')) {
            $oldsubcategories = UserSubcategory::where('user_id', $user->id)->get();
            UserSubcategory::destroy($oldsubcategories);
//                foreach ($request->subcategory_id as $subcategory)
//                {
//                    UserSubcategory::create([
//                        'subcategory_id'=>$subcategory,
//                        'user_id'=>$user->id,
//                    ]);
//                }
            $user->subcategories()->attach($request->subcategory_id);
        }
            if ($request->has('language_id')) {
                $oldlanguages = UserLanguage::where('user_id', $user->id)->get();
                UserSubcategory::destroy($oldlanguages);
//                foreach ($request->language_id as $subcategory)
//                {
//                    UserLanguage::create([
//                        'language_id'=>$subcategory,
//                        'user_id'=>$user->id,
//                    ]);
//                }
                $user->languages()->attach($request->language_id);

            }

        $user->update($data);

        $data['user'] = new UserResource($user);

        return $this->successReturn('', $data);
    }

    public function updatepatient(Request $request)
    {

        $user = auth('api')->user();

        $validator = Validator::make($request->all(), [
            'email' => ['nullable', 'email', 'unique:users,email,' . $user->id],
            'name' => 'string|max:255',
            'phone' => 'unique:users',
            'password' => 'min|6',
            'password_confirmation' => 'required_with:password|same:password|min:6',
            'avatar' => 'image|mimes:jpg,jpeg,svg,png',
            'device_id' => 'required',
            'device_type' => 'required|in:ios,android',

        ]);


        if ($validator->fails())
        {
            return $this->requestFailsReturn($validator);
        }

        $user->update($request->all());

        $data['user'] = new UserResource($user);

        return $this->successReturn('', $data);
    }

}
