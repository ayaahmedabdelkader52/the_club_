<?php

namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\HasResponse;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    use HasResponse;
    public function facebookRedirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function loginWithFacebook()
    {
        try {

            $user = Socialite::driver('facebook')->user();
            $isUser = User::where('fb_id', $user->id)->first();

            if($isUser){
                Auth::login($isUser);
//                $data['token'] = $user->createToken('Laravel Password Grant Client')->accessToken;
                $data['user'] = new UserResource($user);
                return $this->dataReturn($data);
            }else{
                $createUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'fb_id' => $user->id,
                    'password' => encrypt('admin@123')
                ]);

                Auth::login($createUser);
//                $data['token'] = $user->createToken('Laravel Password Grant Client')->accessToken;
                $data['user'] = new UserResource($user);
                return $this->dataReturn($data);
            }

        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }
}
