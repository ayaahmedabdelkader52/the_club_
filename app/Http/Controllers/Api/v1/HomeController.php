<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\AdsResource;
use App\Http\Resources\SliderResource;
use App\Models\Ads;
use App\Models\Slider;
use App\Traits\HasResponse;

class HomeController extends Controller
{
    use HasResponse;

    public function index()
    {
        $sliders = Ads::whereDate('expiry_date', '<', now())->get();

        $data['sliders'] = AdsResource::collection($sliders);

        return $this->dataReturn($data);
    }

    public function sliders()
    {
        $sliders = Slider::orderBy('order')->get();
        $data['sliders'] = SliderResource::collection($sliders);

        return $this->dataReturn($data);
    }
}
