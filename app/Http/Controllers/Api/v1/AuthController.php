<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\ActiveAccountRequest;
use App\Http\Requests\Api\Auth\ForgetPasswordRequest;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\LogoutRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Http\Requests\Api\Auth\ResetPasswordRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\HasResponse;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use HasResponse;

    public function register(RegisterRequest $request)
    {
        $user = User::create($request->except('password_confirmation', 'device_id', 'device_type'));

        $request['token'] = $token = $user->createToken('Laravel Password Grant Client')->accessToken;

        $user->createOrUpdateDevice($request->toArray());
        $user->sendVerificationCode();

        return $this->dataReturn([
            'token' => $token,
            'user' => new UserResource($user)
        ]);
    }

    public function login(LoginRequest $request)
    {

        $user = User::where('phone', $request->phone)->firstOrFail();

        if (!Hash::check($request->password, $user->password)) {
            return $this->failMsg(__('auth.password'));
        }

        $data['token'] = $user->createToken('Laravel Password Grant Client')->accessToken;
        $data['user'] = new UserResource($user);
        return $this->dataReturn($data);
    }

    public function logout(LogoutRequest $request)
    {
        $token = $request->user()->token();
        $request->user()->deleteDevice($request->toArray());

        $token->revoke();
        return $this->successMsg('You have been successfully logged out!');
    }

    public function accountActivation(ActiveAccountRequest $request)
    {
        $user = auth('api')->user();
        $user->markAsActive();

        $user->createOrUpdateDevice($request->toArray());

        $data['user'] = new UserResource($user);
        return $this->dataReturn($data);
    }

    public function forgetPassword(ForgetPasswordRequest $request)
    {
        $user = User::where('phone', $request->phone)->firstOrFail();
        $user->sendVerificationCode();

        return $this->successMsg('تم ارسال الكود بنجاح');
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        auth('api')->user()->resetCode($request->password);

        return $this->successMsg(__('site.alerts.activated_successfully'));
    }

    public function resendCode()
    {
        auth('api')->user()->sendVerificationCode();

        return $this->successMsg(__('auth.code_sent'));

    }
}
