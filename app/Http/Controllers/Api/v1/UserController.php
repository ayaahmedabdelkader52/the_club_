<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\HasResponse;

class UserController extends Controller
{
    use HasResponse;

    public function index()
    {
        $users = User::paginate();
        $data['user'] = UserResource::collection($users);
        $data['pagination'] = $this->paginate($users);

        return $this->dataReturn($data);
    }

}
