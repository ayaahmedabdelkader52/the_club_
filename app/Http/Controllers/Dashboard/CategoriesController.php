<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

use App\Http\Requests\Category\StoreCategoryRequest;
use App\Http\Requests\Category\UpdateRequest;
use App\Models\Category;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;


class CategoriesController extends Controller
{
    use Uploadable;
    use HasResponse;

    public function index()
    {
        $categories = Category::all();

        return view('admin.categories.index', compact('categories'));
    }
    public function create(){

        return view('admin.categories.create');
    }

    public function store(StoreCategoryRequest $request){


        $data = $request->validated();

        if ($request->has('logo')) {

            $data['logo'] = $this->uploadOne($request->logo, 'categories', true, 250, null);
        }

        Category::create($data);

        auth()->user()->saveReport('تعديل قسم');

        return redirect()->route('admin.categories.index')->with('success', trans('dashboard.created_successfully'));

    }

    public function edit(Category $category )
    {
        return view('admin.categories.edit' , compact('category'));
    }

    public function update(UpdateRequest $request , Category $category)
    {

        $data = $request->validated();

        if ($request->has('logo')) {

            File::delete(public_path('assets/uploads/categories/' .$category->logo));

            $data['logo'] = $this->uploadOne($request->logo, 'categories', true, 250, null);
        }else{
            $data['logo'] = $category -> logo ;
        }

        $category->update($data);

        auth()->user()->saveReport('تعديل قسم');

        return redirect()->route('admin.categories.index')->with('success', trans('dashboard.alerts.updated_successfully'));

    }

    public function destroy(Category $category)
    {
        File::delete(public_path("assets/uploads/categories/" . $category->logo));

        $category ->delete();

        auth()->user()->saveReport(__('dashboard.categories.delete_product'));

        return self::successReturn( '' , $category);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request -> ids;

        $categories = Category::find($ids);
        foreach ($categories as $category)
        {
            File::delete(public_path('assets/uploads/categories/' .$category->logo));

        }

        Category::destroy($categories);

        auth()->user()->saveReport('حذف الاقسام');

        return self::successReturn('', $ids);

    }
}
