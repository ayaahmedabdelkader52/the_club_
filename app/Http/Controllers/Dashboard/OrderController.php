<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Languages\LanguageStoreRequest;
use App\Http\Requests\Languages\LanguageUpdateRequest;
use App\Models\Day;
use App\Models\Language;
use App\Models\Order;
use App\Models\OrderDay;
use App\Traits\HasResponse;
use Illuminate\Http\Request;

class OrderController extends Controller


{
    use HasResponse;

    public function index()
    {
        $orders = Order::all();

        return view('admin.orders.index', compact('orders'));
    }
    public function finished()
    {
        $orders = Order::where('status' , 'finished')->get();

        return view('admin.orders.finished', compact('orders'));
    }
    public function active()
    {
        $orders = Order::where('status' , 'active')->get();

        return view('admin.orders.active', compact('orders'));
    }
    public function rejected()
    {
        $orders = Order::where('status' , 'rejected')->get();

        return view('admin.orders.rejected', compact('orders'));
    }
    public function pending()
    {
        $orders = Order::where('status' , 'new')->get();

        return view('admin.orders.pending', compact('orders'));
    }

    public function show(Order $order)
    {
        $ids = OrderDay::where('order_id' , $order->id)->get();
//        return $ids;
//        $days = [] ;
        foreach ($ids as $id)
       $days [] = Day::where('id' , $id->day_id)->first();
//        $days=  $order->days->id;
//        dd($days);
        return view('admin.orders.show', compact('order' , 'days'));
    }


    public function destroy(Language $language)
    {

        $language ->delete();

        auth()->user()->saveReport(__('dashboard.language.delete_product'));

        return self::successReturn( '' , $language);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request -> ids;

        $languages = Language::find($ids);

        Language::destroy($languages);

        auth()->user()->saveReport('حذف اللغات');

        return self::successReturn('', $ids);

    }
}
