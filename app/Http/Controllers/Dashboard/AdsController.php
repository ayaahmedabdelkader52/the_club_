<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdsRequest;
use App\Http\Requests\UpdateAdsRequest;
use App\Models\Ads;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Http\Request;

class AdsController extends Controller
{
    use Uploadable, HasResponse;

    public function index()
    {
        $ads = Ads::latest()->get();
        return view('admin.ads.index', compact('ads'));
    }

    public function create()
    {
        return view('admin.ads.create');
    }

    public function store(AdsRequest $request)
    {
        Ads::create($request->validated());

        auth()->user()->saveReport('اضافة اعلان');
        return redirect()->route('admin.ads.index')->with('success', 'تم اضافه الاعلان بنجاح');

    }

    public function edit(Ads $ad)
    {
        return view('admin.ads.edit', compact('ad'));
    }

    public function update(UpdateAdsRequest $request, Ads $ad)
    {
        $data = $request->validated();

//        if ($request->image)
//            $data['image'] = $request->image ? $this->uploadOne($request->image, 'ads', true, 250, null) : '';
//
        $ad->update($data);

        auth()->user()->saveReport('تعديل اعلان');
        return redirect()->route('admin.ads.index')->with(['success' => 'تم تعديل الاعلان بنجاح']);
    }

    public function destroy(Ads $ad)
    {
        $data = ['adId' => $ad->id,];
        $ad->delete();

        auth()->user()->saveReport('حذف اعلان');
        return self::successReturn('', $data);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request->ads;
        $ads = Ads::find($ids);

        Ads::destroy($ads);

        auth()->user()->saveReport('حذف اعلانات');
        return self::successReturn('', $ids);
    }
}
