<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\FaqRequest;
use App\Models\Faq;
use App\Traits\HasResponse;
use Illuminate\Http\Request;

class FaqsController extends Controller
{
    use  HasResponse;

    public function index()
    {

        $faqs = Faq::latest()->get();
        return view('admin.faqs.index', compact('faqs'));
    }

    public function create()
    {
        return view('admin.faqs.create');
    }

    public function store(FaqRequest $request)
    {
        $data = $request->validated();

        Faq::create($data);

        auth()->user()->saveReport('اضافة سؤال');
        return redirect()->route('admin.faqs.index')->with(['success' => 'تم االاضافه بنجاح']);
    }

    public function edit(Faq $faq)
    {
        return view('admin.faqs.edit', compact('faq'));
    }

    public function update(FaqRequest $request, Faq $faq)
    {
        $data = $request->validated();
        $faq->update($data);

        auth()->user()->saveReport('تعديل سؤال');
        return redirect()->route('admin.faqs.index')->with(['success' => 'تم التعديل بنجاح']);
    }

    public function destroy(Faq $faq)
    {
        $data = ['faqId' => $faq->id,];
        $faq->delete();

        auth()->user()->saveReport('حذف سؤال');
        return self::successReturn('', $data);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request->faqs;
        $faqs = Faq::find($ids);

        Faq::destroy($faqs);

        auth()->user()->saveReport('حذف متعدد للاسئلة');
        return self::successReturn('', $ids);
    }
}
