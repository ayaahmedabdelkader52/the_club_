<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\District;
use App\Traits\HasResponse;
use Illuminate\Http\Request;
use App\Http\Requests\District\UpdateRequest;
use App\Http\Requests\District\StoreRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\districtsExport;

class DistrictController extends Controller
{
    use HasResponse;
    public function index(City $city)
    {
        $districts = $city->districts;

        return view('admin.districts.index', compact('districts', 'city'));
    }

    public function create(City $city)
    {
        return view('admin.districts.create', compact('city'));
    }

    public function store(StoreRequest $request)
    {
        District::create($request->validated());

        auth()->user()->saveReport('اضافة حي');
        return redirect()->route('admin.cities.districts', $request->input('city_id'))->with('success', __('dashboard.alerts.created_successfully'));
    }

    public function show(District $district)
    {
        $city = $district->city;
        return view('admin.districts.show', compact('district', 'city'));
    }

    public function edit(District $district)
    {
        // dd($district->id);
        $city = $district->city;
        return view('admin.districts.edit', compact('district', 'city'));
    }

    public function update(UpdateRequest $request, District $district)
    {
        $district->update($request->validated());

        auth()->user()->saveReport('تعديل حي');
        return redirect()->route('admin.cities.districts', $request->input('city_id'))->with('success', trans('dashboard.alerts.updated_successfully'));
    }

    public function destroy(District $district)
    {
        $district->delete();

        auth()->user()->saveReport('حذف حي');
        return $this->successReturn(__('dashboard.alerts.deleted_successfully'));
    }

    public function destroySelected(Request $request)
    {
        $ids = $request->districts;
        $districts = District::find($ids);

        District::destroy($districts);

        auth()->user()->saveReport('حذف متعدد للأحياء');
        return $this->successReturn(__('dashboard.alerts.deleted_successfully'));
    }

    public function downloadAllCategories()
    {
        auth()->user()->saveReport('تنزيل الاحياء كملف Excel');
        return Excel::download( new districtsExport(), 'all-districts.xlsx');
    }

}
