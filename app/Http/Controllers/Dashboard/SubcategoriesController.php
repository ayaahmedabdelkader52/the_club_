<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubCategory\StoreSubRequest;
use App\Http\Requests\SubCategory\UpdateSubRequest;
use App\Models\Category;
use App\Models\Subcategory;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SubcategoriesController extends Controller
{


    use Uploadable;
    use HasResponse;

    public function index()
    {
        $subcategories = Subcategory::all()  ;


        return view('admin.subcategories.index', compact('subcategories' ));
    }

    public function create()
    {

        $categories = Category::all();

        return view('admin.subcategories.create' , compact(  'categories'));
    }

    public function store(StoreSubRequest $request)
    {
        $data=$request->validated();
        if($request->has('logo')) {
            $data['logo'] = $this->uploadOne($request->logo, 'subcategories', true, 250, null);
        }
        $data['type'] = 'dashboard';
       Subcategory::create($data);

        auth()->user()->saveReport(__('dashboard.subcategories.create_products'));

        return redirect()->route('admin.subcategories.index')->with('success', __('dashboard.alerts.created_successfully'));
    }

    public function edit(Subcategory $subcategory)
    {
        $categories = Category:: all();

        return view('admin.subcategories.edit', compact('subcategory','categories'));
    }

    public function update(UpdateSubRequest $request, Subcategory $subcategory)
    {
        $data = $request->all();

        if ($request->has('logo')) {
            File::delete(public_path("assets/uploads/subcategories/" . $subcategory->logo));

            $data['logo'] = $this->uploadOne($request->logo, 'subcategories', true, 250, null);
        }else{
            $data['logo'] = $subcategory->logo;
        }

        $subcategory->update($data);

        auth()->user()->saveReport(__('dashboard.subcategories.edit_product'));

        return redirect()->route('admin.subcategories.index')->with('success', trans('dashboard.alerts.updated_successfully'));
    }

    public function destroy(Subcategory $subcategory)
    {
        File::delete(public_path("assets/uploads/subcategories/" . $subcategory->photo));

        $subcategory->delete();

        auth()->user()->saveReport(__('dashboard.subcategories.delete_product'));

        return self::successReturn('', $subcategory);

    }

    public function destroySelected(Request $request)
    {

        $ids = $request-> ids ;

        $subcategories = Subcategory::find($ids);

        foreach ($subcategories as $subcategory)
        {
            File::delete(public_path("assets/uploads/subcategories/" . $subcategory->photo));
        }
        Subcategory::destroy($subcategories);

        auth()->user()->saveReport('حذف التخصصات');

        return self::successReturn('', $ids);
    }



}
