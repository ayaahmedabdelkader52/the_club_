<?php

namespace App\Http\Controllers\Dashboard;

use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\Role;
use App\Models\Setting;
use App\Models\User;
use App\Traits\HasResponse;
use App\Traits\PermissionTrait;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    use Uploadable, HasResponse, PermissionTrait;

    public function index()
    {

        $users = User::with('role')->get();

        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::all();

        return view('admin.users.create', compact('roles'));
    }

    public function store(StoreUserRequest $request)
    {
        $data = $request->validated();

        $filteredPhone = User::cleanPhone($data['phone']);
        $data['phone'] = $request['phone'] = $filteredPhone['phone'];
        $data['country_key'] = $filteredPhone['country_key'];
        $data['password'] = Hash::make($data['password']);

        // To Validate Phone after filter phone
        $request->validate([
            'phone' => ['unique:users,phone']
        ]);

        if ($request->has('avatar')) {
            $data['avatar'] = $this->uploadOne($request->avatar, 'users', true, 250, null);
        }

        $user = User::create($data);
        if ($request->status == 'pending') {
            // Send Verification code
            User::sendVerificationCode($user);
        }

        auth()->user()->saveReport('اضافة عضو');

        return redirect()->route('admin.users.index')->with('success', trans('dashboard.created_successfully'));
    }

    public function show(User $user)
    {
        if (!$user->role)
            return back()->with('error', __('dashboard.alerts.something went_wrong_please_try_again'));

        $user->role->load('permissions');
        $superPermissions = $this->getAll();
        $roleUserPermissions = $user->role->permissions->pluck('name')->toArray();

        return view('admin.users.show', compact('user', 'superPermissions', 'roleUserPermissions'));


    }


    public function edit(User $user)
    {
        $roles = Role::all();

        return view('admin.users.edit', compact('user', 'roles'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $data = $request->validated();

        $filteredPhone = User::cleanPhone($data['phone']);
        $data['phone'] = $filteredPhone['phone'];
        $data['country_key'] = $filteredPhone['country_key'];

        // To Validate after filter phone
        $request->validate([
            'phone' => ['required', Rule::unique('users')->ignore($user->id, 'id')],
        ]);

        if ($request->has('avatar')) {
            if ($user->avatar != 'default.png')
                File::delete(public_path('assets/uploads/users/' . $user->avatar));

            $data['avatar'] = $this->uploadOne($request->avatar, 'users', true, 250, null);
        }

        $user->update($data);

        auth()->user()->saveReport('تعديل عضو');

        return redirect()->route('admin.users.index')->with('success', __('dashboard.Updated successfully!'));
    }

    public function destroy(User $user)
    {
        $data = ['userId' => $user->id,];

        if ($user->avatar != 'default.png')
            File::delete(public_path('assets/uploads/users/' . $user->avatar));

        $user->delete();

        auth()->user()->saveReport('حذف عضو');

        return self::successReturn('', $data);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request->users;
        $users = User::find($ids);

        foreach ($users as $user) {
            if ($user->avatar != 'default.png')
                File::delete(public_path('assets/uploads/users/' . $user->avatar));
        }

        User::destroy($users);

        auth()->user()->saveReport('حذف اعضاء');

        return self::successReturn('', $ids);
    }

    public function downloadAllUsers()
    {
        auth()->user()->saveReport('تنزيل الاعضاء كملف Excel');
        return Excel::download(new UsersExport('users'), 'all-users.xlsx');
    }

    public function blocked()
    {
        $users = User::with('role')->where('status', 'block')->get();

        return view('admin.users.blocked', compact('users'));
    }

    public function block(User $user)
    {
        $user->update(['status' => 'block']);

        auth()->user()->saveReport('حظر عضو');
        return $this->successReturn(__('dashboard.alerts.blocked_successfully'));
    }

    public function unBlock(User $user)
    {
        $user->update(['status' => 'active']);
        auth()->user()->saveReport('فك حظر عضو');

        return $this->successReturn(__('dashboard.alerts.un_blocked_successfully'));
    }


    public function activeUsers()
    {
        $users = User::with('role')->where('status', 'active')->get();
        return view('admin.users.active', compact('users'));

    }
     public function doctors()
    {
        $users = User::with('role')->where('type', 'doctor')->orderBy('created_at' , 'desc')->get();
        $new=$users->toArray();
        return view('admin.users.doctor', compact('users'));

    }
    public function patients()
    {
        $users = User::with('role')->where('type', 'patient')->get();
        return view('admin.users.patient', compact('users'));

    }

    public function inActiveUsers()
    {
        $users = User::with('role')->where('status', 'pending')->get();
        return view('admin.users.inactive', compact('users'));
    }

//    public function maleUsers()
//    {
//        $users = User::with('role')->where('gender', 'male')->get();
//        return view('admin.users.male', compact('users'));
//    }

//    public function femaleUsers()
//    {
//        $users = User::with('role')->where('gender', 'female')->get();
//        return view('admin.users.female', compact('users'));
//    }


}
