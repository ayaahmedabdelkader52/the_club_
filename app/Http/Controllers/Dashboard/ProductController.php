<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Traits\HasResponse;
use Illuminate\Http\Request;
use App\Http\Requests\Product\UpdateRequest;
use App\Http\Requests\Product\StoreRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\productsExport;

class ProductController extends Controller
{
    use HasResponse;
    public function index()
    {
        $products = Product::all();

        return view('admin.products.index', compact('products'));
    }

    public function create()
    {
        return view('admin.products.create');
    }

    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        Product::create($data);

        auth()->user()->saveReport(__('dashboard.products.create_product'));

        return redirect()->route('admin.products.index')->with('success', __('dashboard.alerts.created_successfully'));
    }

    public function show(Product $product)
    {
        return view('admin.products.show', compact('product'));
    }

    public function edit(Product $product)
    {
        return view('admin.products.edit', compact('product'));
    }

    public function update(UpdateRequest $request, Product $product)
    {
        $data = $request->validated();

        $product->update($data);

        auth()->user()->saveReport(__('dashboard.products.edit_product'));

        return redirect()->route('admin.products.index')->with('success', trans('dashboard.alerts.updated_successfully'));
    }

    public function destroy(Product $product)
    {
        $product->delete();

        auth()->user()->saveReport(__('dashboard.products.delete_product'));

        return $this->successReturn(__('dashboard.alerts.deleted_successfully'));
    }

    public function destroySelected(Request $request)
    {
        $ids = $request->ids;
        $products = Product::find($ids);

        Product::destroy($products);

        auth()->user()->saveReport(__('dashboard.products.delete_product'));

        return $this->successReturn(__('dashboard.alerts.deleted_successfully'));
    }

    public function downloadAllproducts()
    {
        return Excel::download( new productsExport(), 'all-products.xlsx');
    }

}
