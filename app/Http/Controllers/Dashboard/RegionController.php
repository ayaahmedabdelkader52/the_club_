<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Region;
use App\Traits\HasResponse;
use Illuminate\Http\Request;
use App\Http\Requests\Region\UpdateRequest;
use App\Http\Requests\Region\StoreRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\regionsExport;

class RegionController extends Controller
{
    use HasResponse;

    public function index(Country $country)
    {
        $regions = Region::where('country_id', $country->id)
            ->select('id', 'name', 'country_id', 'code', 'population')
            ->get();

        return view('admin.regions.index', compact('regions', 'country'));
    }

    public function create(Country $country)
    {
        return view('admin.regions.create', compact('country'));
    }

    public function store(StoreRequest $request)
    {
        Region::create($request->validated());

        auth()->user()->saveReport('اضافة منطفة');
        return redirect()->route('admin.countries.regions', $request->input('country_id'))->with('success', __('dashboard.alerts.created_successfully'));
    }

    public function show(Region $region)
    {
        $regionCountry = $region->country;

        return view('admin.regions.show', compact('region', 'regionCountry'));
    }

    public function edit(Region $region)
    {
        $regionCountry = $region->country;

        return view('admin.regions.edit', compact('region', 'regionCountry'));
    }

    public function update(UpdateRequest $request, Region $region)
    {
        $data = $request->validated();

        $region->update($data);

        auth()->user()->saveReport('تعديل منطقة');
        return redirect()->route('admin.countries.regions', $request->input('country_id'))->with('success', __('dashboard.alerts.updated_successfully'));
    }

    public function destroy(Region $region)
    {
        $region->delete();

        auth()->user()->saveReport('حذف منطقة');
        return $this->successReturn(__('dashboard.alerts.deleted_successfully'));
    }

    public function destroySelected(Request $request)
    {
        $ids = $request->regions;
        $regions = Region::find($ids);

        Region::destroy($regions);

        auth()->user()->saveReport('حذف متعدد  للمناطق');
        return $this->successReturn(__('dashboard.alerts.deleted_successfully'));
    }

    public function downloadAllRegions()
    {
        auth()->user()->saveReport('تنزيل المناطق كملف Excel');
        return Excel::download(new regionsExport(), 'all-regions.xlsx');
    }

}
