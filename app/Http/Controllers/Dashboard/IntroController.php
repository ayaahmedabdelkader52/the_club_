<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Intro\StoreRequest;
use App\Http\Requests\Intro\UpdateRequest;
use App\Models\Intro;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class IntroController extends Controller
{

    use Uploadable;
    use HasResponse;

    public function index()
    {
        $intros = Intro::all();

        return view('admin.intros.index', compact('intros'));
    }
    public function create(){

        return view('admin.intros.create');
    }

    public function store(StoreRequest $request){

//        dd($request->all());

        $data = $request->validated();

        if ($request->has('image')) {

            $data['image'] = $this->uploadOne($request->image, 'intros', true, 250, null);
        }

        Intro::create($data);

        auth()->user()->saveReport('حفظ المقدمه');

        return redirect()->route('admin.intros.index')->with('success', trans('dashboard.created_successfully'));

    }

    public function edit(Intro $intro )
    {
        return view('admin.intros.edit' , compact('intro'));
    }

    public function update(UpdateRequest $request , Intro $intro)
    {

        $data = $request->validated();

        if ($request->has('image')) {

            File::delete(public_path('assets/uploads/intros/' .$intro->image));

            $data['image'] = $this->uploadOne($request->image, 'intros', true, 250, null);
        }else{
            $data['image'] = $intro -> image ;
        }

        $intro->update($data);

        auth()->user()->saveReport('تعديل intro');

        return redirect()->route('admin.intros.index')->with('success', trans('dashboard.alerts.updated_successfully'));

    }

    public function destroy(Intro $intro)
    {
        File::delete(public_path("assets/uploads/intros/" . $intro->image));

        $intro ->delete();

        auth()->user()->saveReport(__('dashboard.intro.delete_product'));

        return self::successReturn( '' , $intro);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request -> ids;

        $intros = Intro::find($ids);
        foreach ($intros as $intro)
        {
            File::delete(public_path("assets/uploads/intros/" . $intro->image));

        }

        Intro::destroy($intros);

        auth()->user()->saveReport('حذف intros');

        return self::successReturn('', $ids);

    }

}
