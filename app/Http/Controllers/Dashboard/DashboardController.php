<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard()
    {
//        $users = User::where('role_id', 2)->orderBy('created_at', 'desc')->get();
//        $admins = User::where('role_id', 1)->orderBy('created_at', 'desc')->get();
            $numdoctors = User::where('type' , 'doctor')->count();
            $numpatients = User::where('type' , 'patient')->count();
            $allusers = User::all()->count()  ;
            $blocked_users = User::where('status' , 'block')->count()  ;
            $active_users = User::where('status' , 'active')->count()   ;
            $pending_users = User::where('status' , 'pending')->count()   ;
            $finished_order = Order::where('status' , 'finished')->count();
            $new_order = Order::where('status' , 'new')->count();
            $active_order = Order::where('status' , 'active')->count();
            $rejected_order = Order::where('status' , 'rejected')->count();
        return view('admin.index', compact('numdoctors' , 'numpatients' ,
            'finished_order' , 'new_order' , 'active_order' ,'rejected_order' ,'allusers' , 'blocked_users',
        'active_users' , 'pending_users'));
    }

    public function them($them)
    {
        Session()->has('them') ? Session()->forget('them') : '';
        $them == 'dark' ? Session()->put('them', 'dark') : Session()->put('them', 'light');
        return back();
    }
}
