<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Languages\LanguageStoreRequest;
use App\Http\Requests\Languages\LanguageUpdateRequest;
use App\Models\Language;
use App\Traits\HasResponse;
use Illuminate\Http\Request;

class LanguagesController extends Controller

{
    use HasResponse;

    public function index()
    {
        $languages = Language::all();

        return view('admin.languages.index', compact('languages'));
    }
    public function create(){

        return view('admin.languages.create');
    }

    public function store(LanguageStoreRequest $request){


        $data = $request->validated();

        Language::create($data);

        auth()->user()->saveReport('تعديل قسم');

        return redirect()->route('admin.languages.index')->with('success', trans('dashboard.created_successfully'));

    }

    public function edit(Language $language )
    {
        return view('admin.languages.edit' , compact('language'));
    }

    public function update(LanguageUpdateRequest $request , Language $language)
    {

        $data = $request->validated();


        $language->update($data);

        auth()->user()->saveReport('تعديل قسم');

        return redirect()->route('admin.languages.index')->with('success', trans('dashboard.alerts.updated_successfully'));

    }

    public function destroy(Language $language)
    {

        $language ->delete();

        auth()->user()->saveReport(__('dashboard.language.delete_product'));

        return self::successReturn( '' , $language);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request -> ids;

        $languages = Language::find($ids);

        Language::destroy($languages);

        auth()->user()->saveReport('حذف اللغات');

        return self::successReturn('', $ids);

    }
}
