<?php

namespace App\Http\Controllers\Dashboard;

use App\Exports\CountriesExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Country\StoreRequest;
use App\Http\Requests\Country\UpdateRequest;
use App\Models\Country;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CountryController extends Controller
{
    use Uploadable, HasResponse;
    public function index()
    {
        $sar = Country::find(Country::SA_ID);
        $countries = Country::all();

        return view('admin.countries.index', compact('countries', 'sar'));
    }

    public function create()
    {
        return view('admin.countries.create');
    }

    public function store(StoreRequest $request)
    {
        Country::create($request->validated());

        auth()->user()->saveReport('اضافة دولة');
        return redirect()->route('admin.countries.index')->with('success', trans('dashboard.created_successfully'));
    }

    public function show(Country $country)
    {
        return view('admin.countries.show', compact('country'));
    }

    public function edit(Country $country)
    {

        return view('admin.countries.edit', compact('country'));
    }

    public function update(UpdateRequest $request, Country $country)
    {
        $data = $request->validated();
        $data['active'] = $request->has('active');

        $country->update($data);

        auth()->user()->saveReport('تعديل دولة');
        return redirect()->route('admin.countries.index')->with('success', __('dashboard.Updated successfully!'));
    }

    public function destroy(Country $country)
    {
        $country->delete();

        auth()->user()->saveReport('حذف دولة');
        return $this->successReturn();
    }

    public function destroySelected(Request $request)
    {
        $ids = $request->countries;
        $countries = Country::find($ids);

        Country::destroy($countries);

        auth()->user()->saveReport('حذف متعدد للدول');
        return self::successReturn('', $ids);
    }

    public function downloadAllCountries()
    {
        auth()->user()->saveReport('تنزيل الدول كملف Excel');
        return Excel::download(new CountriesExport(), 'all-countries.xlsx');
    }

}
