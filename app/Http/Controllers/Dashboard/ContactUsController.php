<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Mail\PublicMessage;
use App\Models\Contact;
use App\Models\User;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    use Uploadable, HasResponse, Uploadable;

    public function index()
    {
        $messages = Contact::latest()->get();
        return view('admin.contact.index', compact('messages'));
    }

    public function show(Contact $contact)
    {
        $contact->markAsRead();

        return view('admin.contact.show')->with(['message' => $contact]);
    }

    public function destroy(Contact $contact){
        $contact->delete();

        auth()->user()->saveReport('حذف رسالة واردة');
        return $this->successReturn();
    }

    public function destroySelected(Request $request)
    {
        $users = Contact::find($request->messages);

        Contact::destroy($users);

        auth()->user()->saveReport('حذف متعدد للرسائل الواردة');
        return $this->successReturn();
    }

    // *** Send message to Single User: Start ** //
    public function replySmsToSingleUser(SendSMSRequest $request)
    {
        $request->validate(['user_phone' => 'required',]);

        $number = $request->user_phone;

        $this->sendSms($number, $request->sms_message_content);

        auth()->user()->saveReport(' الرد علي رسالة واردة ب SMS');

        return back()->with('success', __('dashboard.alerts.message_sent_successfully'));
    }

    public function replyEmailToSingleUser(SendEmailRequest $request)
    {
        $request->validate(['user_email' => 'required',]);

        if (!$this->checkEmailConfig()) {
            return back()->with('error', 'لم يتم ارسال الرساله ! .. يرجى مراجعة بيانات ال SMTP');
        }

        Mail::to($request->user_email)->send(new PublicMessage($request->email_message_content));

        auth()->user()->saveReport(' الرد علي رسالة واردة بايميل');

        return back()->with('success', __('dashboard.alerts.message_sent_successfully'));
    }

    public function replyNotificationToSingleUser(SendNotificationRequest $request)
    {
        if (!$request->user_id)
            return back()->with('error', 'هذا المستخدم غير موجود !');

        $user = User::findOrFail($request->user_id);

        $key = 'from_admin';
        $data = ['title' => $request->notification_message_title, 'message' => $request->notification_message_content, 'key' => $key];

        $this->sendNotification($user, $key, $data, false);

        auth()->user()->saveReport(' الرد علي رسالة واردة باشعار');

        return back()->with('success', __('dashboard.alerts.message_sent_successfully'));
    }
    // *** Send message to Single User: End ** //
}
