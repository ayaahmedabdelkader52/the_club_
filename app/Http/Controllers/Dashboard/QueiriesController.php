<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Queries;
use App\Traits\HasResponse;
use Illuminate\Http\Request;

class QueiriesController extends Controller
{
    use HasResponse;

    public function index()
    {
        $queries = Queries::all();

        return view('admin.queries.index', compact('queries'));
    }
    public function destroy(Queries $query)
    {

        $query ->delete();

        auth()->user()->saveReport(__('dashboard.query.delete_product'));

        return self::successReturn( '' , $query);
    }

    public function destroySelected(Request $request)
    {
        $ids = $request -> ids;

        $queries = Queries::find($ids);

        Queries::destroy($queries);

        auth()->user()->saveReport('حذف الاستفسارات');

        return self::successReturn('', $ids);

    }
}
