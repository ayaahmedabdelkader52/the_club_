<?php

namespace App\Http\Controllers\Dashboard\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\Settings\AdminProfileReqeust;
use App\Http\Requests\Settings\AdminUpdatePasswordRequest;
use App\Http\Requests\Settings\SettingRequest;
use App\Models\Setting;
use App\Models\MessagePackage;
use App\Models\Social;
use App\Models\User;
use App\Traits\HasResponse;
use App\Traits\Uploadable;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    use Uploadable, HasResponse;

    public function update(SettingRequest $request)
    {
        $settings = $request->all('settings');
        if ($request->has('settings.logo')) {
            $settings['settings']['logo'] = $this->uploadOne($request->settings['logo'], 'settings', true, 250, null);
        }

        foreach ($settings['settings'] as $key => $value) {
            $setting = Setting::where('key', $key)->first();
            ($setting) ? $setting->update(['value' => $value]) : Setting::create(['key' => $key, 'value' => $value]);

        }

        return redirect()->back()->with('success', __('dashboard.alerts.updated_successfully'));
    }

    public function index()
    {
        $socials = Social::orderBy('created_at', 'desc')->get();
        $smtp = MessagePackage::where('type', 'smtp')->first();
        $fcm = MessagePackage::where('type', 'fcm')->first();
        $smsPackages = DB::table('message_packages')
            ->whereIn('type', MessagePackage::SMS_PACKAGES)
            ->get();
        $otherPackages = array_diff(
            MessagePackage::SMS_PACKAGES, $smsPackages->pluck('type')->toArray()
        );

        return view('admin.settings.index', compact('socials', 'smtp', 'fcm', 'smsPackages', 'otherPackages'));
    }

    public function updateProfile(AdminProfileReqeust $request, User $user)
    {
        $user->update($request->validated());

        return redirect()->back()->with('success', __('dashboard.alerts.updated_successfully'));

    }

    public function updatePassword(AdminUpdatePasswordRequest $request)
    {
        auth()->user()->update([
            'password' => $request->password
        ]);

        return redirect()->back()->with('success', __('dashboard.alerts.updated_successfully'));
    }

    public function changeLanguage($language)
    {
        app()->setLocale($language);
        session()->put('lang', $language);

        return back();
    }


}
