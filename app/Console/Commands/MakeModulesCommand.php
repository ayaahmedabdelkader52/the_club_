<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class MakeModulesCommand extends Command
{
    protected $description = 'Create All Admin modules .';
    private $modelName;
    private $sakePluralName;
    private $camelPluralName;
    private $sakeSingleName;
    private $camelSingleName;
    private $sakePluralVariable;
    private $camelPluralVariable;
    private $sakeSingleVariable;
    private $camelSingleVariable;
    private $migrationClassName;
    private $arSingleName;
    private $arPluralName;

    protected $signature = 'make:modules
                {name=name : The name of model that you want to create.}
                {arSingleName=arSingleName : The Arabic single name like منتج}
                {arPluralName=arPluralName : The Arabic plural name like منتجات}
                {--ob : Create observer}
                {--seed : Create seeder}
                {--resource : Create resource}';

    public function handle()
    {
        $this->bindVariables();
        $msg = '';

        $msg .= ($returnMgs = $this->createModel()) ? "Model Created        -> " . $returnMgs . "\n" : '';
        $msg .= ($returnMgs = $this->createController()) ? "Controller Created   -> " . $returnMgs . "\n" : '';
        $msg .= ($returnMgs = $this->createMigration()) ? "Migration Created    -> " . $returnMgs . "\n" : '';
        $msg .= ($returnMgs = $this->createIndexView()) ? "Index View Created   -> " . $returnMgs . "\n" : '';
        $msg .= ($returnMgs = $this->createCreateView()) ? "Create View Created  -> " . $returnMgs . "\n" : '';
        $msg .= ($returnMgs = $this->createEditView()) ? "Edit View Created    -> " . $returnMgs . "\n" : '';
        $msg .= ($returnMgs = $this->createShowView()) ? "Show View Created    -> " . $returnMgs . "\n" : '';
        $msg .= ($returnMgs = $this->createStoreRequest()) ? "Store Request        -> " . $returnMgs . "\n" : '';
        $msg .= ($returnMgs = $this->createUpdateRequest()) ? "Update Request       -> " . $returnMgs . "\n" : '';
        $msg .= ($returnMgs = $this->createExport()) ? "Export               -> " . $returnMgs . "\n" : '';
        $this->putRoutes();
        $this->putSidebar();
        $this->putTranslations();
        $this->putRoutesTranslations();
        $msg .= "Admin Routes updates -> " . base_path('routes/admin.php') . "\n";
        $msg .= "Ar Routes Updated    -> " . base_path('resources/lang/ar/routes.php') . "\n";
        $msg .= "En Routes Updated    -> " . base_path('resources/lang/en/routes.php') . "\n";
        $msg .= "Ar dashboard Updated -> " . base_path('resources/lang/ar/dashboard.php') . "\n";
        $msg .= "En dashboard Updated -> " . base_path('resources/lang/en/dashboard.php');

        // create observer (optional)
        if ($this->option('ob')) {
            Artisan::call('make:observer', ['name' => $this->modelName . 'Observer']);
        }
        // #create observer (optional)
        //
        // create seeder (optional)
        if ($this->option('seed')) {
            Artisan::call('make:seeder', ['name' => $this->modelName . 'TableSeeder']);
        }
        // #create seeder (optional)

        // create request (optional)
        if ($this->option('resource')) {
            Artisan::call('make:resource', ['name' => 'Api/' . $this->modelName . 'Resource']);
        }
        // #create request (optional)

        $this->info($msg);
    }

    private function createModel()
    {
        $baseModel = (File::get(public_path('assets/src/models/ModelsName.php')));
        $newModel = app_path('Models\\' . $this->modelName . '.php');
        $content = $this->replaceVariablesNames($baseModel);

        if (file_exists($newModel)) {
            if ($this->confirm('Model Already exists, Do you want to Overwrite ', true)) {
                $this->writeContentIntoFile($newModel, $content);
                return $newModel;
            }
            return false;
        }

        $this->writeContentIntoFile($newModel, $content);
        return $newModel;
    }

    private function createController()
    {
        $baseController = (File::get(public_path('assets/src/controllers/TestController.php')));
        $newController = app_path('Http\Controllers\Dashboard\\' . $this->modelName . 'Controller.php');
        $directory = app_path('Http\Controllers\Dashboard');
        $content = $this->replaceVariablesNames($baseController);
        $this->createDirectoryIfNotExists($directory);

        if (file_exists($newController)) {
            if ($this->confirm('Controller Already exists, Do you want to Overwrite ', true)) {
                $this->writeContentIntoFile($newController, $content);
                return $newController;
            }
            return false;
        }

        $this->writeContentIntoFile($newController, $content);
        return $newController;
    }

    public function createMigration()
    {
        $baseMigration = (File::get(public_path('assets/src/migrations/test.php')));
        $directory = database_path('migrations');
        $newMigration = $directory . '\\' . date('Y_m_d_His') . '_create_' . $this->sakePluralName . '_table.php';
        $content = $this->replaceVariablesNames($baseMigration);
        $this->writeContentIntoFile($newMigration, $content);

        return $newMigration;
    }

    public function createIndexView()
    {
        $baseIndex = (File::get(public_path('assets/src/views/index.blade.php')));
        $content = $this->replaceVariablesNames($baseIndex);
        $directory = base_path('resources\views\admin\\' . $this->sakePluralName);
        $this->createDirectoryIfNotExists($directory);
        $newIndex = $directory . '\index.blade.php';

        if (file_exists($newIndex)) {
            if ($this->confirm('Index blade Already exists, Do you want to Overwrite ', true)) {
                $this->writeContentIntoFile($newIndex, $content);
                return $newIndex;
            }
            return false;
        }

        $this->writeContentIntoFile($newIndex, $content);
        return $newIndex;
    }

    public function createCreateView()
    {
        $baseCreate = (File::get(public_path('assets/src/views/create.blade.php')));
        $content = $this->replaceVariablesNames($baseCreate);
        $directory = base_path('resources\views\admin\\' . $this->sakePluralName);
        $this->createDirectoryIfNotExists($directory);
        $newCreate = $directory . '\create.blade.php';

        if (file_exists($newCreate)) {
            if ($this->confirm('Create blade Already exists, Do you want to Overwrite ', true)) {
                $this->writeContentIntoFile($newCreate, $content);
                return $newCreate;
            }
            return false;
        }

        $this->writeContentIntoFile($newCreate, $content);
        return $newCreate;
    }

    public function createEditView()
    {
        $baseEdit = (File::get(public_path('assets/src/views/edit.blade.php')));
        $content = $this->replaceVariablesNames($baseEdit);
        $directory = base_path('resources\views\admin\\' . $this->sakePluralName);
        $this->createDirectoryIfNotExists($directory);
        $newEdit = $directory . '\edit.blade.php';

        if (file_exists($newEdit)) {
            if ($this->confirm('Edit blade Already exists, Do you want to Overwrite ', true)) {
                $this->writeContentIntoFile($newEdit, $content);
                return $newEdit;
            }
            return false;
        }

        $this->writeContentIntoFile($newEdit, $content);
        return $newEdit;
    }

    public function createShowView()
    {
        $baseShow = (File::get(public_path('assets/src/views/show.blade.php')));
        $content = $this->replaceVariablesNames($baseShow);
        $directory = base_path('resources\views\admin\\' . $this->sakePluralName);
        $this->createDirectoryIfNotExists($directory);
        $newShow = $directory . '\show.blade.php';

        if (file_exists($newShow)) {
            if ($this->confirm('Show blade Already exists, Do you want to Overwrite ', true)) {
                $this->writeContentIntoFile($newShow, $content);
                return $newShow;
            }
            return false;
        }

        $this->writeContentIntoFile($newShow, $content);
        return $newShow;
    }

    private function createStoreRequest()
    {
        $baseRequest = (File::get(public_path('assets/src/requests/StoreRequest.php')));
        $content = $this->replaceVariablesNames($baseRequest);
        $this->createDirectoryIfNotExists(app_path('Http\Requests\\' . $this->modelName));
        $newRequest = app_path('Http\Requests\\' . $this->modelName . '\StoreRequest.php');

        if (file_exists($newRequest)) {
            if ($this->confirm('Store Request Already exists, Do you want to Overwrite ', true)) {
                $this->writeContentIntoFile($newRequest, $content);
                return $newRequest;
            }
            return false;
        }
        $this->writeContentIntoFile($newRequest, $content);
        return $newRequest;
    }

    private function createUpdateRequest()
    {
        $baseRequest = (File::get(public_path('assets/src/requests/UpdateRequest.php')));
        $content = $this->replaceVariablesNames($baseRequest);
        $this->createDirectoryIfNotExists(app_path('Http\Requests\\' . $this->modelName));
        $newRequest = app_path('Http\Requests\\' . $this->modelName . '\UpdateRequest.php');

        if (file_exists($newRequest)) {
            if ($this->confirm('Update Request Already exists, Do you want to Overwrite ', true)) {
                $this->writeContentIntoFile($newRequest, $content);
                return $newRequest;
            }
            return false;
        }
        $this->writeContentIntoFile($newRequest, $content);
        return $newRequest;
    }

    private function createExport()
    {
        $baseExport = (File::get(public_path('assets/src/Exports/Export.php')));
        $content = $this->replaceVariablesNames($baseExport);
        $this->createDirectoryIfNotExists(app_path('Exports'));
        $newExport = app_path('Exports\\' . $this->camelPluralName . 'Export.php');

        if (file_exists($newExport)) {
            if ($this->confirm('Export File is Already exists, Do you want to Overwrite ', true)) {
                $this->writeContentIntoFile($newExport, $content);
                return $newExport;
            }
            return false;
        }
        $this->writeContentIntoFile($newExport, $content);
        return $newExport;
    }

    private function putRoutes()
    {
        // create web routes
        file_put_contents('routes/admin.php',
            preg_replace(
                "/#new_routes_here/",
                "
    /** -------------- " . $this->modelName . " Start --------------- */
    Route::group(['prefix' => '" . $this->camelPluralName . "'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\\" . $this->modelName . "Controller@index',
            'as' => '" . $this->camelPluralName . ".index',
            'title' => __('dashboard.$this->camelPluralName.$this->camelPluralName'),
            'child' => [
                'admin." . $this->camelPluralName . ".index',
                'admin." . $this->camelPluralName . ".create',
                'admin." . $this->camelPluralName . ".downloadExcel',
                'admin." . $this->camelPluralName . ".store',
                'admin." . $this->camelPluralName . ".show',
                'admin." . $this->camelPluralName . ".edit',
                'admin." . $this->camelPluralName . ".update',
                'admin." . $this->camelPluralName . ".destroy',
                'admin." . $this->camelPluralName . ".destroy_selected',

            ]
        ]);
        Route::get('/download', [" . $this->modelName . "Controller::class, 'downloadAll" . $this->camelPluralName . "'])->name('" . $this->camelPluralName . ".downloadExcel');
        Route::resource('" . $this->camelPluralName . "'," . $this->modelName . "Controller::class)->except('index');
        Route::post('/selected', [" . $this->modelName . "Controller::class, 'destroySelected'])->name('" . $this->camelPluralName . ".destroy_selected');
    });
    /** -------------- " . $this->modelName . " End --------------- */

    #new_routes_here
                     ",
                file_get_contents('routes/admin.php')
            ));
    }

    private function putSidebar()
    {
        // create web routes
        file_put_contents('resources/views/admin/includes/sidebar.blade.php',
            preg_replace(
                "/<!-- #new_link_here -->/",
                "
                <!-- Start " . $this->modelName . "  -->
                @hasPermission('admin." . $this->camelPluralName . ".index')
                    <li class=' nav-item @if(\Request::route()->getName() == 'admin." . $this->camelPluralName . ".index') active @endif'>
                        <a href='{{ route('admin." . $this->camelPluralName . ".index') }}'>
                            <i class='feather icon-help-circle'></i>
                            <span class='menu-title' data-i18n='Email'>{{ __('dashboard." . $this->sakePluralName . '.' . $this->sakePluralName . "') }}</span>
                        </a>
                    </li>
                @endhasPermission
                <!-- End " . $this->modelName . "  -->
                <!-- #new_link_here -->
                     ",
                file_get_contents('resources/views/admin/includes/sidebar.blade.php')
            ));
    }

    private function putTranslations()
    {
        // create web routes
        file_put_contents('resources/lang/ar/dashboard.php',
            preg_replace(
                "/#new_trans_here/",
                "
   '" . $this->sakePluralName . "' => [
        '" . $this->sakePluralName . "' => '" . $this->arPluralName . "',
        'create_" . $this->sakeSingleName . "' => 'إضافة " . $this->arSingleName . "',
        'delete_" . $this->sakeSingleName . "' => 'حذف " . $this->arSingleName . "',
        'edit_" . $this->sakeSingleName . "' => 'تعديل " . $this->arSingleName . "',
        'show_" . $this->sakeSingleName . "' => 'عرض " . $this->arSingleName . "',
        'all_" . $this->sakePluralName . "' => 'كل ال" . $this->arPluralName . "',
        'do_you_want_to_delete_this_" . $this->sakeSingleName . "' => 'هل تود فعلا مسح هذا " . $this->arSingleName . "',
        'sorry_you_can_not_delete_this_" . $this->sakeSingleName . "' => 'عفوا هذا " . $this->arSingleName . " لا يمكن مسحه !',
    ],
    #new_trans_here
                     ",
                file_get_contents('resources/lang/ar/dashboard.php')
            ));

        file_put_contents('resources/lang/en/dashboard.php',
            preg_replace(
                "/#new_trans_here/",
                "
   '" . $this->sakePluralName . "' => [
        '" . $this->sakePluralName . "' => '" . $this->camelPluralName . "',
        'create_" . $this->sakeSingleName . "' => 'Create " . $this->camelSingleName . "',
        'delete_" . $this->sakeSingleName . "' => 'Delete " . $this->camelSingleName . "',
        'edit_" . $this->sakeSingleName . "' => 'Edit " . $this->camelSingleName . "',
        'show_" . $this->sakeSingleName . "' => 'Show " . $this->camelSingleName . "',
        'all_" . $this->sakePluralName . "' => 'All " . $this->camelPluralName . "',
        'do_you_want_to_delete_this_" . $this->sakeSingleName . "' => 'Do you want to delete this " . $this->camelSingleName . "',
        'sorry_you_can_not_delete_this_" . $this->sakeSingleName . "' => 'Sorry you can not delete this " . $this->camelSingleName . " ',
    ],
    #new_trans_here
                     ",
                file_get_contents('resources/lang/en/dashboard.php')
            ));
    }

    private function putRoutesTranslations()
    {
        // create web routes
        file_put_contents('resources/lang/ar/routes.php',
            preg_replace(
                "/#new_routes_trans_here/",
                "
       '" . $this->sakePluralName . "' => [
            'index' => 'عرض الكل',
            'create' => 'عرض صفحة إضافة " . $this->arSingleName . "',
            'store' => 'تخزين " . $this->arSingleName . "',
            'edit' => 'عرض صفحة تعديل " . $this->arSingleName . "',
            'update' => 'تحديث " . $this->arSingleName . "',
            'downloadExcel' => 'تحميل ك Excel ',
            'show' => 'عرض " . $this->arSingleName . "',
            'destroy' => 'حذف " . $this->arSingleName . "',
            'destroy_selected' => 'حذف متعدد',
        ],
        #new_routes_trans_here
                     ",
                file_get_contents('resources/lang/ar/routes.php')
            ));

        file_put_contents('resources/lang/en/routes.php',
            preg_replace(
                "/#new_routes_trans_here/",
                "
        '" . $this->sakePluralName . "' => [
            'index' => 'Show all',
            'create' => 'Show Create " . $this->camelSingleName . " Page',
            'store' => 'Store " . $this->camelSingleName . "',
            'edit' => 'Show Edit " . $this->camelSingleName . " Page',
            'update' => 'Update " . $this->camelSingleName . "',
            'downloadExcel' => 'Download As Excel',
            'show' => 'Show " . $this->camelSingleName . "',
            'destroy' => 'Delete " . $this->camelSingleName . "',
            'destroy_selected' => 'Delete Selected',
        ],
        #new_routes_trans_here
                     ",
                file_get_contents('resources/lang/en/routes.php')
            ));
    }

    private function writeContentIntoFile($file, $content)
    {
        $con = fopen($file, 'w');
        fwrite($con, $content);
    }

    private function bindVariables()
    {
        $this->modelName = $modelName = $this->argument('name');
        $this->sakePluralName = $this->pluralize($this->unCamelCase($modelName));
        $this->camelPluralName = $this->pluralize($this->camelize($this->modelName));
        $upperPluralName = $this->pluralize($this->upperCase($this->modelName));
        $this->sakeSingleName = $this->unCamelCase($this->modelName);
        $this->camelSingleName = $this->camelize($this->modelName);
        $this->sakePluralVariable = '$' . $this->pluralize($this->unCamelCase($this->modelName));
        $this->camelPluralVariable = '$' . $this->pluralize($this->camelize($this->modelName));
        $this->sakeSingleVariable = '$' . $this->unCamelCase($this->modelName);
        $this->camelSingleVariable = '$' . $this->camelize($this->modelName);
        $this->migrationClassName = 'Create' . $upperPluralName . 'Table';
        $this->arSingleName = $this->argument('arSingleName');
        $this->arPluralName = $this->argument('arPluralName');
    }

    private function createDirectoryIfNotExists($directory)
    {
        if (!File::isDirectory($directory)) {
            File::makeDirectory($directory, 0777, true, true);
        }
    }

    private function replaceVariablesNames($base)
    {
        $content = str_replace('ModelName', $this->modelName, $base);
        $content = str_replace('$pluralVariable', $this->camelPluralVariable, $content);
        $content = str_replace('$singularVariable', $this->camelSingleVariable, $content);
        $content = str_replace('pluralCompactName', $this->camelPluralName, $content);
        $content = str_replace('singularCompactName', $this->camelSingleName, $content);
        $content = str_replace('viewSource', $this->sakePluralName, $content);
        $content = str_replace('pluralCompactVariableExport', $this->sakeSingleName, $content);
        $content = str_replace('__migration_class_name__', $this->migrationClassName, $content);
        $content = str_replace('__snake_plural_name__', $this->sakePluralName, $content);
        $content = str_replace('__page_title__', $this->camelPluralName, $content);
        $content = str_replace('__pluralVariable__', $this->camelPluralVariable, $content);
        $content = str_replace('__singleVariable__', $this->camelSingleVariable, $content);
        $content = str_replace('__routeName__', $this->sakePluralName, $content);
        $content = str_replace('__jsPluralVariable__', $this->camelPluralName, $content);
        $content = str_replace('__snake_single_name__', $this->camelSingleName, $content);
        $content = str_replace('__pluralName__', $this->camelPluralName, $content);
        $content = str_replace('__singleName__', $this->camelSingleName, $content);

        return str_replace('routeName', $this->sakePluralName, $content);
    }

    private function camelize($input, $separator = '_')
    {
        return lcfirst(str_replace($separator, '', ucwords($input, $separator)));
    }

    private function upperCase($input, $separator = '_')
    {
        return str_replace($separator, '', ucwords($input, $separator));
    }

    private function unCamelCase($input, $separated = '_')
    {
        $input = preg_replace('/([a-z])([A-Z])/', "\\1" . $separated . "\\2", $input);
        $input = strtolower($input);
        return $input;
    }

    private function pluralize($singular)
    {
        $last_letter = strtolower($singular[strlen($singular) - 1]);

        switch ($last_letter) {
            case 'y':
                return substr($singular, 0, -1) . 'ies';
            case 's':
            case 'x':
                return $singular . 'es';
            default:
                return $singular . 's';
        }
    }
}
