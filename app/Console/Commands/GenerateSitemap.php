<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Sitemap\SitemapGenerator;

class GenerateSitemap extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the sitemap.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // SitemapGenerator::create(env('APP_URL'))->writeToFile(public_path('site-map.xml'));
        SitemapGenerator::create('http://127.0.0.1:8000/dashboard/countries')->writeToFile(public_path('site-map.xml'));

        $this->info('The sitemap Generated Successfully .');
    }
}
