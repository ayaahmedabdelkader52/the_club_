<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromArray, WithHeadings
{
    public function __construct($type){
        $this->type = $type;
    }
    public function headings(): array
    {
        return [
            __('dashboard.user.name'),
            __('dashboard.user.phone'),
            __('dashboard.user.role'),
            __('dashboard.user.status'),
            __('dashboard.main.Created At'),
        ];
    }

    public function array(): array
    {
        $users = User::select('name', 'phone', 'role_id', 'status', 'created_at')->latest()->get();

        foreach($users as $user){
            $status = __('dashboard.user.' . $user->status);

            $data[] = [
                __('dashboard.user.name') => $user->name,
                __('dashboard.user.phone') => $user->fullPhone,
                __('dashboard.user.role') => ($user->role) ? $user->role->name : '',
                __('dashboard.user.status') => $status,
                __('dashboard.main.Created At') => date('Y-m-d H:i', strtotime($user->created_at))
            ];
        }

        return $data;
    }

}
