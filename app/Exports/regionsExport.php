<?php


namespace App\Exports;


use App\Models\Region;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class regionsExport implements FromArray, WithHeadings
{
    public function headings(): array
    {
        return [
            __('dashboard.main.name_in_ar'),
            __('dashboard.main.name_in_en'),
            'الكود',
            'التعداد السكاني',
            __('dashboard.main.Created At'),
        ];
    }

    public function array(): array
    {
        $regions = Region::all();

        foreach($regions as $region){
            $data[] = [
                __('dashboard.main.name_in_ar') => $region->getTranslation('name', 'ar'),
                __('dashboard.main.name_in_en') => $region->getTranslation('name', 'en'),
                'الكود' => ($region->code ) ? $region->code : 'غير معروف',
                'التعداد السكاني' => ($region->population ) ? $region->population : 'غير معروف',
                __('dashboard.main.Created At') => date('Y-m-d H:i', strtotime($region->created_at))
            ];
        }

        return $data;
    }
}
