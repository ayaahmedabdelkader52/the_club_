<?php

namespace App\Exports;

use App\Models\Country;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CountriesExport implements FromArray, WithHeadings
{
    public function headings(): array
    {
        return [
            __('dashboard.main.name_in_ar'),
            __('dashboard.main.name_in_en'),
            __('dashboard.main.currency_in_ar'),
            __('dashboard.main.currency_in_en'),
            __('dashboard.main.currency_code_in_ar'),
            __('dashboard.main.currency_code_in_en'),
            'ISO2',
            'ISO3',
            'كود الموبايل',
            __('dashboard.main.Created At'),
        ];
    }

    public function array(): array
    {
        $countries = Country::all();

        foreach($countries as $country){
            $data[] = [
                __('dashboard.main.name_in_ar') => $country->getTranslation('name', 'ar'),
                __('dashboard.main.name_in_en') => $country->getTranslation('name', 'en'),
                __('dashboard.main.currency_in_ar') => $country->getTranslation('currency', 'ar'),
                __('dashboard.main.currency_in_en') => $country->getTranslation('currency', 'en'),
                __('dashboard.main.currency_code_in_ar') => $country->getTranslation('currency', 'ar'),
                __('dashboard.main.currency_code_in_en') => $country->getTranslation('currency', 'en'),
                'ISO2' => $country->iso2,
                'ISO3' => $country->iso3,
                'كود الموبايل' => $country->calling_code,
                __('dashboard.main.Created At') => date('Y-m-d H:i', strtotime($country->created_at))
            ];
        }

        return $data;
    }
}
