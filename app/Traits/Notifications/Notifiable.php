<?php

namespace App\Traits\Notifications;

use App\Models\User;
use Illuminate\Support\Str;

trait Notifiable
{
    use NotificationTrait;

    public function notify($notification)
    {
        $this->notifyFor($notification);
    }

    private function notifyFor($notification)
    {
        foreach ($notification->via() as $channel) {
            if (method_exists($this, $method = 'notifyFor'.Str::studly($channel))) {
                $this->{$method}($notification);
            }
        }

    }

    public static function notifyAdmins($notification)
    {
        $admins = User::where('type', User::ADMIN_TYPE)->get();

        foreach ($admins as $admin) {
            $admin->notify($notification);
        }
    }

}
