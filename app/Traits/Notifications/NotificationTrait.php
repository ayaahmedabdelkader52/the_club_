<?php

namespace App\Traits\Notifications;

trait NotificationTrait
{
    use FcmAndDBTrait;
    use EmailTrait;
    use SmsTrait;

    public function notifyForDatabase($notification)
    {
        $data = $notification->toDatabase();
        $data['user_id'] = $this->id;

        $this->sendToDatabase($data);
    }

    public function notifyForFcm($notification): void
    {
        $data = $notification->toFcm();
        $devices = $this
            ->join('devices', 'users.id', '=', 'devices.user_id')
            ->select('users.id as user_id', 'devices.device_id', 'devices.device_type', 'devices.orders_notify')
            ->where('devices.device_id', $this->id)
            ->get();

        $this->sendToFcm($devices, $data);
    }

    public function notifyForSms($notification)
    {
        $data = $notification->toSms();
        $phone = $this->fullPhone;
        $message = $data['message'];

        $this->sendSms($phone, $message);
    }

    public function notifyForEmail($notification)
    {
        $data = $notification->toEmail();

        $this->sendEmailMessage($this->email, $data);
    }

}
