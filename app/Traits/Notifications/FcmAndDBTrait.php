<?php

namespace App\Traits\Notifications;

use App\Jobs\Notifications\DatabaseJob;
use App\Jobs\Notifications\FcmJob;
use App\Models\MessagePackage;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

trait FcmAndDBTrait
{
    /**
     * `devices` must have `user_id`, `device_id`, and `device_type`
     * `data` must have `title`,`key`, `message`, `image`, and `data`
     */
//    public function sendNotification($devices, array $data)
//    {
//        $notifications = [];
//        foreach ($devices as $device) {
////            dd($device);
//            $notifications[] = [
//                'user_id' => $device['user_id'],
//                'notifier_id' => auth()->id(),
//                'title' => $data['title'],
//                'message' => $data['message'],
//                'key' => $data["key"],
//                'image' => $data['image'] ?? '',
//                'data' => json_encode($data['data'] ?? []),
//                'created_at' => date('y-m-d g:i'),
//                'updated_at' => date('y-m-d g:i'),
//            ];
//
//        }
//
//        $dbNotifications = array_unique($notifications, SORT_REGULAR);
//        $this->sendToDatabase($dbNotifications);
//
//        $this->sendToFcm($devices, $data);
//    }
    public function sendNotification($user, $type, $data, $sendFcm = false)
    {
        $notifications = [];


            $notifications[] = [
                'user_id' => $user['user_id'],
                'notifier_id' => auth()->id(),
                'title' => $data['title'],
                'message' => $data['message'],
                'key' => $data["key"],
                'image' => $data['image'] ?? '',
                'data' => json_encode($data['data'] ?? []),
                'created_at' => date('y-m-d g:i'),
                'updated_at' => date('y-m-d g:i'),
            ];


        $this->sendToDatabase($notifications);

        if (!$sendFcm){
            return;
        }
        foreach ($user['devices'] as $device) {
            if ($device->show_ads) {
//                dd($device->device_type);
                $this->sendFcm($device->device_id, $device->device_type, $data , $type);
            }
        }
    }
    private function sendFcm($device_id, $device_type, $data, $type ,$setBadge = 0)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($data['message']);
        $notificationBuilder->setBody($data['message'])->setSound('default');
//         $notificationBuilder->setBody($type)->setSound('default');

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $dataBuilder = new PayloadDataBuilder();

        $dataBuilder->addData($data);

        $data = $dataBuilder->build();

        if ($device_type == 'android') {
            $downstreamResponse = FCM::sendTo($device_id, $option, null, $data);
        } else {

            $downstreamResponse = FCM::sendTo($device_id, $option, $notification, $data);

        }
        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
    }
    private function sendToDatabase($notifications)
    {
        $job = new DatabaseJob($notifications);
        dispatch($job);
    }

    private function sendToFcm($devices, $data)
    {
        $this->checkFcmConfig();

        if (count($devices) > 0) {
            $job = new FcmJob($devices, $data);
            dispatch($job);
        }
    }

    private function checkFcmConfig()
    {
        $fcm = MessagePackage::where('type', 'fcm')->first();
        if (!$fcm)
            throw new \Exception('Your message dos not sent, please Check your FCM data in Admin Settings !');
        if (
            $fcm->server_key == "" ||
            $fcm->sender_id == ""
        ) {
            throw new \Exception('Your message dos not sent, please Check your FCM data in Admin Settings !');
        }
        return true;
    }

}
