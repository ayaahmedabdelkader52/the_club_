<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

trait Imageable
{
    private $className;

    public function __construct()
    {
        $this->className = Str::snake(class_basename($this));
    }


    public function getImagePathAttribute()
    {
        return asset("assets/uploads/$this->className/" . $this->image);
    }

    public function setImageAttribute($image)
    {
        if ($this->image && $this->image != "default.png") {
            File::delete(public_path("assets/uploads/$this->className/" . $this->image));
        }
        $this->attributes["image"] = $this->uploadOne($image, $this->className, true);
    }

}
