<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PublicMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $message;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        return $this->markdown('emails.public-message')
            ->with('data', $this->data);
    }
}
