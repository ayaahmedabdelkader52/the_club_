<?php

namespace App\Exceptions;


use Exception;


class ApplicationIsCompletedException extends Exception
{
    public function __construct($message)
    {
        if ($this->appKeyGenerated()) {
            throw new Exception($message);
        }

    }

    private function appKeyGenerated()
    {
        if (config('app.key')) {
            return true;
        }

        return false;
    }

}
