<?php

namespace App\Observers;

use Illuminate\Support\Facades\Cache;

class PermissionObserver
{
    public function created()
    {
        Cache::forget('authPermissions');
    }

    public function updated()
    {
        Cache::forget('authPermissions');
    }

    public function deleted()
    {
        Cache::forget('authPermissions');
    }

}
