<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'doctor_id',
        'status',
        'total_price',
        'price_after',
        'hours_per_day',
        'total_days',
        'status',
        'city',
        'start',
        'end',
    ];
    public function days()
    {
        return $this->belongsToMany(Day::class , 'order_days' , 'order_id' , 'day_id');
    }
    public function doctor()
    {
        return $this->belongsTo(User::class , 'doctor_id' , 'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class , 'user_id' , 'id');

    }

}
