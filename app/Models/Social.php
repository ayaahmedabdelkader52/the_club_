<?php

namespace App\Models;

use App\Traits\Imageable;
use App\Traits\Uploadable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    use HasFactory;
    use Uploadable;
    use Imageable;

    protected $fillable = [
        'name',
        'link',
        'image'
    ];

    public function getImagePathAttribute()
    {
        return asset('assets/uploads/socials/' . $this->image);
    }
}
