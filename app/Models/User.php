<?php

namespace App\Models;

use App\Notifications\SendVerificationCode;
use App\Traits\DeviceTrait;
use App\Traits\Notifications\Notifiable;
use App\Traits\ReportTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, ReportTrait, DeviceTrait;
    use Notifiable;

    const ADMIN_ID = 1;
    const USER_ID = 2;
    const ADMIN_TYPE = 'admin';

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
    ];

    public function getFullPhoneAttribute()
    {
        return $this->country_key . '0' . $this->phone;
    }

    public function getAvatarPathAttribute()
    {
        return asset('assets/uploads/users/' . $this->avatar);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function setPhoneAttribute($phone)
    {
        $filteredPhone = User::cleanPhone($phone);
        $this->attributes['phone'] = $filteredPhone['phone'];
        $this->attributes['country_key'] = $filteredPhone['country_key'];
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
    public function ordersDoctor()
    {
        return $this->hasMany(Order::class , 'doctor_id' , 'id');
    }
    public function ordersUsers()
    {
        return $this->hasMany(Order::class , 'user_id' , 'id');
    }
    public function queriesdoctor()
    {
        return $this->hasMany(Queries::class , 'doctor_id' , 'id');
    }
    public function queriesuser()
    {
        return $this->hasMany(Queries::class , 'user_id' , 'id');

    }
    public function subcategories()
    {
        return $this->belongsToMany(Subcategory::class , 'user_subcategories');
    }
    public function rates()
    {
        return $this->hasMany(Rate::class , 'user_id' , 'id');
    }
    public function languages()
    {
        return $this->belongsToMany(Language::class , 'user_languages');
    }
    public function userlanguages()
    {
        return $this->belongsTo(UserLanguage::class );
    }
    public function complain()
    {
        return $this->hasMany(Complain::class);
    }

    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    public function reports()
    {
        return $this->hasMany(Report::class);
    }
    public function contactsUs()
    {
        return $this->hasMany(Contact::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function readNotifications()
    {
        return $this->notifications()->where('seen', 1);
    }

    public function unreadNotifications()
    {
        return $this->notifications()->where('seen', 0);
    }

    /**
     * Return the devices only `user_id`, `device_id`, and `device_type` from `users` table and `user_devices` table
     */
    public function scopeWithDevices($builder)
    {
        $builder
            ->join('devices', 'users.id', '=', 'devices.user_id')
            ->select('users.id as user_id', 'devices.device_id', 'devices.device_type', 'devices.orders_notify');
    }

    /**
     * Return the Filtered `users`
     */
    public function scopeFilter($builder, $request)
    {
        $builder
            ->when($request->user_type == 'admins', function ($query) {
                $query->where('type', 'admin');
            })
            ->when($request->user_type == 'active_users', function ($query) {
                $query->where('active', 'true');
            })
            ->when($request->user_type == 'inactive_users', function ($query) {
                $query->where('active', 'false');
            })
            ->when($request->user_type == 'block_users', function ($query) {
                $query->where('status', 'block');
            });
    }

    public function scopeUsers($builder)
    {
        return $builder->where('role_id', self::USER_ID);
    }

    public static function cleanPhone($phone)
    {
        $string = str_replace(' ', '', $phone);
        $string = str_replace('-', '', $string);

        if (str_starts_with($phone, '+966')) {
            return [
                'country_key' => '00966',
                'phone' => substr($string, 4),
            ];
        } elseif (str_starts_with($phone, '00966')) {
            return [
                'country_key' => '00966',
                'phone' => substr($string, 5),
            ];
        } elseif (str_starts_with($phone, '966')) {
            return [
                'country_key' => '00966',
                'phone' => substr($string, 3),
            ];
            /** Egypt Phone */
        } elseif (str_starts_with($phone, '+2')) {

            return [
                'country_key' => '002',
                'phone' => substr($string, 3)
            ];
        } elseif (str_starts_with($phone, '002')) {
            return [
                'country_key' => '002',
                'phone' => substr($string, 4)
            ];
        } elseif (str_starts_with($phone, '2')) {
            return [
                'country_key' => '002',
                'phone' => substr($string, 2),
            ];
        } elseif (str_starts_with($phone, '01')) {
            return [
                'country_key' => '002',
                'phone' => substr($string, 1),
            ];
        } else {
            return [
                'country_key' => '00966',
                'phone' => $string,
            ];
        }
    }

    public function sendVerificationCode()
    {
        $code = mt_rand(1111, 9999);
        $code = 1234;
        $this->notify(new SendVerificationCode($code));

        $this->update(['code' => $code]);
    }

    public function markAsActive()
    {
        $this->update(['status' => 'active']);
    }
    public function resetCode($password)
    {
        $this->update([
            'code' => null,
            'password' => $password
        ]);
    }
}
