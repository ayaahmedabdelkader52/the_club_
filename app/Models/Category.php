<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = [
        'title_ar',
        'title_en',
        'logo',
    ];

    public function subcategories()
    {
      return  $this->hasMany(Subcategory::class , 'category_id' , 'id');
    }
    public function getLogoPathAttribute()
    {
        return asset('assets/uploads/categories/' . $this->logo);
    }

}
