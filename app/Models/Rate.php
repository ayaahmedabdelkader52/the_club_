<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    use HasFactory;
    protected $fillable = [
        'rate',
        'comment',
        'user_id',
        'type',
    ];
    public function user()
    {
        $this->belongsTo(User::class);
    }

}
