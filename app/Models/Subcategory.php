<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    use HasFactory;
    protected $fillable = [
        'title_ar',
        'title_en',
        'logo',
        'category_id',
        'user_id',
        'type',
    ];

    public function category()
    {
      return  $this->belongsTo(Category::class , 'category_id' , 'id' );
    }

    public function users()
    {
      return  $this->belongsToMany(User::class , 'user_subcategories'  );
    }
    public function getLogoPathAttribute()
    {
        return asset('assets/uploads/subcategories/' . $this->logo);
    }
}
