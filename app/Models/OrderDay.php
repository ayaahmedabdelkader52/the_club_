<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDay extends Model
{
    use HasFactory;
    protected $fillable = [
      'order_id',
      'day_id',
    ];
    public function day()
    {
        return $this->belongsTo(Day::class);
    }
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
