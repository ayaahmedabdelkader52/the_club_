<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLanguage extends Model
{
    use HasFactory;
    protected $fillable = [
        'language_id',
        'user_id',
    ];

    public function user()
    {
        return $this->hasMany(User::class,'user_id' , 'id');
    }
    public function languages()
    {
        return $this->hasMany(Language::class,'language_id' , 'id');
    }
}
