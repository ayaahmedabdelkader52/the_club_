<?php

namespace App\Models;

use App\Traits\Imageable;
use App\Traits\Uploadable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    use HasFactory;
    use Uploadable;
    use Imageable;

    protected $fillable = [
        'title',
        'content',
        'image',
        'expiry_date'
    ];

}
