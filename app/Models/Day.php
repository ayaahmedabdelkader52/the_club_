<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    use HasFactory;
    protected $fillable = [
        'name_ar',
        'name_en',
    ];
    public function orders()
    {
        return $this->belongsToMany(Order::class , 'order_days');
    }

}
