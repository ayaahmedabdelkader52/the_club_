<?php

namespace App\Providers;

use App\Http\View\Composers\NotificationComposer;
use App\Models\Contact;
use App\Models\Setting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Pagination\Paginator;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    private $notifications;

    public function register()
    {
        //
    }




    public function boot()
    {
        Paginator::useBootstrap();
    }
}
