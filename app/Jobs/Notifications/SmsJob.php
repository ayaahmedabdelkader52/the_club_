<?php

namespace App\Jobs\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SmsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $phone;
    private $message;
    private $data;
    private $service;

    public function __construct($phone, $message, $activePackage, $service)
    {
        $this->phone = $phone;
        $this->message = $message;
        $this->service = $service;

        $this->data['username'] = $activePackage->username;
        $this->data['password'] = $activePackage->password;
        $this->data['sender'] = $activePackage->sender_name;
    }

    public function handle()
    {
        $service = new $this->service;
        $service->send($this->phone, $this->message, $this->data);
    }
}
