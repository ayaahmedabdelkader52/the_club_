<?php

namespace App\Notifications;

class SmsFromAdmin
{
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function via()
    {
        return ['sms'];
    }

    /**
     * `message` key is required
     */
    public function toSms()
    {
        return [
            'message' => $this->request->message,
        ];
    }
}
