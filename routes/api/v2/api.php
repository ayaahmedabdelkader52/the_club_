<?php

use App\Http\Controllers\Api\v2\{AuthController,
    DoctorController,
    HomeController,
    OrderController,
    PatientController,
    ProfileController,
    SettingController,
    SocialController,
    UserController};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route::get('/intros' , [HomeController::class , 'index']);

    Route::get('/logo' , [SettingController::class , 'logo']);

//Route::get('/user/{user}', [UserController::class, 'show'])->name('login.api');
//    Route::get('/redirect/{provider}', [AuthController::class, 'redirect']);
//    Route::get('/callback/{provider}', [AuthController::class, 'callback']);
    Route::post('/login', [AuthController::class, 'login'])->name('login.api');
    Route::post('/register', [AuthController::class, 'Register']);
    Route::post('/forgetPassword', [AuthController::class, 'forgetPassword']);
    Route::post('/resetPassword', [AuthController::class, 'resetPassword']);
    Route::get('auth/facebook', [SocialController::class, 'facebookRedirect']);
    Route::get('auth/facebook/callback', [SocialController::class, 'loginWithFacebook']);

Route::middleware('auth:api')->group(function () {
//    Route::middleware('active:user')->group(function () {
    Route::post('activecode' , [AuthController::class ,'accountActivation'])->name('active');
    Route::get('resendCode' , [AuthController::class ,'resendCode']);
    Route::middleware('active.user')->group(function () {

    Route::post('/completeregister', [AuthController::class, 'completeRegister'])->name('complete');

    // our routes to be protected will go in here

    Route::get('/plan/{id}' , [OrderController::class , 'plan']);
    Route::post('/addorder/{id}' , [OrderController::class , 'addOrder']);


    Route::get('/doctorprofile' , [ProfileController::class , 'show']);
    Route::get('/profile' , [ProfileController::class , 'view']);
    Route::post('/editdoctor' , [ProfileController::class , 'updatedoctor']);
    Route::post('/editprofile' , [ProfileController::class , 'updatepatient']);

    Route::get('/categories' , [HomeController::class , 'categories']);

    //Doctor SubCategory
    Route::get('/getsubcategories/{id}' , [DoctorController::class , 'getsubcategories']);
    Route::post('/subcategory/{id}' , [DoctorController::class , 'addsubcategory']);
    Route::get('/editSubcategory/{id}' , [DoctorController::class , 'editSubcategory']);
    Route::post('/updateSubcategory/{id}' , [DoctorController::class , 'updateSubcategory']);
    Route::get('/deleteSubcategory/{id}' , [DoctorController::class , 'deleteSubcategory']);

    //Patient SubCategory

    Route::get('/subcategories/{id}' , [PatientController::class , 'subcategories']);

    Route::get('/doctors/{id}' , [PatientController::class , 'doctors']);
    Route::get('/doctor/{id}' , [PatientController::class , 'doctor']);

    // Order Doctor

    Route::get('/neworders' , [DoctorController::class , 'newOrders']);
    Route::get('/activeorders' , [DoctorController::class , 'activeOrders']);
    Route::get('/finishedorders' , [DoctorController::class , 'finishedOrders']);

    //Doctor Button

    Route::post('/activebutton' , [DoctorController::class , 'activebutton']);
    Route::post('/rejectbutton' , [DoctorController::class , 'rejectbutton']);
    Route::post('/finishingbutton' , [DoctorController::class , 'finishbutton']);

    // Order Patient

    Route::get('/new' , [PatientController::class , 'newOrders']);
    Route::get('/active' , [PatientController::class , 'activeOrders']);
    Route::get('/finished' , [PatientController::class , 'finishedOrders']);

    // Queries Patient
    Route::post('sendQuery/{id}' , [PatientController::class , 'sendInqueires']);
    Route::get('viewQueries' , [PatientController::class , 'viewQueries']);
    Route::get('viewQuery/{id}' , [PatientController::class , 'oneQuery']);

    // Queries Doctor
    Route::post('answerInqueires/{id}' , [DoctorController::class , 'answerInqueires']);
    Route::get('viewInqueries' , [DoctorController::class , 'viewInqueries']);
    Route::get('viewOneQuery/{id}' , [DoctorController::class , 'oneQuery']);

            // Filter

    Route::get('/filtercost/{id}',[PatientController::class , 'filterCost' ]);
    Route::get('/filterrate/{id}',[PatientController::class , 'filterRate' ]);
    Route::get('/filterplace/{id}',[PatientController::class , 'filterPlace' ]);

            //search
    Route::post('/search/{id}' , [PatientController::class , 'search']);

            //rate
    Route::post('/rate/{id}' , [PatientController::class , 'rate']);

    //Patient Balance
    Route::get('balance' , [PatientController::class , 'balance']);

    //Patient button
    Route::post('finishbutton' , [PatientController::class , 'finishbutton']);
    Route::post('paybutton' , [PatientController::class , 'paybutton']);

            //setting

    Route::post('/contact',[SettingController::class , 'contactUs']);
    Route::get('/aboutus' , [SettingController::class , 'aboutUs']);

    Route::get('/balance' , [PatientController::class , 'balance']);

    Route::get('/city' , [SettingController::class , 'cities']);

    Route::get('/faqs' , [SettingController::class , 'faqs']);
    Route::get('/socials' , [SettingController::class , 'socials']);

    Route::post('/logout', [AuthController::class, 'logout'])->name('logout.api');


        Route::middleware('auth:api')->get('/users', [AuthController::class, 'getUsers']);
        Route::get('users',[UserController::class,'index']);


        Route::group(['prefix' => 'user'],function(){

            Route::post('/login',[AuthController::class,'login']);
            Route::get('/all',[UserController::class,'index'])->middleware('api.token');

        });
});

});


