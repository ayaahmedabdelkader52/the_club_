<?php

use App\Http\Controllers\Api\v1\{
    AuthController,
    UserController,
};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/user/{user}', [UserController::class, 'show'])->name('login.api');
Route::post('/login', [AuthController::class, 'login'])->name('login.api');
Route::post('/register', [AuthController::class, 'register'])->name('register.api');

Route::middleware('auth:api')->group(function () {
    // our routes to be protected will go in here
    Route::post('/logout', [AuthController::class, 'logout'])->name('logout.api');
});

Route::middleware('auth:api')->get('/users', [AuthController::class, 'getUsers']);
Route::get('users',[UserController::class,'index']);


Route::group(['prefix' => 'user'],function(){
    Route::post('/login',[AuthController::class,'login']);
    Route::get('/all',[UserController::class,'index'])->middleware('api.token');
});
