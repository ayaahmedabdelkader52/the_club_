<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\v1\{
    AuthController,
    ProfileController,
    UserController
};
//Route::middleware('api.lang:api')->group(function () {


    Route::post('/login_post', [AuthController::class, 'login'])->name('login_post');
    Route::post('/register', [AuthController::class, 'register'])->name('register.api');
    Route::post('/password/forget', [AuthController::class, 'forgetPassword']);
    Route::post('/password/reset', [AuthController::class, 'resetPassword']);

    Route::middleware('auth:api')->group(function () {
        // *** User Start *** //
        Route::group(['prefix' => 'users'], function () {
            Route::get('/', [UserController::class, 'index']);
            Route::post('/show', [UserController::class, 'show']);
        });
        // *** User End *** //

        // *** Profile Start *** //
        Route::group(['prefix' => 'profile'], function () {
            Route::get('/show', [ProfileController::class, 'show']);
            Route::post('/update', [ProfileController::class, 'update']);
        });
        // *** Profile End *** //


        // *** Auth Start *** //
        Route::get('/code/resend', [AuthController::class, 'resendCode']);
        Route::post('/account/active', [AuthController::class, 'accountActivation'])->name('account.active');
        Route::post('/logout', [AuthController::class, 'logout'])->name('logout.api');
        // *** Auth End *** //
    });

//});
