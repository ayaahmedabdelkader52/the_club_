<?php

use App\Http\Controllers\Dashboard\{AdsController,
    CategoriesController,
    CityController,
    AuthController,
    ComplainController,
    ContactUsController,
    CountryController,
    DashboardController,
    FaqsController,
    DistrictController,
    IntroController,
    LanguagesController,
    MessageController,
    OrderController,
    PermissionController,
    NotificationController,
    ProductController,
    QueiriesController,
    RegionController,
    ReportController,
    Settings\SocialMediaController,
    SubcategoriesController,
    UserController,
    QrCodeController,
    Settings\SettingController,
    Settings\PackageController};


Route::get('/generate-qrcode', [QrCodeController::class, 'index']);

Route::group(['as' => 'admin.', 'middleware' => ['admin', 'role.check', 'locale']], function () {

//    Route::get('/generate-qrcode', [QrCodeController::class, 'index']);

    /** -------------- Home Start --------------- */
    Route::get('/', [
        'uses' => 'App\Http\Controllers\Dashboard\DashboardController@dashboard',
        'as' => 'admin',
        'title' => 'الصفحه الرئيسيه',
        'child' => [
            'admin.admin'
        ]
    ]);
    /** -------------- Home End --------------- */

    /** -------------- Settings Start --------------- */
    Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\Settings\SettingController@index',
            'as' => 'index',
            'title' => __('dashboard.main.settings'),
            'child' => [
                'admin.settings.index',
                'admin.settings.update',
                'admin.settings.update_profile',
                'admin.settings.update_password',
                'admin.settings.store_social_media',
                'admin.settings.update_social_media',
                'admin.settings.delete_social_media',
                'admin.settings.update_smtp',
                'admin.settings.update_sms',
                'admin.settings.update_fcm',
            ]

        ]);
        Route::post('/update/setting', [SettingController::class, 'update'])->name('update');
        Route::post('/update/profile/{user}', [SettingController::class, 'updateProfile'])->name('update_profile');
        Route::post('/update/password', [SettingController::class, 'updatePassword'])->name('update_password');

        Route::post('/store/social', [SocialMediaController::class, 'store'])->name('store_social_media');
        Route::put('/update/social/{social}', [SocialMediaController::class, 'update'])->name('update_social_media');
        Route::delete('/{social}', [SocialMediaController::class, 'destroy'])->name('delete_social_media');

        Route::post('smtp/update', [PackageController::class, 'smtp'])->name('update_smtp');
        Route::put('packages/update', [PackageController::class, 'updateSms'])->name('update_sms');
        Route::post('fcm/update', [PackageController::class, 'updateFcm'])->name('update_fcm');
    });
    /** -------------- Settings End --------------- */

    /** -------------- Reports Start --------------- */
    Route::group(['prefix' => 'reports'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\ReportController@index',
            'as' => 'reports.index',
            'title' => 'التقارير',
            'child' => [
                'admin.reports.index',
                'admin.reports.destroy',
                'admin.reports.destroy_selected',
            ]
        ]);

        Route::delete('/{report}', [ReportController::class, 'destroy'])->name('reports.destroy');
        Route::post('/', [ReportController::class, 'destroySelected'])->name('reports.destroy_selected');

    });
    /** -------------- Reports End --------------- */

    /** -------------- inbox-page Start --------------- */
    Route::group(['prefix' => 'inbox-page', 'as' => 'inbox.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\ContactUsController@index',
            'as' => 'index',
            'title' => 'البريد الالكتروني',
            'child' => [
                'admin.inbox.index',
                'admin.inbox.show',
                'admin.inbox.destroy',
                'admin.inbox.destroy_selected',
                'admin.inbox.reply_sms_to_single_user',
                'admin.inbox.reply_email_to_single_user',
                'admin.inbox.reply_notification_to_single_user',
            ]
        ]);
        Route::get('show/{contact}', [ContactUsController::class, 'show'])->name('show');
        Route::delete('/{contact}', [ContactUsController::class, 'destroy'])->name('destroy');
        Route::post('/delete-selected', [ContactUsController::class, 'destroySelected'])->name('destroy_selected');


        /** -------------- Reply message to Single User: Start --------------- */
        Route::post('/reply-sms-to-single-inbox', [ContactUsController::class, 'replySmsToSingleUser'])->name('reply_sms_to_single_user');
        Route::post('/reply-email-to-single-inbox', [ContactUsController::class, 'replyEmailToSingleUser'])->name('reply_email_to_single_user');
        Route::post('/reply-notification-to-single-inbox', [ContactUsController::class, 'replyNotificationToSingleUser'])->name('reply_notification_to_single_user');
        /** -------------- Reply message to Single User: End --------------- */
    });
    /** -------------- inbox-page End --------------- */

    /** -------------- complains Start --------------- */
    Route::group(['prefix' => 'complains', 'as' => 'complains.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\ComplainController@index',
            'as' => 'index',
            'title' => 'الشكاوي والمقترحات',
            'child' => [
                'admin.complains.index',
                'admin.complains.show',
                'admin.complains.destroy',
                'admin.complains.destroy_selected',
                'admin.complains.change_status',
                'admin.complains.reply_sms_to_single_user',
                'admin.complains.reply_email_to_single_user',
                'admin.complains.reply_notification_to_single_user',
            ]
        ]);
        Route::get('-show/{complain}', [ComplainController::class, 'show'])->name('show');
        Route::delete('/{complain}', [ComplainController::class, 'destroy'])->name('destroy');
        Route::post('/delete-selected-complain', [ComplainController::class, 'destroySelected'])->name('destroy_selected');
        Route::post('/change-status-complain/{complain}', [ComplainController::class, 'changeStatus'])->name('change_status');

        /** -------------- Reply message to Single User: Start --------------- */
        Route::post('/reply-sms-to-single-complain', [ComplainController::class, 'replySmsToSingleUser'])->name('reply_sms_to_single_user');
        Route::post('/reply-email-to-single-complain', [ComplainController::class, 'replyEmailToSingleUser'])->name('reply_email_to_single_user');
        Route::post('/reply-notification-to-single-complain', [ComplainController::class, 'replyNotificationToSingleUser'])->name('reply_notification_to_single_user');
        /** -------------- Reply message to Single User: End --------------- */
    });
    /** -------------- complains End --------------- */

    /** -------------- User Start --------------- */
    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\UserController@index',
            'as' => 'index',
            'title' => 'الاعضاء',
            'child' => [
                'admin.users.index',
                'admin.users.blocked',
                'admin.users.active',
                'admin.users.inactive',
                'admin.users.doctor',
                'admin.users.patient',
                'admin.users.male',
                'admin.users.female',
                'admin.users.create',
                'admin.users.downloadUsers',
                'admin.users.store',
                'admin.users.show',
                'admin.users.edit',
                'admin.users.update',
                'admin.users.destroy',
                'admin.users.destroy_selected',
                'admin.users.block_user',
                'admin.users.un_block_user',
                'admin.notification.create',
                'admin.users.send_sms_to_all',
                'admin.users.send_email_to_all',
                'admin.users.send_notification_to_all',
                'admin.users.send_sms_to_single_user',
                'admin.users.send_email_to_single_user',
                'admin.users.send_notification_to_single_user',
            ]
        ]);
        Route::get('/blocked', [UserController::class, 'blocked'])->name('blocked');
        Route::get('/active', [UserController::class, 'activeUsers'])->name('active');
        Route::get('/inactive', [UserController::class, 'inActiveUsers'])->name('inactive');
        Route::get('/doctors', [UserController::class, 'doctors'])->name('doctor');
        Route::get('/patients', [UserController::class, 'patients'])->name('patient');
        Route::get('/male', [UserController::class, 'maleUsers'])->name('male');
        Route::get('/female', [UserController::class, 'femaleUsers'])->name('female');
        Route::get('/download-users', [UserController::class, 'downloadAllUsers'])->name('downloadUsers');
        Route::get('/create', [UserController::class, 'create'])->name('create');
        Route::post('/', [UserController::class, 'store'])->name('store');
        Route::get('/{user}', [UserController::class, 'show'])->name('show');
        Route::get('/{user}/edit', [UserController::class, 'edit'])->name('edit');
        Route::put('/{user}', [UserController::class, 'update'])->name('update');
        Route::delete('/{user}', [UserController::class, 'destroy'])->name('destroy');
        Route::post('/selected', [UserController::class, 'destroySelected'])->name('destroy_selected');
        Route::post('/block-user/{user}', [UserController::class, 'block'])->name('block_user');
        Route::post('/un-block-user/{user}', [UserController::class, 'unBlock'])->name('un_block_user');

        /** -------------- Send message to All User: Start --------------- */
        Route::post('/send-sms-to-all', [MessageController::class, 'smsToAllUsers'])->name('send_sms_to_all');
        Route::post('/send-email-to-all', [MessageController::class, 'emailAllUsers'])->name('send_email_to_all');
        Route::post('/send-notification-to-all', [MessageController::class, 'notificationAllUsers'])->name('send_notification_to_all');
        /** -------------- Send message to All User: End --------------- */
        //
        /** -------------- Send message to Single User: Start --------------- */
        Route::post('/send-sms-to-single-user', [MessageController::class, 'smsToSingleUser'])->name('send_sms_to_single_user');
        Route::post('/send-email-to-single-user', [MessageController::class, 'emailToSingleUser'])->name('send_email_to_single_user');
        Route::post('/send-notification-to-single-user', [MessageController::class, 'notificationToSingleUser'])->name('send_notification_to_single_user');
        /** -------------- Send message to Single User: End --------------- */
    });
    /** -------------- User End --------------- */

    /** -------------- Countries: Start --------------- */
    Route::group(['prefix' => 'countries', 'as' => 'countries.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\CountryController@index',
            'as' => 'index',
            'title' => 'الدول',
            'child' => [
                'admin.countries.index',
                'admin.countries.create',
                'admin.countries.downloadCountries',
                'admin.countries.store',
                'admin.countries.show',
                'admin.countries.edit',
                'admin.countries.update',
                'admin.countries.destroy',
                'admin.countries.destroy_selected',
                'admin.countries.regions',
                'admin.countries.regions_create',
            ]
        ]);
        Route::get('/create' , [CountryController::class, 'create'])->name('create');
        Route::post('/store' , [CountryController::class, 'store'])->name('store');
        Route::get('/edit/{country}' , [CountryController::class, 'edit'])->name('edit');
        Route::get('/show/{country}' , [CountryController::class, 'show'])->name('show');
        Route::post('/update/{country}' , [CountryController::class, 'update'])->name('update');
        Route::get('/destroy/{country}' , [CountryController::class, 'destroy'])->name('destroy');
        Route::get('/download-countries', [CountryController::class, 'downloadAllCountries'])->name('downloadCountries');
        Route::post('/selected', [CountryController::class, 'destroySelected'])->name('destroy_selected');
        // *** Regions: Start *** //
        Route::get('/regions/{country}', [RegionController::class, 'index'])->name('regions');
        Route::get('{country}/regions/create', [RegionController::class, 'create'])->name('regions_create');
        // *** Regions: End *** //
    });
    /** -------------- Countries: End --------------- */

    /** -------------- Regions Start --------------- */
    Route::group(['prefix' => 'regions', 'as' => 'regions.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\RegionController@index',
            'as' => 'index',
            'title' => 'المناطق',
            'child' => [
                'admin.regions.index',
                'admin.regions.create',
                'admin.regions.downloadCities',
                'admin.regions.store',
                'admin.regions.show',
                'admin.regions.edit',
                'admin.regions.update',
                'admin.regions.destroy',
                'admin.regions.destroy_selected',
                'admin.regions.cities',
                'admin.regions.cities_create',
            ]
        ]);
        // Route::get('/create/{country}', [RegionController::class, 'create'])->name('create');
        Route::get('regions/download', [RegionController::class, 'downloadAllRegions'])->name('downloadExcel');
        Route::resource('/', RegionController::class)->except('index', 'create');
        Route::post('/selected', [RegionController::class, 'destroySelected'])->name('destroy_selected');
        // *** Cities: Start *** //
        Route::get('/cities/{region}', [CityController::class, 'index'])->name('cities');
        Route::get('{region}/cities/create', [CityController::class, 'create'])->name('cities_create');
        // *** Cities: End *** //
    });
    /** -------------- Regions: End --------------- */

    /** -------------- Cities: Start --------------- */
    Route::group(['prefix' => 'cities', 'as' => 'cities.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\CityController@index',
            'as' => 'index',
            'title' => 'المدن',
            'child' => [
                'admin.cities.index',
                'admin.cities.create',
                'admin.cities.downloadCities',
                'admin.cities.store',
                'admin.cities.show',
                'admin.cities.edit',
                'admin.cities.update',
                'admin.cities.destroy',
                'admin.cities.destroy_selected',
                'admin.cities.districts',
                'admin.cities.districts_create',

            ]
        ]);
        Route::resource('/', CityController::class)->except('index', 'create');
        Route::get('/download', [CityController::class, 'downloadAllCities'])->name('downloadExcel');
        Route::post('/selected', [CityController::class, 'destroySelected'])->name('destroy_selected');
        // *** Districts: Start *** //
        Route::get('/districts/{city}', [DistrictController::class, 'index'])->name('districts');
        Route::get('{city}/districts/create', [DistrictController::class, 'create'])->name('districts_create');
        // *** Districts: End *** //
    });
    /** -------------- Cities: End --------------- */

    /** -------------- Districts Start --------------- */
    Route::group(['prefix' => 'districts', 'as' => 'districts.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\DistrictController@index',
            'as' => 'index',
            'title' => 'الأحياء',
            'child' => [
                'admin.districts.index',
                'admin.districts.create',
                'admin.districts.downloadCities',
                'admin.districts.store',
                'admin.districts.show',
                'admin.districts.edit',
                'admin.districts.update',
                'admin.districts.destroy',
                'admin.districts.destroy_selected',

            ]
        ]);
        Route::resource('/', DistrictController::class)->except('index', 'create');
        Route::get('/download', [DistrictController::class, 'downloadAllCities'])->name('downloadExcel');
        Route::post('/selected', [DistrictController::class, 'destroySelected'])->name('destroy_selected');
    });
    /** -------------- Districts End --------------- */

    /** -------------- Permissions Start --------------- */
    Route::group(['prefix' => 'permissions', 'as' => 'permissions.'], function () {

        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\PermissionController@index',
            'as' => 'index',
            'title' => 'الصلاحيات',
            'child' => [
                'admin.permissions.index',
                'admin.permissions.create',
                'admin.permissions.store',
                'admin.permissions.show',
                'admin.permissions.edit',
                'admin.permissions.update',
                'admin.permissions.destroy',
                'admin.permissions.destroy_selected',
            ]
        ]);
        Route::get('/create', [PermissionController::class, 'create'])->name('create');
        Route::post('/', [PermissionController::class, 'store'])->name('store');
        Route::get('/{role}', [PermissionController::class, 'show'])->name('show');
        Route::get('/{role}/edit', [PermissionController::class, 'edit'])->name('edit');
        Route::put('/{role}', [PermissionController::class, 'update'])->name('update');
        Route::delete('/{role}', [PermissionController::class, 'destroy'])->name('destroy');
        Route::post('/selected', [PermissionController::class, 'destroySelected'])->name('destroy_selected');
    });
    /** -------------- Permissions End --------------- */

    /** -------------- Notifications Start --------------- */
    Route::group(['prefix' => 'notifications', 'as' => 'notifications.'], function () {
        Route::get('/', [
            'uses' => 'App\Http\Controllers\Dashboard\NotificationController@index',
            'as' => 'index',
            'title' => 'الاشعارات',
            'child' => [
                'admin.notification.create',
                'admin.notification.store_sms',
                'admin.notification.store_email',
                'admin.notification.store_notification',
            ]
        ]);
        Route::get('/create', [NotificationController::class, 'create'])->name('create');
        Route::post('/store-sms', [NotificationController::class, 'sendSmsMessage'])->name('store_sms');
        Route::post('/store-email', [NotificationController::class, 'sendEmail'])->name('store_email');
        Route::post('notification/store', [NotificationController::class, 'adminSendNotification'])->name('store_notification');
    });
    /** -------------- Notifications End --------------- */

    /** -------------- ads Start --------------- */
    Route::group(['prefix' => 'ads'], function () {
        Route::get('/index', [
            'uses' => 'App\Http\Controllers\Dashboard\AdsController@index',
            'as' => 'ads.index',
            'title' => 'الاعلانات',
            'child' => [
                'admin.ads.index',
                'admin.ads.create',
                'admin.ads.store',
                'admin.ads.edit',
                'admin.ads.update',
                'admin.ads.destroy',
                'admin.ads.destroy_selected',
            ]
        ]);
        Route::resource('ads', AdsController::class)->except('index');
        Route::post('destroy/selected', [AdsController::class, 'destroySelected'])->name('ads.destroy_selected');
    });
    /** -------------- ads Start --------------- */

    /** -------------- Faqs Start --------------- */
    Route::group(['prefix' => 'faqs'], function () {
        Route::get('/index', [
            'uses' => 'App\Http\Controllers\Dashboard\FaqsController@index',
            'as' => 'faqs.index',
            'title' => 'الاسئله الشائعه',
            'child' => [
                'admin.faqs.index',
                'admin.faqs.create',
                'admin.faqs.store',
                'admin.faqs.edit',
                'admin.faqs.update',
                'admin.faqs.destroy',
                'admin.faqs.destroy_selected',
            ]
        ]);
        Route::resource('faqs', AdsController::class)->except('index');
        Route::post('destroy/selected', [AdsController::class, 'destroySelected'])->name('faqs.destroy_selected');
    });
    /** -------------- Faqs End --------------- */

    /** -------------- Intro Start --------------- */
    Route::group(['prefix' => 'intros', 'as' => 'intros.'], function () {

        Route::get('/', [
            'uses'=>'App\Http\Controllers\Dashboard\IntroController@index',
            'as'=>'index',
            'title'=>'المقدمه',
            'child'=>[
                'admin.intros.index',
                'admin.intros.create',
                'admin.intros.store',
                'admin.intros.edit',
                'admin.intros.update',
                'admin.intros.destroy',
                'admin.intros.destroy_selected',
            ],
        ]);
        Route::get('/create' , [IntroController::class, 'create'])->name('create');
        Route::post('/store' , [IntroController::class, 'store'])->name('store');
        Route::get('/edit/{intro}' , [IntroController::class, 'edit'])->name('edit');
        Route::post('/update/{intro}' , [IntroController::class, 'update'])->name('update');
        Route::get('/destroy/{intro}' , [IntroController::class, 'destroy'])->name('destroy');
        Route::post('destroyall' , [IntroController::class, 'destroySelected'])->name('destroy_selected');

    });
    /** -------------- Intro End --------------- */

    /** -------------- Category Start --------------- */
    Route::group(['prefix' => 'categories', 'as' => 'categories.'], function () {

        Route::get('/', [
            'uses'=>'App\Http\Controllers\Dashboard\CategoriesController@index',
            'as'=>'index',
            'title'=>'الاقسام الرئيسيه',
            'child'=>[
                'admin.categories.index',
                'admin.categories.create',
                'admin.categories.store',
                'admin.categories.show',
                'admin.categories.edit',
                'admin.categories.update',
                'admin.categories.destroy',
                'admin.categories.destroy_selected',
            ],
        ]);
        Route::get('/create' , [CategoriesController::class, 'create'])->name('create');
        Route::post('/store' , [CategoriesController::class, 'store'])->name('store');
        Route::get('/show' , [CategoriesController::class, 'show'])->name('show');
        Route::get('/edit/{category}' , [CategoriesController::class, 'edit'])->name('edit');
        Route::post('/update/{category}' , [CategoriesController::class, 'update'])->name('update');
        Route::get('/destroy/{category}' , [CategoriesController::class, 'destroy'])->name('destroy');
        Route::post('destroyall' , [CategoriesController::class, 'destroySelected'])->name('destroy_selected');

    });

    /** -------------- Category End --------------- */

 /** -------------- Subcategory Start --------------- */
    Route::group(['prefix' => 'subcategories', 'as' => 'subcategories.'], function () {

        Route::get('/', [
            'uses'=>'App\Http\Controllers\Dashboard\SubcategoriesController@index',
            'as'=>'index',
            'title'=>'الاقسام الفرعيه',
            'child'=>[
                'admin.subcategories.index',
                'admin.subcategories.create',
                'admin.subcategories.store',
                'admin.subcategories.show',
                'admin.subcategories.edit',
                'admin.subcategories.update',
                'admin.subcategories.destroy',
                'admin.subcategories.destroy_selected',
            ],
        ]);
        Route::get('/create' , [SubcategoriesController::class, 'create'])->name('create');
        Route::post('/store' , [SubcategoriesController::class, 'store'])->name('store');
        Route::get('/show' , [SubcategoriesController::class, 'show'])->name('show');
        Route::get('/edit/{subcategory}' , [SubcategoriesController::class, 'edit'])->name('edit');
        Route::post('/update/{subcategory}' , [SubcategoriesController::class, 'update'])->name('update');
        Route::get('/destroy/{subcategory}' , [SubcategoriesController::class, 'destroy'])->name('destroy');
        Route::post('destroyall' , [SubcategoriesController::class, 'destroySelected'])->name('destroy_selected');

    });

    /** -------------- Subcategory End --------------- */

 /** -------------- Orders Start --------------- */
    Route::group(['prefix' => 'orders', 'as' => 'orders.'], function () {

        Route::get('/', [
            'uses'=>'App\Http\Controllers\Dashboard\OrderController@index',
            'as'=>'index',
            'title'=>'الطلبات',
            'child'=>[
                'admin.orders.index',
                'admin.orders.finished',
                'admin.orders.rejected',
                'admin.orders.active',
                'admin.orders.pending',
                'admin.orders.show',
                'admin.orders.destroy',
                'admin.orders.destroy_selected',
            ],
        ]);
       Route::get('/show/{order}' , [OrderController::class, 'show'])->name('show');
       Route::get('/finished' , [OrderController::class, 'finished'])->name('finished');
       Route::get('/active' , [OrderController::class, 'active'])->name('active');
       Route::get('/rejected' , [OrderController::class, 'rejected'])->name('rejected');
       Route::get('/pending' , [OrderController::class, 'pending'])->name('pending');
       Route::get('/destroy/{subcategory}' , [OrderController::class, 'destroy'])->name('destroy');
        Route::post('destroyall' , [OrderController::class, 'destroySelected'])->name('destroy_selected');

    });

    /** -------------- Orders End --------------- */

/** -------------- Language Start --------------- */
    Route::group(['prefix' => 'languages', 'as' => 'languages.'], function () {

        Route::get('/', [
            'uses'=>'App\Http\Controllers\Dashboard\LanguagesController@index',
            'as'=>'index',
            'title'=>'اللغات',
            'child'=>[
                'admin.languages.index',
                'admin.languages.create',
                'admin.languages.store',
                'admin.languages.show',
                'admin.languages.edit',
                'admin.languages.update',
                'admin.languages.destroy',
                'admin.languages.destroy_selected',
            ],
        ]);
        Route::get('/create' , [LanguagesController::class, 'create'])->name('create');
        Route::post('/store' , [LanguagesController::class, 'store'])->name('store');
        Route::get('/edit/{subcategory}' , [LanguagesController::class, 'edit'])->name('edit');
        Route::post('/update/{subcategory}' , [LanguagesController::class, 'update'])->name('update');
        Route::get('/destroy/{subcategory}' , [LanguagesController::class, 'destroy'])->name('destroy');
        Route::post('destroyall' , [LanguagesController::class, 'destroySelected'])->name('destroy_selected');

    });

    /** -------------- Languages End --------------- */

/** -------------- Queries Start --------------- */
    Route::group(['prefix' => 'queries', 'as' => 'queries.'], function () {

        Route::get('/', [
            'uses'=>'App\Http\Controllers\Dashboard\QueiriesController@index',
            'as'=>'index',
            'title'=>'الاستفسارات',
            'child'=>[
                'admin.queries.index',
                'admin.queries.destroy',
                'admin.queries.destroy_selected',
            ],
        ]);
        Route::get('/destroy/{query}' , [QueiriesController::class, 'destroy'])->name('destroy');
        Route::post('destroyall' , [QueiriesController::class, 'destroySelected'])->name('destroy_selected');

    });

    /** -------------- Queries End --------------- */

    #new_routes_here


});


Route::group(['as' => 'admin.'], function () {
    Route::get('them/{them}', [DashboardController::class, 'them'])->name('them');
    Route::get('change/{languages}', [SettingController::class, 'changeLanguage'])->name('change.languages');

    Route::get('login', [AuthController::class, 'login'])->name('login')->middleware('guest');
    Route::post('login', [AuthController::class, 'postLogin'])->name('post.login');
    Route::post('logout/admin', [AuthController::class, 'logoutAdmin'])->name('logout');
});
