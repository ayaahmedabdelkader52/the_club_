<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('doctor_id')->nullable();
            $table->string('price_after')->nullable();
            $table->string('total_price')->nullable();
            $table->enum('type' , ['monthly' , 'daily' , 'weekly'])->nullable();
            $table->dateTime('start')->nullable();
            $table->dateTime('end')->nullable();
            $table->boolean('is_agree')->nullable();
            $table->boolean('is_paied')->default(0);
            $table->string('hours_per_day')->nullable();
            $table->string('total_days')->nullable();
            $table->string('city')->nullable();
            $table->enum('status' , ['new' , 'active' ,'have_finished' , 'finished', 'rejected'])->default('new');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
