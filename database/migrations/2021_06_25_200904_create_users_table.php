<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('country_key')->nullable();
            $table->string('phone')->nullable();
            $table->string('password')->nullable();
            $table->string('nationality')->nullable();
            $table->string('weekly_off')->default(0);
            $table->string('monthly_off')->default(0);
            $table->string('price_hour')->nullable();
            $table->text('bio')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->string('avatar')->default('default.png');
            $table->foreignId('role_id')->default(2)->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->enum('status', ["block", "pending", "active"])->default('pending');
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->enum('type', ['admin', 'doctor' , 'patient'])->nullable();
            $table->string('avg_rate')->nullable();
            $table->string('code')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('lang')->nullable();
            $table->text('address')->nullable();
            $table->string('social_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
