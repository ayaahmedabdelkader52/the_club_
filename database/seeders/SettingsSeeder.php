<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $settings = [
        [
            'key' => 'logo',
            'value' => 'default.png',
        ],
        [
            'key' => 'site_email',
            'value' => 'aait@info.com',
        ],
        [
            'key' => 'site_phone',
            'value' => '012227965236'
        ],
        [
            'key' => 'site_name',
            'value' => 'أوامر',
        ],
        [
            'key' => 'description_ar',
            'value' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص ',
        ],
        [
            'key' => 'description_en',
            'value' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.',
        ],
        [
            'key' => 'site_keywords',
            'value' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.',
        ],
        [
            'key' => 'copyright_ar',
            'value' => 'جميع الحقوق محفوظة لشركة أوامر الشبكة سياسات 2021',
        ],
        [
            'key' => 'copyright_en',
            'value' => 'All rights reserved to a Awamer Elsabaka Policies 2021',
        ],
        [
            'key' => 'policies_ar',
            'value' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص ',
        ],
        [
            'key' => 'policies_en',
            'value' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.',
        ],
        [
            'key' => 'about_us_ar',
            'value' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص ',
        ],
        [
            'key' => 'about_us_en',
            'value' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.',
        ],
        [
            'key' => 'google_places_key',
            'value' => '',
        ],
        [
            'key' => 'wasl_api_key',
            'value' => '',
        ],
        [
            'key' => 'currencyconverterapi',
            'value' => '',
        ],
        [
            'key' => 'google_analytics',
            'value' => '',
        ],
        [
            'key' => 'live_chat',
            'value' => '',
        ],
        [
            'key' =>'star',
            'value' =>'4',
        ],
    ];

    public function run()
    {
        Setting::insert($this->settings);
    }
}
