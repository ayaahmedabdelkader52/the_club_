<?php

namespace Database\Seeders;

use App\Models\Social;
use Illuminate\Database\Seeder;

class SocialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $socials = [
        [
            'name' => 'facebook',
            'image' => 'facebook.png',
            'link' => 'https://www.facebook.com/',
        ],
        [
            'name' => 'twitter',
            'image' => 'twitter.png',
            'link' => 'https://www.twitter.com/',
        ],
        [
            'name' => 'instagram',
            'image' => 'instagram.png',
            'link' => 'https://www.instagram.com/',
        ],
        [
            'name' => 'whatsapp',
            'image' => 'whatsapp.png',
            'link' => '55566677',
        ]
    ];

    public function run()
    {
            Social::insert($this->socials);

    }
}
