var loadFile = function(event) {

    var outputId = event.path[0].id;
    outputId = 'output-' + outputId ;

    // console.log(outputId);
    // console.log(event.path[0].id);

    var outputs = document.getElementById(outputId);
    outputs.src = URL.createObjectURL(event.target.files[0]);
    outputs.onload = function() {
        URL.revokeObjectURL(outputs.src) // free memory
    }
};
