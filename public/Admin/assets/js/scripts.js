(function(window, undefined) {
  'use strict';

  /*
  NOTE:
  ------
  PLACE HERE YOUR OWN JAVASCRIPT CODE IF NEEDED
  WE WILL RELEASE FUTURE UPDATES SO IN ORDER TO NOT OVERWRITE YOUR JAVASCRIPT CODE PLEASE CONSIDER WRITING YOUR SCRIPT HERE.  */

})(window);

function fireError(title = '', content = '', timer = 1500, position = 'top-start', showConfirmButton = false) {
    Swal.fire({
        position: 'top-start',
        type: 'error',
        title: title,
        text: content,
        showConfirmButton,
        timer,
        confirmButtonClass: 'btn btn-primary',
        buttonsStyling: false,
    })
}

function fireSuccess(title = '', content = '', timer = 1500, position = 'top-start', showConfirmButton = false) {
    Swal.fire({
        position: position,
        type: "success",
        title: title,
        text: content,
        showConfirmButton,
        timer,
        confirmButtonClass: 'btn btn-primary',
        buttonsStyling: false,
    })
}
